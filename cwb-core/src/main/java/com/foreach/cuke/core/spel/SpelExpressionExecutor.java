/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.spel;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public final class SpelExpressionExecutor
{
	private static final Logger LOG = LoggerFactory.getLogger( SpelExpressionExecutor.class );

	@Autowired
	private SpelEvaluationContext evaluationContext;

	@Autowired
	private SpelRootProperties spelRootProperties;

	private ExpressionParser parser;
	private StandardEvaluationContext spelCtx;

	@PostConstruct
	public void prepareParser() {
		parser = new SpelExpressionParser();
		spelCtx = new StandardEvaluationContext();
		spelCtx.setRootObject( evaluationContext );
		spelCtx.addPropertyAccessor( spelRootProperties );
		spelCtx.addPropertyAccessor( evaluationContext.getPropertyAccessor() );
	}

	public boolean getValueAsBoolean( String expression ) {
		return parse( expression, Boolean.class );
	}

	public String getValueAsString( String expression ) {
		return ObjectUtils.toString( getValue( expression ) );
	}

	public Object getValue( String expression ) {
		return parse( expression, Object.class );
	}

	public <T> T parse( String expression, Class<T> type ) {
		String parseable = replaceShorthandLookups( expression );

		T evaluated = parser.parseExpression( parseable, new TemplateParserContext() ).getValue( spelCtx, type );
		if ( !StringUtils.isBlank( expression ) && !ObjectUtils.equals( expression, evaluated ) ) {
			LOG.debug( "evaluated expression {} to {}", expression, evaluated );
		}
		return evaluated;
	}

	/**
	 * Replace all expression in a given string by their string value.
	 */
	public String replaceExpressions( String original ) {
		String parseable = replaceShorthandLookups( original );

		return parser.parseExpression( parseable, new TemplateParserContext() ).getValue( spelCtx, String.class );
	}

	/**
	 * Evaluates all values in the map as SpEL expressions.
	 */
	public Map<String, String> replaceValues( Map<String, String> parameters ) {
		Map<String, String> parsed = new HashMap<String, String>();
		for ( Map.Entry<String, String> entry : parameters.entrySet() ) {
			parsed.put( entry.getKey(), getValueAsString( entry.getValue() ) );
		}

		return parsed;
	}

	private String replaceShorthandLookups( String expression ) {
		return StringUtils.defaultString( expression ).replaceAll( "\\$\\{([^\\$\\{\\}]*)\\}", "#{lookup('$1')}" );
	}

	private static class TemplateParserContext implements ParserContext
	{
		public String getExpressionPrefix() {
			return "#{";
		}

		public String getExpressionSuffix() {
			return "}";
		}

		public boolean isTemplate() {
			return true;
		}
	}
}
