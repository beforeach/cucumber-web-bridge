/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.cli;

import cucumber.runtime.io.MultiLoader;
import cucumber.runtime.model.*;
import gherkin.formatter.model.Tag;
import gherkin.formatter.model.TagStatement;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Arne Vandamme
 */
public class FeatureBatchBuilder
{
	public static final Tag KEEP_TOGETHER = new Tag( "@keepTogether", 0 );

	public static enum Distribution
	{
		// Simply split the list into chunks (last chunk will always be the biggest)
		CHUNK,
		// Divide the features in a round-robin fashion (no chunk will have more than one extra)
		ROUND_ROBIN
	}

	public static List<List<String>> distribute( List<String> entriesToDistribute,
	                                             int preferredNumberOfBatches,
	                                             Distribution distribution ) {
		List<List<String>> batches = new ArrayList<List<String>>( preferredNumberOfBatches );
		if ( distribution == Distribution.CHUNK ) {
			distributeByChunk( entriesToDistribute, batches, preferredNumberOfBatches );
		}
		else {
			distributeRoundRobin( entriesToDistribute, batches, preferredNumberOfBatches );
		}

		return batches;
	}

	private static void distributeRoundRobin( List<String> entries,
	                                          List<List<String>> distributed,
	                                          int preferredNumberOfBatches ) {
		List<List<String>> actualBatches = new ArrayList<List<String>>( preferredNumberOfBatches );
		for ( int i = 0; i < preferredNumberOfBatches; i++ ) {
			actualBatches.add( new ArrayList<String>() );
		}

		int position = 0;

		for ( String entry : entries ) {
			actualBatches.get( position++ ).add( entry );

			if ( position == actualBatches.size() ) {
				position = 0;
			}
		}

		for ( List<String> batch : actualBatches ) {
			if ( !batch.isEmpty() ) {
				distributed.add( batch );
			}
		}
	}

	private static void distributeByChunk( List<String> entries,
	                                       List<List<String>> distributed,
	                                       int preferredNumberOfBatches ) {
		int numberPerBatch = preferredNumberOfBatches > entries.size() ? 1 : entries.size() / preferredNumberOfBatches;

		List<String> subSet = new ArrayList<String>();

		for ( String entry : entries ) {
			subSet.add( entry );

			if ( subSet.size() == numberPerBatch && distributed.size() < preferredNumberOfBatches - 1 ) {
				distributed.add( subSet );
				subSet = new ArrayList<String>();
			}
		}

		if ( !subSet.isEmpty() ) {
			distributed.add( subSet );
		}
	}

	public static List<String> listScenarios( File directory, boolean keepScenarioOutline, List<String> tags ) {
		List<String> scenarios = new ArrayList<String>();

		List<CucumberFeature> features = CucumberFeature.load(
				new MultiLoader( Thread.currentThread().getContextClassLoader() ),
				Collections.singletonList( directory.getAbsolutePath() ),
				new ArrayList<Object>( tags )
		);

		for ( CucumberFeature feature : features ) {
			String featureFile = new File( directory, feature.getPath() ).getAbsolutePath();

			boolean keepFeatureTogether = shouldKeepTogether( feature.getGherkinFeature() );

			boolean hasScenarios = false;

			for ( CucumberTagStatement tagStatement : feature.getFeatureElements() ) {
				if ( tagStatement instanceof CucumberScenario ) {
					if ( keepFeatureTogether ) {
						hasScenarios = true;
						featureFile = featureFile + ":" + tagStatement.getGherkinModel().getLine();
					}
					else {
						scenarios.add( featureFile + ":" + tagStatement.getGherkinModel().getLine() );
					}
				}
				else if ( tagStatement instanceof CucumberScenarioOutline ) {
					CucumberScenarioOutline outline = (CucumberScenarioOutline) tagStatement;

					if ( keepScenarioOutline
							|| keepFeatureTogether
							|| shouldKeepTogether( outline.getGherkinModel() ) ) {
						hasScenarios = true;

						// All examples behave as one scenario
						StringBuilder scenarioWithExamples = new StringBuilder( featureFile );

						for ( CucumberExamples examples : outline.getCucumberExamplesList() ) {
							for ( CucumberScenario scenario : examples.createExampleScenarios() ) {
								scenarioWithExamples.append( ":" ).append( scenario.getGherkinModel().getLine() );
							}
						}

						if ( keepFeatureTogether ) {
							featureFile = scenarioWithExamples.toString();
						}
						else {
							scenarios.add( scenarioWithExamples.toString() );
						}
					}
					else {
						// Scenario outline examples behaves as separate scenario
						for ( CucumberExamples examples : outline.getCucumberExamplesList() ) {
							for ( CucumberScenario scenario : examples.createExampleScenarios() ) {
								scenarios.add( featureFile + ":" + scenario.getGherkinModel().getLine() );
							}
						}
					}
				}
			}

			if ( keepFeatureTogether && hasScenarios ) {
				scenarios.add( featureFile );
			}
		}

		return scenarios;
	}

	private static boolean shouldKeepTogether( TagStatement tagStatement ) {
		return tagStatement.getTags().contains( KEEP_TOGETHER );
	}

	public static List<String> list( String fileOrDirectory ) {
		File f = FileUtils.getFile( fileOrDirectory );

		return f.isDirectory() ? buildDirectoryFeatureList( f ) : Collections.<String>emptyList();
	}

	public static List<String> buildDirectoryFeatureList( File directory ) {
		Collection<File> features = FileUtils.listFiles( directory, new String[] { "feature" }, true );
		List<String> files = new ArrayList<String>( features.size() );

		for ( File f : features ) {
			files.add( f.getAbsolutePath() );
		}

		Collections.sort( files );

		return files;
	}
}
