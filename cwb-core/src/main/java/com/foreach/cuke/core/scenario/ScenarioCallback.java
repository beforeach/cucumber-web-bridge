package com.foreach.cuke.core.scenario;

import cucumber.api.Scenario;

public interface ScenarioCallback
{
	boolean accept( Scenario scenario );

	void before( Scenario scenario );

	void after( Scenario scenario );
}
