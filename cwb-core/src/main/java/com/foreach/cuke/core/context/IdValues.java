/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.context;

import cucumber.api.Scenario;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class IdValues
{
	private static final Logger LOG = LoggerFactory.getLogger( IdValues.class );

	private AtomicLong currentNumber;
	private final String testrun;
	private final String today;

	private String scenario;

	private volatile boolean randomized;

	public IdValues() {
		try {
			currentNumber = new AtomicLong(
					DateUtils.parseDate( "2040-01-01", "yyyy-MM-dd" ).getTime() - System.currentTimeMillis() );
		}
		catch ( Exception e ) {
			throw new RuntimeException( e );
		}

		testrun = UUID.randomUUID().toString();
		today = FastDateFormat.getInstance( "yyyy-MM-dd" ).format( new Date() );

		scenario = Long.toHexString( currentNumber.incrementAndGet() );
	}

	public String getToday() {
		LOG.trace( "id.today = {}", today );
		return today;
	}

	public String getScenario() {
		LOG.trace( "id.scenario = {}", scenario );
		return scenario;
	}

	public String getTestrun() {
		LOG.trace( "id.testrun = {}", testrun );
		return testrun;
	}

	public String getNext() {
		String next = Long.toHexString( currentNumber.incrementAndGet() );
		LOG.trace( "id.next = {}", next );
		return next;
	}

	public long getNextNumber() {
		long nextLong = currentNumber.incrementAndGet();
		LOG.trace( "id.nextNumber = {}", nextLong );
		return nextLong;
	}

	public synchronized void updateScenario( Scenario scenario ) {
		if ( !randomized ) {
			randomized = true;
			int subtraction = new Random( scenario.getId().hashCode() ).nextInt( 100000000 );

			currentNumber.set( currentNumber.get() - subtraction );
		}

		this.scenario = Long.toHexString( currentNumber.incrementAndGet() );
	}
}
