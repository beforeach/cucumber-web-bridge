/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.steps;

import com.foreach.cuke.core.util.MapChecker;
import com.foreach.cuke.core.util.ObjectMatcher;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.commons.beanutils.BeanMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@Component
public class StandardSteps extends AbstractCoreSteps
{
	@Autowired
	private ObjectMatcher objectMatcher;

	@Autowired
	private MapChecker mapChecker;

	@Value("#{systemProperties['nopause']}")
	private Boolean disablePause;

	@Then("^(?:i )?pause$")
	public void pause() throws Throwable {
		if ( disablePause != Boolean.TRUE ) {
			LOG.info( "Pausing feature execution until button in dialog is pressed..." );

			JFrame jf = new JFrame();
			JOptionPane.showMessageDialog( jf, "Feature execution is paused and will resume when you click OK.",
			                               "Cucumber paused", JOptionPane.INFORMATION_MESSAGE );
			jf.toFront();
			jf.repaint();
		}
		else {
			LOG.info( "Disallowing execution pause because the 'nopause' property is specified." );
		}
	}

	@Then("^ensure that \"([^\"]*)\" does not equal \"([^\"]*)\"$")
	public void ensure_that_does_not_equal( String spelExpressionOne, String spelExpressionTwo ) throws Throwable {
		Object left = spel.getValue( spelExpressionTwo );
		Object right = spel.getValue( spelExpressionOne );

		LOG.debug( "assert value of {} does not equal {}", left, right );
		boolean matches = objectMatcher.matches( left, right );
		assertFalse( matches );
	}

	@Then("^ensure that \"([^\"]*)\" (?:equals|should equal) \"([^\"]*)\"$")
	public void ensure_that_equals( String spelExpressionOne, String spelExpressionTwo ) throws Throwable {
		Object left = spel.getValue( spelExpressionTwo );
		Object right = spel.getValue( spelExpressionOne );

		LOG.debug( "assert value of {} equals {}", left, right );
		boolean matches = objectMatcher.matches( left, right );
		assertTrue( matches );
	}

	@Then("^ensure that \"([^\"]*)\" (?:contains|should contain) \"([^\"]*)\"$")
	public void ensure_that_contains( String spelExpressionOne, List<String> properties ) throws Throwable {
		Object entity = spel.getValue( spelExpressionOne );

		Map<String, String> values = new HashMap<String, String>();
		for ( String property : properties ) {
			values.put( property, null );
		}
		Map entities = new BeanMap( entity );
		mapChecker.contains( entities, values, true );
	}

	@Then("^ensure that \"([^\"]*)\" (?:contains|should contain) \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void ensure_that_contains_with_value( String spelExpressionOne,
	                                             String propertyName,
	                                             String propertyValue ) throws Throwable {
		Object entity = spel.getValue( spelExpressionOne );

		Map<String, String> values = new HashMap<String, String>();
		values.put( propertyName, propertyValue );

		Map entities = new BeanMap( entity );
		mapChecker.contains( entities, values, true );
	}

	@Then("^ensure that \"([^\"]*)\" (?:contains|should contain):$")
	public void ensure_that_contains( String spelExpressionOne, DataTable dataTable ) throws Throwable {
		Object entity = spel.getValue( spelExpressionOne );

		int numberOfCells = dataTable.topCells().size();
		if ( numberOfCells != 1 && numberOfCells != 2 ) {
			fail( "should not contain only supports tables of 1 or 2 columns" );
		}
		Map<String, String> values = new HashMap<String, String>();
		if ( numberOfCells == 1 ) {
			List<String> datatablecells = dataTable.asList( String.class );
			for ( String cell : datatablecells ) {
				values.put( cell, null );
			}
		}
		if ( numberOfCells == 2 ) {
			values = dataTable.asMap( String.class, String.class );
		}
		Map entities = new BeanMap( entity );
		mapChecker.contains( entities, values, true );
	}

	@Then("^ensure that \"([^\"]*)\"$")
	public void ensure_that( String spelExpression ) throws Throwable {
		LOG.debug( "assert expression true: \"{}\"", spelExpression );
		assertTrue( spel.getValueAsBoolean( spelExpression ) );
	}

	@Given("^that \"([^\"]*)\"$")
	public void that( String spelExpression ) throws Throwable {
		LOG.debug( "asserting expression true: \"{}\"", spelExpression );
		assertTrue( spel.getValueAsBoolean( spelExpression ) );
	}

	@And("^(?:i )?save \"([^\"]*)\" as \"([^\"]*)\"$")
	public void save_as( String value, String variableName ) throws Throwable {
		LOG.debug( "saving value {} as variable '{}'", value, variableName );
		context.put( variableName, spel.getValue( value ) );
	}

	@And("^(?:i )?save variables:$")
	public void save_variables( Map<String, String> variables ) throws Throwable {
		for ( Map.Entry<String, String> entry : variables.entrySet() ) {
			save_as( entry.getValue(), entry.getKey() );
		}
	}

	@Given("^i execute \"([^\"]*)\"$")
	public void i_execute( String spelExpression ) throws Throwable {
		LOG.debug( "executing expression: {}", spelExpression );
		spel.parse( spelExpression, Object.class );
	}
}
