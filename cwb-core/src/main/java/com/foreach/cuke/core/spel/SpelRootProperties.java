/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.spel;

import org.springframework.expression.AccessException;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.PropertyAccessor;
import org.springframework.expression.TypedValue;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Holds a set of instances that behave as properties of the root evaluation context
 * when an expression is evaluated.  These properties can only be read.
 */
@Component
public class SpelRootProperties implements PropertyAccessor
{
	private Map<String, Object> rootProperties = new HashMap<String, Object>();

	@Override
	public Class<?>[] getSpecificTargetClasses() {
		return new Class[] { SpelEvaluationContext.class };
	}

	@Override
	public boolean canRead( EvaluationContext context, Object target, String name ) throws AccessException {
		return rootProperties.containsKey( name );
	}

	@Override
	public TypedValue read( EvaluationContext context, Object target, String name ) throws AccessException {
		return new TypedValue( rootProperties.get( name ) );
	}

	@Override
	public boolean canWrite( EvaluationContext context, Object target, String name ) throws AccessException {
		return false;
	}

	@Override
	public void write( EvaluationContext context, Object target, String name, Object newValue ) throws AccessException {

	}

	public void add( String name, Object value ) {
		rootProperties.put( name, value );
	}

	public void remove( String name ) {
		rootProperties.remove( name );
	}
}
