package com.foreach.cuke.core.scenario;

import cucumber.api.Scenario;

public abstract class SimpleScenarioCallback implements ScenarioCallback
{

	@Override
	public final boolean accept( Scenario scenario ) {
		return true;
	}

	@Override
	public void before( Scenario scenario ) {
	}

	@Override
	public void after( Scenario scenario ) {
	}
}
