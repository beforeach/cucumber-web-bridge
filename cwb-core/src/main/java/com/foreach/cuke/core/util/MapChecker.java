/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.util;

import com.foreach.cuke.core.spel.MapAsPropertyAccessor;
import com.foreach.cuke.core.spel.SpelExpressionExecutor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import java.util.*;

import static org.junit.Assert.*;

@Component
public class MapChecker
{
	protected final Logger LOG = LoggerFactory.getLogger( getClass() );

	@Autowired
	private SpelExpressionExecutor spel;

	@Autowired
	private ObjectMatcher objectMatcher;

	@SuppressWarnings("unchecked")
	public boolean contains( Map entity, Map<String, ?> values, boolean shouldFail ) {
		return contains( null, entity, values, shouldFail );
	}

	public boolean contains( String label, List<Object> entities, Map<String, ?> values, boolean shouldFail ) {
		if ( entities.size() == 1 ) {
			Object value = entities.get( 0 );

			return contains( label, value, values, shouldFail );
		}

		for ( int i = 0; i < entities.size(); i++ ) {
			String prefix = String.format( "%s[%s]", label, i );
			Object value = entities.get( i );

			if ( contains( prefix, value, values, false ) ) {
				return true;
			}
		}

		assertFalse( String.format( "Did not find any match in list [%s]", label ), shouldFail );

		return false;
	}

	@SuppressWarnings("unchecked")
	public boolean contains( String prefix, Object entity, Map<String, ?> values, boolean shouldFail ) {
		if ( entity == null ) {
			assertFalse( "Source data is null for map checking.", shouldFail );
			return false;
		}

		for ( Map.Entry<String, ?> row : values.entrySet() ) {
			String key = row.getKey();
			String label = ( prefix != null ? prefix + "." : "" ) + key;

			List<Object> valuesToCheck = retrieveValues( entity, key );

			if ( valuesToCheck == null ) {
				assertFalse( String.format( "Field [%s] not found", label ), shouldFail );
				return false;
			}

			if ( shouldCheckValue( row ) ) {
				Object expected = row.getValue();

				if ( valuesToCheck.size() == 1 ) {
					if ( !match( label, valuesToCheck.get( 0 ), expected, shouldFail ) ) {
						return false;
					}
				}
				else {
					boolean found = false;
					for ( Object valueToCheck : valuesToCheck ) {
						if ( match( label, valueToCheck, expected, false ) ) {
							found = true;
							break;
						}
					}

					assertFalse( String.format( "Field [%s] did not return any matches", label ),
					             !found && shouldFail );

					return found;
				}
			}
		}

		return true;
	}

	public Object retrieveSingleValue( Object entity, String key ) {
		List<Object> objects = retrieveValues( entity, key );

		return objects == null || objects.size() > 1 ? null : objects.get( 0 );
	}

	@SuppressWarnings("unchecked")
	public List<Object> retrieveValues( Object entity, String key ) {
		if ( entity instanceof Map ) {
			Map map = (Map) entity;

			if ( map.containsKey( key ) ) {
				return Collections.singletonList( map.get( key ) );
			}
		}

		if ( isAnyListPositionExpression( key ) ) {
			String listPropertyKey = getListPropertyKey( key );
			String listItemPropertyKey = getListItemPropertyKey( key );

			Object newEntity = StringUtils.isBlank( listPropertyKey )
					? entity
					: retrieveSingleValue( entity, listPropertyKey );

			if ( newEntity == null ) {
				return null;
			}

			if ( !( newEntity instanceof List ) ) {
				LOG.debug( "Field [{}] is not a List but {}", listPropertyKey, newEntity );
				return null;
			}

			if ( StringUtils.isBlank( listItemPropertyKey ) ) {
				return (List) newEntity;
			}
			else {
				List<Object> candidates = new LinkedList<Object>();

				for ( Object item : (List) newEntity ) {
					List<Object> itemMembers = retrieveValues( item, listItemPropertyKey );

					if ( itemMembers != null ) {
						candidates.addAll( itemMembers );
					}
				}

				return candidates;
			}
		}

		SpelExpressionParser parser = new SpelExpressionParser();
		StandardEvaluationContext evaluationContext = new StandardEvaluationContext( entity );
		evaluationContext.addPropertyAccessor( new MapAsPropertyAccessor() );
		evaluationContext.setRootObject( entity );

		try {
			Expression expression = parser.parseExpression( key );
			return nullify( Collections.singletonList( expression.getValue( evaluationContext ) ) );
		}
		catch ( SpelEvaluationException see ) {
			// cannot evaluate with SpEL, continuing with normal verification
		}
		catch ( NullPointerException npe ) {
			// cannot evaluate with SpEL, continuing with normal verification
		}

		// Null collection means the key was actually not found
		// this is different than empty collection or collection with null value
		return null;
	}

	/**
	 * If the list is a single element with null value, same as null list.
	 */
	private List<Object> nullify( List<Object> list ) {
		return list != null && list.size() == 1 && list.get( 0 ) == null ? null : list;
	}

	private String getListPropertyKey( String key ) {
		return StringUtils.substringBefore( key, "[]" );
	}

	private String getListItemPropertyKey( String key ) {
		String after = StringUtils.substringAfter( key, "[]" );

		if ( after.startsWith( "." ) ) {
			after = after.substring( 1 );
		}

		return after;
	}

	private boolean isAnyListPositionExpression( String key ) {
		return StringUtils.contains( key, "[]" );
	}

	@SuppressWarnings("unchecked")
	public boolean match( String label, Object actual, Object expected, boolean shouldFail ) {
		Object replaced = expected;

		if ( expected instanceof String ) {
			replaced = spel.getValue( (String) expected );
		}

		if ( replaced instanceof List && actualIsAlsoList( label, replaced, actual, shouldFail ) ) {
			return contains( label, (List) actual, (List) replaced, shouldFail );
		}
		else if ( replaced instanceof Map /*&& actualIsAlsoMap( label, replaced, actual, shouldFail )*/ ) {
			return contains( label, actual, (Map<String, ?>) replaced, shouldFail );
		}
		else {
			boolean matches = objectMatcher.matches( replaced, actual );

			if ( shouldFail ) {
				assertTrue(
						String.format( "Field [%s] failed: expected [%s] but got [%s]",
						               label, replaced, actual ),
						matches
				);
			}

			return matches;
		}
	}

	@SuppressWarnings("unchecked")
	protected boolean contains( String prefix, List source, List expected, boolean shouldFail ) {
		// For every expected item: find all candidates from the source that match
		Map<String, List<Integer>> matches = new HashMap<String, List<Integer>>();

		for ( int j = 0; j < expected.size(); j++ ) {
			String label = prefix + "[" + j + "]";

			Object needle = expected.get( j );
			List<Integer> positions = new LinkedList<Integer>();

			for ( int i = 0; i < source.size(); i++ ) {
				if ( match( label, source.get( i ), needle, false ) ) {
					positions.add( i );
				}
			}

			assertFalse( String.format( "Field [%s] failed: no match found", label ),
			             shouldFail && positions.isEmpty() );

			matches.put( label, positions );
		}

		if ( matches.size() > 1 ) {
			return checkUniqueCombinationIsPresent( prefix, matches, shouldFail );
		}

		return !matches.isEmpty();
	}

	private boolean checkUniqueCombinationIsPresent( String label,
	                                                 Map<String, List<Integer>> matches,
	                                                 boolean shouldFail ) {
		// A unique combination of positions should be present, so we did *not* use the same element
		// for matching against more than one expected element
		for ( Map.Entry<String, List<Integer>> entry : matches.entrySet() ) {
			for ( int i = 0; i < entry.getValue().size(); i++ ) {
				if ( isUnique( matches, entry.getKey(), i ) ) {
					return true;
				}
			}
		}

		// If we get here, no unique combination was found, we should fail
		if ( shouldFail ) {
			fail( String.format( "Field [%s] failed: unable to match all items", label ) );
		}

		return false;
	}

	private boolean isUnique( Map<String, List<Integer>> matches, String label, int startingPoint ) {
		for ( Map.Entry<String, List<Integer>> entry : matches.entrySet() ) {
			if ( !entry.getKey().equals( label ) ) {
				for ( int i = 0; i < entry.getValue().size(); i++ ) {
					Set<Integer> positions = new HashSet<Integer>();
					positions.add( matches.get( label ).get( startingPoint ) );
					positions.add( entry.getValue().get( i ) );

					for ( Map.Entry<String, List<Integer>> sub : matches.entrySet() ) {
						if ( !sub.getKey().equals( entry.getKey() ) ) {
							positions.add( sub.getValue().get( 0 ) );
						}
					}

					// If we have a different position for every match, unique combination was found
					if ( positions.size() == matches.size() ) {
						return true;
					}
				}
			}
		}

		// No unique combination was found
		return false;
	}

	private boolean actualIsAlsoList( String label, Object expected, Object actual, boolean shouldFail ) {
		if ( actual == null ) {
			assertFalse( String.format( "Field [%s] failed: expected [%s] but was null", label, expected ),
			             shouldFail );
			return false;
		}
		if ( !( actual instanceof List ) ) {
			assertFalse( String.format( "Field [%s] failed: expected a List but was [%s]", label, actual ),
			             shouldFail );
			return false;
		}

		return true;
	}

	private boolean actualIsAlsoMap( String label, Object expected, Object actual, boolean shouldFail ) {
		if ( actual == null ) {
			assertFalse( String.format( "Field [%s] failed: expected [%s] but was null", label, expected ),
			             shouldFail );
			return false;
		}
		if ( !( actual instanceof Map ) ) {
			assertFalse( String.format( "Field [%s] failed: expected a Map but was [%s]", label, actual ),
			             shouldFail );
			return false;
		}

		return true;
	}

	private boolean shouldCheckValue( Map.Entry<String, ?> entry ) {
		Object value = entry.getValue();

		if ( value == null ) {
			return false;
		}

		if ( value instanceof String ) {
			return StringUtils.isNotEmpty( (String) value );
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean doesNotContain( Map entity, Map<String, ?> values, boolean shouldFail ) {
		boolean contains = contains( entity, values, false );

		if ( shouldFail && contains ) {
			fail( "Values were found in the source" );
		}

		return !contains;
	}

	@SuppressWarnings("unchecked")
	public boolean doesNotContain( List<Object> entities, Map<String, ?> values, boolean shouldFail ) {
		boolean contains = contains( "", entities, values, false );

		if ( shouldFail && contains ) {
			fail( "Values were found in the source" );
		}

		return !contains;
	}
}
