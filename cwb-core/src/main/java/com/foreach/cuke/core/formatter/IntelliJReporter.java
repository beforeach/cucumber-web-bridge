/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.formatter;

import cucumber.runtime.StepDefinitionMatch;
import gherkin.formatter.model.*;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import javax.swing.*;
import java.lang.reflect.Field;

public class IntelliJReporter extends TeamcityStatusReporter
{
	private static final Logger LOG = LoggerFactory.getLogger( IntelliJReporter.class );

	public static final String PREFIX_SCENARIO = "SCENARIO ";
	public static final String PREFIX_FEATURE = "FEATURE  ";
	public static final String PREFIX_STEP = "STEP     ";
	public static final String PREFIX_BACKGROUND = "BACKGROUND";
	public static final String PREFIX_CUSTOM = " +          ";
	public static final String PROPERTY_PAUSE_ON_ERROR = "pauseOnError";

	private int featureCounter = 0, scenarioCounter = 0, stepCounter = 0;

	public IntelliJReporter( Appendable appendable ) {
		super( appendable );
	}

	@Override
	public void feature( Feature feature ) {
		super.feature( feature );

		MDC.put( "prefix", PREFIX_FEATURE );

		countFeature();

		LOG.info( "=========================================================" );
		LOG.info( feature.getName() );
		LOG.info( "=========================================================" );
		MDC.remove( "prefix" );

		LOG.info( "" );
	}

	@Override
	public void scenarioOutline( ScenarioOutline scenarioOutline ) {
		super.scenarioOutline( scenarioOutline );

		countScenario();

		LOG.info( "--------------------------------------------------------" );
		MDC.put( "prefix", PREFIX_SCENARIO );
		MDC.put( "scenarioId", scenarioOutline.getLine() );
		LOG.info( scenarioOutline.getName() );
		MDC.remove( "prefix" );
	}

	@Override
	public void background( Background background ) {
		super.background( background );

LOG.info( "--------------------------------------------------------" );
		MDC.put( "prefix", PREFIX_BACKGROUND );
//		MDC.put( "scenarioId", scenarioOutline.getLine() );
		LOG.info( "" );
		MDC.remove( "prefix" );
	}

	@Override
	public void scenario( Scenario scenario ) {
		super.scenario( scenario );

		countScenario();

		LOG.info( "--------------------------------------------------------" );
		MDC.put( "prefix", PREFIX_SCENARIO );
		MDC.put( "scenarioId", scenario.getLine() );
		LOG.info( scenario.getName() );
		MDC.remove( "prefix" );
	}


	@Override
	public void endOfScenarioLifeCycle( Scenario scenario ) {
		stepCounter = 0;
		createId();

		LOG.info( "--------------------------------------------------------" );
		clearId();

		LOG.info( "" );

		super.endOfScenarioLifeCycle( scenario );
	}

	@Override
	public void after( Match match, Result result ) {
		super.after( match, result );
		MDC.remove( "prefix" );
	}

	@Override
	public void match( Match match ) {
		super.match( match );

		if ( match instanceof StepDefinitionMatch ) {
			countStep();

			Field stepField = ReflectionUtils.findField( StepDefinitionMatch.class, "step" );
			stepField.setAccessible( true );

			StepDefinitionMatch stepDefinitionMatch = (StepDefinitionMatch) match;

			try {
				Step step = (Step) stepField.get( stepDefinitionMatch );

				MDC.put( "prefix", PREFIX_STEP );

				LOG.info( "{}{} [line: {}]", step.getKeyword(), step.getName(), step.getLine() );

				MDC.put( "prefix", PREFIX_CUSTOM );
			}
			catch ( Exception e ) {

			}
		}
	}

	@Override
	public void write( String text ) {
		LOG.info( text );
	}

	@Override
	public void result( Result result ) {
		super.result( result );
		if( result.getStatus().equals( Result.FAILED ) && BooleanUtils.toBoolean( System.getProperty( PROPERTY_PAUSE_ON_ERROR )) ){
			JFrame jf = new JFrame();

			JOptionPane.showMessageDialog( jf, result.getErrorMessage(), "Exception occurred!", JOptionPane.INFORMATION_MESSAGE );
			jf.toFront();
			jf.repaint();
		}
	}

	private void countFeature() {
		featureCounter++;
		scenarioCounter = 0;
		stepCounter = 0;
		createId();
	}

	private void countScenario() {
		scenarioCounter++;
		stepCounter = 0;
		createId();
	}

	private void countStep() {
		stepCounter++;
		createId();
	}

	private void createId() {
		MDC.put( "stepId", String.format( "%02d.%02d.%02d", featureCounter, scenarioCounter, stepCounter ) );
	}

	private void clearId() {
		MDC.remove( "stepId" );
	}
}
