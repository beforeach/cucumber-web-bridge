package com.foreach.cuke.core.scenario;

import cucumber.api.Scenario;
import org.apache.commons.lang3.StringUtils;

public abstract class SystemPropertyScenarioCallback implements ScenarioCallback
{

	@Override
	public final boolean accept( Scenario scenario ) {
		return StringUtils.isNotBlank( System.getProperty( getPropertyKey() ) );
	}

	@Override
	public void before( Scenario scenario ) {
	}

	@Override
	public void after( Scenario scenario ) {
	}

	protected abstract String getPropertyKey();
}
