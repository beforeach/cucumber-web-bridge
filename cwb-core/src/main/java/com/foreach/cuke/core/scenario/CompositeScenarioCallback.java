package com.foreach.cuke.core.scenario;

import cucumber.api.Scenario;

import java.util.LinkedList;

public abstract class CompositeScenarioCallback implements ScenarioCallback
{
	private LinkedList<ScenarioCallback> callbacks = new LinkedList<ScenarioCallback>();

	protected void addCallback( ScenarioCallback callback ) {
		this.callbacks.add( callback );
	}

	@Override
	public final boolean accept( Scenario scenario ) {
		for( ScenarioCallback callback : callbacks ){
			if( callback.accept( scenario )){
				return true;
			}
		}
		return false;
	}

	@Override
	public final void before( Scenario scenario ) {
		for( ScenarioCallback callback : callbacks ){
			if( callback.accept( scenario )){
				callback.before( scenario );
				break;
			}
		}
	}

	@Override
	public final void after( Scenario scenario ) {
		for( ScenarioCallback callback : callbacks ){
			if( callback.accept( scenario )){
				callback.after( scenario );
				break;
			}
		}
	}
}
