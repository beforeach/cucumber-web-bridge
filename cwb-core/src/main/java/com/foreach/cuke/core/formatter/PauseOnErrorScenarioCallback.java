package com.foreach.cuke.core.formatter;

import com.foreach.cuke.core.scenario.CompositeScenarioCallback;
import com.foreach.cuke.core.scenario.SystemPropertyScenarioCallback;
import com.foreach.cuke.core.scenario.TagScenarioCallback;
import com.foreach.cuke.core.spel.SpelEvaluationContext;
import cucumber.api.Scenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PauseOnErrorScenarioCallback extends CompositeScenarioCallback
{

	@Autowired
	private SpelEvaluationContext context;

	private void setSystemProperty( String pauseOnError ) {
		System.setProperty( IntelliJReporter.PROPERTY_PAUSE_ON_ERROR, pauseOnError );
	}

	private void enable(){
		setSystemProperty( "true" );
	}

	private void disable(){
		setSystemProperty( "false" );
	}

	@PostConstruct
	private void init(){

		addCallback( new SystemPropertyScenarioCallback()
		{
			@Override
			protected String getPropertyKey() {
				return "onerror.pause";
			}

			@Override
			public void before( Scenario scenario ) {
				enable();
			}

		} );

		addCallback( new TagScenarioCallback()
		{
			@Override
			protected SpelEvaluationContext getContext() {
				return context;
			}

			@Override
			protected String getTagName() {
				return "pauseOnError";
			}

			@Override
			protected void before( String argument ) {
				enable();
			}

			@Override
			protected void after( String argument ) {
				disable();
			}
		} );

	}
}
