/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.cli;

import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.GsonBuilder;
import gherkin.deps.com.google.gson.internal.StringMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.InputSource;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Arne Vandamme
 */
public class LogAggregator
{
	private final List<File> directories;
	private ItemTracker scenarioTracker = new ItemTracker();
	private ItemTracker stepTracker = new ItemTracker();
	public LogAggregator( List<File> directories ) {
		this.directories = directories;
	}

	public String aggregatedJson() {
		Map<Object, StringMap> aggregated = new LinkedHashMap<Object, StringMap>();

		Gson gson = new Gson();

		List<StringMap> output = new ArrayList<StringMap>();

		for ( File dir : directories ) {
			// JSON report
			File json = new File( dir, "cucumber-report.json" );

			if ( json.exists() && json.length() > 0 ) {
				try {
					List entries = gson.fromJson( FileUtils.readFileToString( json, "UTF-8" ), List.class );

					if ( entries != null ) {
						for ( Object entry : entries ) {
							StringMap featureMap = (StringMap) entry;
							Object featureId = featureMap.get( "id" );

							StringMap mergeTarget = aggregated.get( featureId );

							if ( mergeTarget == null ) {
								aggregated.put( featureId, featureMap );
							}
							else {
								Collection elements = (Collection) mergeTarget.get( "elements" );

								List<Object> newChildren = new ArrayList<Object>( elements );

								for ( Object child : (Collection) featureMap.get( "elements" ) ) {
									newChildren.add( child );
								}

								mergeTarget.put( "elements", newChildren );
							}
						}
					}
				}
				catch ( IOException ioe ) {
					throw new RuntimeException( ioe );
				}
			}
		}

		double backgroundDuration = 0;

		for ( Map.Entry<Object, StringMap> entry : aggregated.entrySet() ) {
			StringMap featureMap = entry.getValue();
			String featureFile = Objects.toString( featureMap.get( "uri" ) );

			for ( Object child : (Collection) featureMap.get( "elements" ) ) {
				Map element = (Map) child;

				if ( ObjectUtils.equals( element.get( "type" ), "background" ) ) {
					backgroundDuration = handleSteps( (Collection) element.get( "steps" ) );
				}
				else if ( ObjectUtils.equals( element.get( "type" ), "scenario" ) ) {
					double scenarioDuration = handleSteps( (Collection) element.get( "steps" ) );
					scenarioDuration += backgroundDuration;

					String scenario =
							featureFile + ":" + ( (Double) element.get( "line" ) ).intValue();

					scenarioTracker.log( scenario, scenarioDuration );

					backgroundDuration = 0;
				}
			}

			output.add( entry.getValue() );
		}

		return new GsonBuilder().setPrettyPrinting().create().toJson( output );
	}

	public Collection<TrackingRecord> getScenarioTrackingRecords() {
		List<TrackingRecord> list =
				new ArrayList<TrackingRecord>( scenarioTracker.items.values() );

		Collections.sort( list );

		return list;
	}

	public Collection<TrackingRecord> getStepTrackingRecords() {
		List<TrackingRecord> list =
				new ArrayList<TrackingRecord>( stepTracker.items.values() );

		Collections.sort( list );

		return list;
	}

	private double handleSteps( Collection steps ) {
		double totalDuration = 0;
		if ( steps != null ) {
			for ( Object step : steps ) {
				try {
					Map stepMap = (Map) step;
					Map stepResult = (Map) stepMap.get( "result" );

					String name = (String) stepMap.get( "name" );

					if ( stepResult != null ) {
						Double duration = (Double) stepResult.get( "duration" );
						if ( duration != null ) {
							// Only executed steps with a valid duration should be logged, else duration was 0 anyway
							stepTracker.log( name, duration );

							totalDuration += duration;
						}
					}
					else {
						System.err.println( "Step without result block: " + name );
					}
				}
				catch ( Exception e ) {

					System.err.println( "Exception handling step JSON entry" );
					e.printStackTrace();
				}
			}
		}

		return totalDuration;
	}

	/**
	 * JUnit XML.
	 */
	public String aggregatedXml() {
		Map<Object, Element> aggregated = new LinkedHashMap<Object, Element>();

		int totalSkipped = 0;

		for ( File dir : directories ) {
			File xml = new File( dir, "cucumber-report.xml" );

			if ( xml.exists() && xml.length() > 0 ) {
				InputStream inputStream = null;
				Reader reader = null;

				try {
					inputStream = new FileInputStream( xml );
					reader = new InputStreamReader( inputStream, "UTF-8" );

					InputSource is = new InputSource( reader );
					is.setEncoding( "UTF-8" );

					SAXReader saxParser = new SAXReader();
					Document document = saxParser.read( is );

					totalSkipped += Integer.parseInt( document.getRootElement().attributeValue( "skipped" ) );

					for ( Iterator i = document.getRootElement().elementIterator( "testcase" ); i.hasNext(); ) {
						Element node = (Element) i.next();

						String featureName = node.attributeValue( "classname" );
						String scenario = node.attributeValue( "name" );

						String testId = featureName + "-" + scenario;

						aggregated.put( testId, node );
					}

				}
				catch ( Exception de ) {
					throw new RuntimeException( de );
				}
				finally {
					IOUtils.closeQuietly( reader );
					IOUtils.closeQuietly( inputStream );
				}
			}
		}

		Document document = DocumentHelper.createDocument();

		int totalTests = 0;
		int totalFailures = 0;

		BigDecimal totalTime = new BigDecimal( 0 );

		Element root = document.addElement( "testsuite" );
		root.addAttribute( "name", "cucumber.runtime.formatter.JUnitFormatter" );

		for ( Map.Entry<Object, Element> entry : aggregated.entrySet() ) {
			Element node = entry.getValue();

			totalTests += 1;

			if ( node.elementIterator( "failure" ).hasNext() ) {
				totalFailures += 1;
			}

			String attributeValue = node.attributeValue( "time" );

			if ( attributeValue != null ) {
				BigDecimal time = new BigDecimal( attributeValue );
				totalTime = totalTime.add( time );
			}

			root.add( node.detach() );
		}

		root.addAttribute( "skipped", String.valueOf( totalSkipped ) );
		root.addAttribute( "tests", String.valueOf( totalTests ) );
		root.addAttribute( "failures", String.valueOf( totalFailures ) );
		root.addAttribute( "time", String.valueOf( totalTime ) );

		return document.asXML();
	}

	public static class ItemTracker
	{
		private Map<String, TrackingRecord> items = new HashMap<String, TrackingRecord>();

		public void log( String item, double duration ) {
			TrackingRecord record = items.get( item );

			if ( record == null ) {
				record = new TrackingRecord( item );
				items.put( item, record );
			}

			record.add( duration );
		}
	}

	public static class TrackingRecord implements Comparable<TrackingRecord>
	{
		private String name;
		private int count = 0;
		private double totalDuration, maxDuration, minDuration = Double.MAX_VALUE;

		public TrackingRecord( String name ) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public int getCount() {
			return count;
		}

		public double getTotalDuration() {
			return totalDuration / 1000000L;
		}

		public double getAverageDuration() {
			return ( totalDuration / count ) / 1000000L;
		}

		public double getMaxDuration() {
			return maxDuration / 1000000L;
		}

		public double getMinDuration() {
			return minDuration / 1000000L;
		}

		public void add( double duration ) {
			count++;

			totalDuration += duration;
			maxDuration = Math.max( maxDuration, duration );
			minDuration = Math.min( minDuration, duration );
		}

		@Override
		public int compareTo( TrackingRecord other ) {
			int value = Double.compare( other.totalDuration, totalDuration );

			if ( value == 0 ) {
				value = Integer.compare( other.count, count );
			}

			if ( value == 0 ) {
				value = Double.compare( other.maxDuration, maxDuration );
			}

			return value;
		}
	}
}
