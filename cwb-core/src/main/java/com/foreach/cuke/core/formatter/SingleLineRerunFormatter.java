/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.formatter;

import gherkin.formatter.Formatter;
import gherkin.formatter.NiceAppendable;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.*;

import java.util.*;

/**
 * Alternative to {@link cucumber.runtime.formatter.RerunFormatter} that writes a single entry per line,
 * instead of separating using whitespace.
 *
 * @author Arne Vandamme
 */
public class SingleLineRerunFormatter implements Formatter, Reporter
{
	private final NiceAppendable out;
	private String featureLocation;
	private Scenario scenario;
	private boolean isTestFailed = false;
	private Map<String, LinkedHashSet<Integer>>
			featureAndFailedLinesMapping = new HashMap<String, LinkedHashSet<Integer>>();

	public SingleLineRerunFormatter( Appendable out ) {
		this.out = new NiceAppendable( out );
	}

	@Override
	public void uri( String uri ) {
		this.featureLocation = uri;
	}

	@Override
	public void feature( Feature feature ) {
	}

	@Override
	public void background( Background background ) {
	}

	@Override
	public void scenario( Scenario scenario ) {
		this.scenario = scenario;
	}

	@Override
	public void scenarioOutline( ScenarioOutline scenarioOutline ) {
	}

	@Override
	public void examples( Examples examples ) {
	}

	@Override
	public void step( Step step ) {
	}

	@Override
	public void eof() {
	}

	@Override
	public void syntaxError( String state, String event, List<String> legalEvents, String uri, Integer line ) {
	}

	@Override
	public void done() {
		reportFailedScenarios();
	}

	private void reportFailedScenarios() {
		Set<Map.Entry<String, LinkedHashSet<Integer>>> entries = featureAndFailedLinesMapping.entrySet();
		boolean firstFeature = true;
		for ( Map.Entry<String, LinkedHashSet<Integer>> entry : entries ) {
			if ( entry.getValue().size() > 0 ) {
				if ( !firstFeature ) {
					out.append( System.lineSeparator() );
				}
				out.append( entry.getKey() );
				firstFeature = false;
				for ( Integer line : entry.getValue() ) {
					out.append( ":" ).append( line.toString() );
				}
			}
		}
	}

	@Override
	public void close() {
		this.out.close();
	}

	@Override
	public void startOfScenarioLifeCycle( Scenario scenario ) {
		isTestFailed = false;
	}

	@Override
	public void endOfScenarioLifeCycle( Scenario scenario ) {
		if ( isTestFailed ) {
			recordTestFailed();
		}
	}

	@Override
	public void before( Match match, Result result ) {
		if ( isTestFailed( result ) ) {
			isTestFailed = true;
		}
	}

	@Override
	public void result( Result result ) {
		if ( isTestFailed( result ) ) {
			isTestFailed = true;
		}
	}

	private boolean isTestFailed( Result result ) {
		String status = result.getStatus();
		return Result.FAILED.equals( status ) || Result.UNDEFINED.getStatus().equals( status ) || "pending".equals(
				status );
	}

	private void recordTestFailed() {
		LinkedHashSet<Integer> failedScenarios = this.featureAndFailedLinesMapping.get( featureLocation );
		if ( failedScenarios == null ) {
			failedScenarios = new LinkedHashSet<Integer>();
			this.featureAndFailedLinesMapping.put( featureLocation, failedScenarios );
		}

		failedScenarios.add( scenario.getLine() );
	}

	@Override
	public void after( Match match, Result result ) {
		if ( isTestFailed( result ) ) {
			isTestFailed = true;
		}
	}

	@Override
	public void match( Match match ) {
	}

	@Override
	public void embedding( String mimeType, byte[] data ) {
	}

	@Override
	public void write( String text ) {
	}

}
