/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.util;

import java.util.Arrays;

public class FileResource
{
	private String absolutePath;
	private String fileName;
	private byte[] content;

	public FileResource( String absolutePath, String fileName, byte[] newContent ) {
		this.absolutePath = absolutePath;
		this.fileName = fileName;
		if ( newContent == null ) {
			this.content = new byte[0];
		}
		else {
			this.content = Arrays.copyOf( newContent, newContent.length );
		}
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath( String absolutePath ) {
		this.absolutePath = absolutePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName( String fileName ) {
		this.fileName = fileName;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent( byte[] newContent ) {
		if ( newContent == null ) {
			this.content = new byte[0];
		}
		else {
			this.content = Arrays.copyOf( newContent, newContent.length );
		}
	}
}
