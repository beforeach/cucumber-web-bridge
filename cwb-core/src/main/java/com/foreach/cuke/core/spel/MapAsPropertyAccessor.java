/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.spel;

import org.springframework.expression.AccessException;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.PropertyAccessor;
import org.springframework.expression.TypedValue;

import java.util.Map;

public class MapAsPropertyAccessor implements PropertyAccessor
{
	@Override
	public Class<?>[] getSpecificTargetClasses() {
		return null;
	}

	@Override
	public boolean canRead( EvaluationContext evaluationContext, Object o, String s ) throws AccessException {
		if ( o instanceof Map ) {
			return ( (Map) o ).containsKey( s );
		}
		return false;
	}

	@Override
	public TypedValue read( EvaluationContext evaluationContext, Object o, String s ) throws AccessException {
		return new TypedValue( ( (Map) o ).get( s ) );
	}

	@Override
	public boolean canWrite( EvaluationContext evaluationContext, Object o, String s ) throws AccessException {
		return false;
	}

	@Override
	public void write( EvaluationContext evaluationContext, Object o, String s, Object o2 ) throws AccessException {

	}
}
