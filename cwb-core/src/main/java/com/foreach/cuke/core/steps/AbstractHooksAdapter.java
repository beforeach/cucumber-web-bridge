/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.steps;

import com.foreach.cuke.core.formatter.IntelliJReporter;
import com.foreach.cuke.core.scenario.ScenarioCallback;
import com.foreach.cuke.core.spel.SpelEvaluationContext;
import com.foreach.cuke.core.spel.SpelExpressionExecutor;
import cucumber.api.Scenario;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Abstract class to extend and define the common hooks.
 * Put an implementation in every steps package to improve auto detection of the common hooks.
 * <p/>
 * Simply override, annotate the methods with @Before or @After and call the super.
 *
 * @author Arne Vandamme
 */
@ContextConfiguration("classpath:cucumber.xml")
public abstract class AbstractHooksAdapter
{
	private static final Set<String> beforeSet = new HashSet<String>();
	private static final Set<String> afterSet = new HashSet<String>();

	protected final Logger LOG = LoggerFactory.getLogger( getClass() );

	@Autowired
	protected SpelEvaluationContext context;

	@Autowired
	protected SpelExpressionExecutor spel;

	@Autowired( required = false )
	private List<ScenarioCallback> callbacks;

	public synchronized void before( Scenario scenario ) {
		if ( !beforeSet.contains( scenario.getId() ) ) {
			beforeSet.add( scenario.getId() );
			// Reset evaluation context
			context.reset( scenario );

			if( callbacks != null ) {
				for ( ScenarioCallback callback : callbacks ) {
					if ( callback.accept( scenario ) ) {
						callback.before( scenario );
					}
				}
			}
		}
	}

	public synchronized void after( Scenario scenario ) {
		if ( !afterSet.contains( scenario.getId() ) ) {
			afterSet.add( scenario.getId() );
			if( callbacks != null ) {
				for ( ScenarioCallback callback : callbacks ) {
					if ( callback.accept( scenario ) ) {
						callback.after( scenario );
					}
				}
			}
			MDC.put( "prefix", IntelliJReporter.PREFIX_SCENARIO );
			if ( StringUtils.equalsIgnoreCase( "passed", scenario.getStatus() ) ) {
				LOG.info( "{} {}", StringUtils.upperCase( scenario.getStatus() ), scenario.getName() );
			}
			else {
				LOG.warn( "{} {}", StringUtils.upperCase( scenario.getStatus() ), scenario.getName() );
			}

			MDC.remove( "prefix" );
		}
	}
}
