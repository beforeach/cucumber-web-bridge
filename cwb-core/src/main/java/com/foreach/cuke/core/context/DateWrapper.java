/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.context;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.*;

/**
 * Wraps a Date instance providing utility functions for Spring EL.
 */
public class DateWrapper implements Comparable<DateWrapper>
{
	private Date date;

	public DateWrapper( Date date ) {
		this.date = date;
	}

	public Date getRawDate() {
		return date;
	}

	public long getValue() {
		return date.getTime();
	}

	public int getDate() {
		return part( date, Calendar.DATE );
	}

	public int getDay() {
		return part( date, Calendar.DATE );
	}

	public int getDayOfWeek() {
		return part( date, Calendar.DAY_OF_WEEK );
	}

	public int getDayOfYear() {
		return part( date, Calendar.DAY_OF_YEAR );
	}

	public int getMonth() {
		return part( date, Calendar.MONTH ) + 1;
	}

	public int getYear() {
		return part( date, Calendar.YEAR );
	}

	public int getHour() {
		return part( date, Calendar.HOUR_OF_DAY );
	}

	public int getMinute() {
		return part( date, Calendar.MINUTE );
	}

	public int getSecond() {
		return part( date, Calendar.SECOND );
	}

	public int getWeek() {
		return part( date, Calendar.WEEK_OF_YEAR );
	}

	public String format( String pattern ) {
		return FastDateFormat.getInstance( pattern, Locale.ENGLISH ).format( date );
	}

	public DateWrapper addDays( int amount ) {
		return new DateWrapper( DateUtils.addDays( date, amount ) );
	}

	public DateWrapper getNextMonday() {
		return findDay( 1, Calendar.MONDAY );
	}

	public DateWrapper getNextTuesday() {
		return findDay( 1, Calendar.TUESDAY );
	}

	public DateWrapper getNextWednesday() {
		return findDay( 1, Calendar.WEDNESDAY );
	}

	public DateWrapper getNextThursday() {
		return findDay( 1, Calendar.THURSDAY );
	}

	public DateWrapper getNextFriday() {
		return findDay( 1, Calendar.FRIDAY );
	}

	public DateWrapper getNextSaturday() {
		return findDay( 1, Calendar.SATURDAY );
	}

	public DateWrapper getNextSunday() {
		return findDay( 1, Calendar.SUNDAY );
	}

	public DateWrapper getLastMonday() {
		return findDay( -1, Calendar.MONDAY );
	}

	public DateWrapper getLastTuesday() {
		return findDay( -1, Calendar.TUESDAY );
	}

	public DateWrapper getLastWednesday() {
		return findDay( -1, Calendar.WEDNESDAY );
	}

	public DateWrapper getLastThursday() {
		return findDay( -1, Calendar.THURSDAY );
	}

	public DateWrapper getLastFriday() {
		return findDay( -1, Calendar.FRIDAY );
	}

	public DateWrapper getLastSaturday() {
		return findDay( -1, Calendar.SATURDAY );
	}

	public DateWrapper getLastSunday() {
		return findDay( -1, Calendar.SUNDAY );
	}

	public DateWrapper getThisMonth() {
		return new DateWrapper( DateUtils.truncate( date, Calendar.MONTH ) );
	}

	public DateWrapper getThisYear() {
		return new DateWrapper( DateUtils.truncate( date, Calendar.YEAR ) );
	}

	public DateWrapper addMonths( int amount ) {
		return new DateWrapper( DateUtils.addMonths( date, amount ) );
	}

	public DateWrapper addYears( int amount ) {
		return new DateWrapper( DateUtils.addYears( date, amount ) );
	}

	public DateWrapper addMinutes( int amount ) {
		return new DateWrapper( DateUtils.addMinutes( date, amount ) );
	}

	public DateWrapper addSeconds( int amount ) {
		return new DateWrapper( DateUtils.addSeconds( date, amount ) );
	}

	public DateWrapper addHours( int amount ) {
		return new DateWrapper( DateUtils.addHours( date, amount ) );
	}

	public DateWrapper addWeeks( int amount ) {
		return new DateWrapper( DateUtils.addWeeks( date, amount ) );
	}

	public boolean before( DateWrapper wrapper ) {
		return before( wrapper.date );
	}

	public boolean before( Date other ) {
		return date.getTime() < other.getTime();
	}

	public boolean after( DateWrapper wrapper ) {
		return after( wrapper.date );
	}

	public boolean after( Date other ) {
		return date.getTime() > other.getTime();
	}

	@Override
	public boolean equals( Object o ) {
		if ( this == o ) {
			return true;
		}
		if ( o == null || getClass() != o.getClass() ) {
			return false;
		}

		DateWrapper that = (DateWrapper) o;

		if ( date != null ? !date.equals( that.date ) : that.date != null ) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return date != null ? date.hashCode() : 0;
	}

	private DateWrapper findDay( int delta, int dayOfWeek ) {
		Date modified = date;

		do {
			modified = DateUtils.addDays( modified, delta );
		}
		while ( part( modified, Calendar.DAY_OF_WEEK ) != dayOfWeek );

		return new DateWrapper( modified );
	}

	/**
	 * Add the amount of days, specifying which days are allowed.
	 * Days will be added until amount of days in the set of allowed is added.
	 *
	 * @param amount
	 * @param allowedDays
	 * @return
	 */
	public DateWrapper addDays( int amount, String allowedDays ) {
		String[] dayNames = StringUtils.lowerCase( allowedDays ).split( "," );
		Collection<Integer> days = new LinkedList<Integer>();

		for ( String dayName : dayNames ) {
			days.add( getDayByName( dayName ) );
		}

		if ( days.isEmpty() ) {
			throw new RuntimeException( "No days parameter has been specified." );
		}

		int added = 0;

		Date modified = date;
		while ( added < amount ) {
			modified = DateUtils.addDays( modified, 1 );

			if ( days.contains( part( modified, Calendar.DAY_OF_WEEK ) ) ) {
				added++;
			}
		}

		return new DateWrapper( modified );
	}

	private int getDayByName( String name ) {
		if ( StringUtils.startsWithIgnoreCase( name, "mo" ) ) {
			return Calendar.MONDAY;
		}
		if ( StringUtils.startsWithIgnoreCase( name, "tu" ) ) {
			return Calendar.TUESDAY;
		}
		if ( StringUtils.startsWithIgnoreCase( name, "we" ) ) {
			return Calendar.WEDNESDAY;
		}
		if ( StringUtils.startsWithIgnoreCase( name, "th" ) ) {
			return Calendar.THURSDAY;
		}
		if ( StringUtils.startsWithIgnoreCase( name, "fr" ) ) {
			return Calendar.FRIDAY;
		}
		if ( StringUtils.startsWithIgnoreCase( name, "sa" ) ) {
			return Calendar.SATURDAY;
		}
		return Calendar.SUNDAY;
	}

	private int part( Date date, int field ) {
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek( Calendar.MONDAY );
		cal.setTime( date );

		return cal.get( field );
	}

	public int compareTo( DateWrapper o ) {
		return Long.valueOf( getValue() ).compareTo( o.getValue() );
	}

	@Override
	public String toString() {
		return format( "yyyy-MM-dd HH:mm:ss" );
	}
}
