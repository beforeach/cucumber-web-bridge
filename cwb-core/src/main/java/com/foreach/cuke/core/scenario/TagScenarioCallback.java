package com.foreach.cuke.core.scenario;

import com.foreach.cuke.core.spel.SpelEvaluationContext;
import cucumber.api.Scenario;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public abstract class TagScenarioCallback implements ScenarioCallback
{

	private static final String CONTEXT_VARIABLE_KEY = "tagNameArgumentPairs";

	@Override
	public final boolean accept( Scenario scenario ) {
		return nameArgumentPairs( scenario ).containsKey( getTagName() );
	}

	@Override
	public final void before( Scenario scenario ) {
		before( getArgument( scenario ) );
	}

	@Override
	public final void after( Scenario scenario ) {
		after( getArgument( scenario ));
	}

	protected void before( String argument ){
	}

	protected void after( String argument ){
	}

	protected abstract SpelEvaluationContext getContext();

	protected abstract String getTagName();

	protected final String getArgument( Scenario scenario ) {
		return nameArgumentPairs( scenario ).get( getTagName() );
	}

	private Map<String, String> nameArgumentPairs( Scenario scenario ) {
		if ( !getContext().getVariables().containsKey( CONTEXT_VARIABLE_KEY ) ) {
			Map<String, String> callbackNameArgumentPairs = new HashMap<String, String>();
			if ( scenario.getSourceTagNames() != null ) {
				for ( String tag : scenario.getSourceTagNames() ) {
					String[] nameAndArgument = StringUtils.split( tag, ":" );
					if ( ArrayUtils.isNotEmpty( nameAndArgument ) ) {
						String argument = nameAndArgument.length > 1 ? StringUtils.trim( nameAndArgument[1] ) : "";
						callbackNameArgumentPairs.put( StringUtils.trim( nameAndArgument[0].replace( "@", "" ) ),
						                               argument );
					}
				}
			}
			getContext().put( CONTEXT_VARIABLE_KEY, callbackNameArgumentPairs );
		}
		return getContext().lookup( CONTEXT_VARIABLE_KEY );
	}
}
