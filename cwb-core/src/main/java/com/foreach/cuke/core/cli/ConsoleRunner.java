/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.cli;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.commons.lang3.time.StopWatch;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper CLI class that provides a wrapper for {@link cucumber.api.cli.Main} and adds default formatters,
 * and support for multi-process executing based on feature file distribution and automatic reruns with
 * the more advanced tagged rerun functionality.
 *
 * @author Arne Vandamme
 */
public class ConsoleRunner
{
	private static final String VERSION = "v1.2.1";

	private static final Pattern PATTERN = Pattern.compile( "^(.*?)((:\\d+)+)$" );

	private Thread shutdownHook;

	private List<CwbBatch> callables = new ArrayList<CwbBatch>();
	private List<File> batchLogs = new LinkedList<File>();
	private Map<String, Integer> retryCounts = new HashMap<String, Integer>();
	private Map<String, String> featureToOriginalMap = new HashMap<String, String>();

	private String outputDirectory, inputFileOrDirectory;
	private int numberOfThreads = 1;
	private int numberOfRetries = 0;
	private int threadStartDelay = 1000;
	private int maxScenariosToRetry = 10;
	private int batchThreshold = 1;
	private long threadBlockedTimeout = 900000;
	private boolean keepScenarioOutlines = false;
	private boolean generateHtmlReports;
	private String encoding = "UTF-8";

	private FeatureBatchBuilder.Distribution distribution = FeatureBatchBuilder.Distribution.ROUND_ROBIN;

	private String[] glue = new String[0];
	private String[] tags = new String[0];
	private Properties properties = new Properties();
	private String classPath;

	public ConsoleRunner( String outputDirectory, String inputFileOrDirectory ) {
		this.outputDirectory = outputDirectory;
		this.inputFileOrDirectory = inputFileOrDirectory;
	}

	public String getOutputDirectory() {
		return outputDirectory;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding( String encoding ) {
		this.encoding = encoding;
	}

	public void setOutputDirectory( String outputDirectory ) {
		this.outputDirectory = outputDirectory;
	}

	public String getInputFileOrDirectory() {
		return inputFileOrDirectory;
	}

	public void setInputFileOrDirectory( String inputFileOrDirectory ) {
		this.inputFileOrDirectory = inputFileOrDirectory;
	}

	public int getNumberOfThreads() {
		return numberOfThreads;
	}

	public void setNumberOfThreads( int numberOfThreads ) {
		this.numberOfThreads = numberOfThreads;
	}

	public int getNumberOfRetries() {
		return numberOfRetries;
	}

	public void setNumberOfRetries( int numberOfRetries ) {
		this.numberOfRetries = numberOfRetries;
	}

	public String[] getGlue() {
		return glue;
	}

	public void setGlue( String[] glue ) {
		this.glue = glue != null ? glue : new String[0];
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags( String[] tags ) {
		this.tags = tags != null ? tags : new String[0];
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties( Properties properties ) {
		this.properties = properties != null ? properties : new Properties();
	}

	public int getThreadStartDelay() {
		return threadStartDelay;
	}

	public void setThreadStartDelay( int threadStartDelay ) {
		this.threadStartDelay = threadStartDelay;
	}

	public boolean isGenerateHtmlReports() {
		return generateHtmlReports;
	}

	public void setGenerateHtmlReports( boolean generateHtmlReports ) {
		this.generateHtmlReports = generateHtmlReports;
	}

	public FeatureBatchBuilder.Distribution getDistribution() {
		return distribution;
	}

	public void setDistribution( FeatureBatchBuilder.Distribution distribution ) {
		this.distribution = distribution;
	}

	public long getThreadBlockedTimeout() {
		return threadBlockedTimeout;
	}

	public void setThreadBlockedTimeout( long threadBlockedTimeout ) {
		this.threadBlockedTimeout = threadBlockedTimeout;
	}

	public int getMaxScenariosToRetry() {
		return maxScenariosToRetry;
	}

	public void setMaxScenariosToRetry( int maxScenariosToRetry ) {
		this.maxScenariosToRetry = maxScenariosToRetry;
	}

	public int getBatchThreshold() {
		return batchThreshold;
	}

	public void setBatchThreshold( int batchThreshold ) {
		this.batchThreshold = batchThreshold;
	}

	public boolean isMultiProcess() {
		return numberOfThreads > 1;
	}

	public boolean isKeepScenarioOutlines() {
		return keepScenarioOutlines;
	}

	public void setKeepScenarioOutlines( boolean keepScenarioOutlines ) {
		this.keepScenarioOutlines = keepScenarioOutlines;
	}

	public void setClassPath( String classPath ) {
		this.classPath = classPath;
	}

	public String getClassPath() {
		return classPath;
	}

	private boolean shouldRetry( int tries, List<String> scenariosToRun ) {
		return numberOfRetries >= tries && ( scenariosToRun.size() <= maxScenariosToRetry || maxScenariosToRetry == 0 );
	}

	/**
	 * Execute the process.
	 */
	public synchronized int start() throws Exception {
		buildBatchClassPath();

		if ( numberOfThreads > 1 ) {
			System.out.println( "Waiting " + threadStartDelay + " ms between process launches." );
		}

		batchLogs.clear();
		retryCounts.clear();

		System.out.println(
				"Executing tests in " + numberOfThreads + " threads with " + numberOfRetries + " retries." );
		if ( maxScenariosToRetry > 0 && numberOfRetries > 0 ) {
			System.out.println(
					"Execution will fail if more than " + maxScenariosToRetry + " scenarios need to be retried!" );
		}
		System.out.println(
				"Threads will be forcefully killed if they run longer than " + threadBlockedTimeout + " ms." );
		System.out.println();

		List<String> scenariosToRun = FeatureBatchBuilder.listScenarios( new File( inputFileOrDirectory ),
		                                                                 keepScenarioOutlines,
		                                                                 Arrays.asList( tags ) );

		int execution = 0;

		String directory = "initial";

		File output = FileUtils.getFile( getOutputDirectory() );

		if ( output.exists() ) {
			FileUtils.cleanDirectory( output );
		}

		scenariosToRun = createCucumberSafeCopies( scenariosToRun, output, featureToOriginalMap );

		registerShutdownHook();

		do {
			if ( execution++ > 0 ) {
				directory = "retry-" + ( execution - 1 );
				System.out.println();
				System.out.println(
						"Retry run " + ( execution - 1 ) + " with " + scenariosToRun.size() + " scenarios" );

				for ( String feature : scenariosToRun ) {
					registerRetry( feature );
				}
			}

			scenariosToRun = executeScenarios( output, directory, scenariosToRun );
		}
		while ( !scenariosToRun.isEmpty() && shouldRetry( execution, scenariosToRun ) );

		if ( !scenariosToRun.isEmpty() ) {
			System.out.println();

			if ( maxScenariosToRetry != 0 && scenariosToRun.size() > maxScenariosToRetry ) {
				System.out.println( "Not retrying as too many scenarios failed:" );
				System.out.println(
						scenariosToRun.size() + " failures but maximum " + maxScenariosToRetry + " allowed" );
				System.out.println();
			}
			System.out.println( "Stopping execution with " + scenariosToRun.size() + " failed tests remaining:" );

			List<String> originals = toOriginalFeatures( scenariosToRun );

			for ( String feature : originals ) {
				System.out.println( " " + feature );
			}

			FileUtils.writeLines( new File( output, "failed.txt" ), originals );
		}

		unregisterShutdownHook();

		writeAggregatedLogs( output );

		if ( generateHtmlReports ) {
			generateHtmlReports( output );
		}

		System.out.println();

		return scenariosToRun.isEmpty() ? 0 : -1;
	}

	private void buildBatchClassPath() {
		if ( classPath != null ) {
			System.out.println( "--------------------------" );
			System.out.println(
					"Using manually configured classpath for batches (" + classPath.length() + " characters)" );
			System.out.println( "--------------------------" );
			System.out.println();
		}
		else {
			URLClassLoader urlClassLoader = (URLClassLoader) Thread.currentThread().getContextClassLoader();
			URL[] urls = urlClassLoader.getURLs();

			List<String> paths = new ArrayList<String>( urls.length );
			for ( URL url : urls ) {
				paths.add( url.getFile() );
			}

			classPath = StringUtils.join( paths, ";" );
		}
	}

	private void generateHtmlReports( File output ) {
		System.out.println();
		System.out.println( "Generating pretty HTML reports" );

		File htmlDir = new File( output, "cucumber-html-reports" );
		String buildName = FastDateFormat.getInstance( "yyyy-MM-dd HH:mm:ss" ).format( new Date() );
		String projectName = "Cucumber console runner";

		if ( System.getProperty( "buildName" ) != null ) {
			buildName = System.getProperty( "buildName" );
		}
		if ( System.getProperty( "projectName" ) != null ) {
			projectName = System.getProperty( "projectName" );
		}

		try {
			StopWatch sw = new StopWatch();
			sw.start();

			Configuration configuration = new Configuration( output, projectName );
			configuration.setBuildNumber( buildName );
			File reportFile = new File( output, "cucumber-report.json" );
			ReportBuilder reportBuilder = new ReportBuilder(
					Collections.singletonList( reportFile.getAbsolutePath() ), configuration );
			reportBuilder.generateReports();

			FileUtils.copyFile( new File( htmlDir, "overview-features.html" ), new File( htmlDir, "index.html" ) );

			sw.stop();
			System.out.println( "HTML reports generated [" + sw.toString() + "]" );
		}
		catch ( Exception e ) {
			System.err.println( "Exception occurred generating HTML reports" );
			e.printStackTrace();
		}

		System.out.println();
	}

	private void registerShutdownHook() {
		if ( shutdownHook == null ) {
			final ConsoleRunner consoleRunner = this;

			shutdownHook = new Thread( new Runnable()
			{
				@Override
				public void run() {
					for ( CwbBatch batch : consoleRunner.callables ) {
						batch.destroy();
					}
				}
			} );

			Runtime.getRuntime().addShutdownHook( shutdownHook );
		}
	}

	private void unregisterShutdownHook() {
		if ( shutdownHook != null ) {
			Runtime.getRuntime().removeShutdownHook( shutdownHook );
			shutdownHook = null;
		}
	}

	private List<String> createCucumberSafeCopies( List<String> featuresToRun,
	                                               File output,
	                                               Map<String, String> safeToOriginal ) throws IOException {
		File targetDir = new File( output, "features" );
		targetDir.mkdir();

		Map<String, String> copied = new HashMap<String, String>();
		List<String> copies = new ArrayList<String>();

		String basePath = new File( inputFileOrDirectory ).getAbsolutePath();

		int index = 1;

		for ( String original : featuresToRun ) {
			String originalFileName = retrieveFileName( original );

			if ( !copied.containsKey( originalFileName ) ) {
				File dest = new File( targetDir, index++ + ".feature" );
				FileUtils.copyFile( new File( originalFileName ), dest );

				copied.put( originalFileName, dest.getAbsolutePath() );

				String relativeName =
						StringUtils.replace( StringUtils.replaceOnce( originalFileName, basePath, "" ), "\\", "/" );

				if ( relativeName.startsWith( "/" ) ) {
					relativeName = relativeName.substring( 1 );
				}

				safeToOriginal.put( dest.getAbsolutePath(), relativeName );
			}

			String safeFileName = copied.get( originalFileName );

			copies.add( safeFileName + retrieveScenarioSpec( original ) );
		}

		return copies;
	}

	/**
	 * Retrieve the filename part of a scenario string (eg: filename:12:13:14)
	 */
	String retrieveFileName( String scenarioSpec ) {
		Matcher matcher = PATTERN.matcher( scenarioSpec );

		if ( matcher.matches() ) {
			return matcher.group( 1 );
		}

		return scenarioSpec;
	}

	String retrieveScenarioSpec( String scenarioSpec ) {
		Matcher matcher = PATTERN.matcher( scenarioSpec );

		if ( matcher.matches() ) {
			return matcher.group( 2 );
		}

		return "";
	}

	private void registerRetry( String feature ) {
		String original = toOriginalFeature( feature );
		Integer count = retryCounts.get( original );

		retryCounts.put( original, count != null ? count + 1 : 1 );
	}

	private List<String> executeScenarios( File output,
	                                       String directory,
	                                       List<String> scenariosToRun ) throws IOException, InterruptedException, ExecutionException {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		FileUtils.writeLines( new File( output, directory + "/executed.txt" ), toOriginalFeatures( scenariosToRun ) );

		// Distribute all features in the number of batches
		List<List<String>> batches;

		if ( scenariosToRun.isEmpty() ) {
			System.out.println( "No scenarios to execute - skipping execution." );
			return Collections.emptyList();
		}

		if ( scenariosToRun.size() < batchThreshold ) {
			System.out.println( "Reverting to single thread execution: " );
			System.out.println(
					scenariosToRun.size() + " scenarios is below the batch threshold of " + batchThreshold + "" );
			System.out.println();
			batches = Collections.singletonList( scenariosToRun );
		}
		else {
			batches =
					FeatureBatchBuilder.distribute( scenariosToRun, getNumberOfThreads(), distribution );
		}

		ExecutorService executorService = Executors.newFixedThreadPool( batches.size() );

		for ( int i = 0; i < batches.size(); i++ ) {
			String batchName = directory + "/batch-" + ( i + 1 );
			File logDirectory = new File( output, batchName );

			batchLogs.add( logDirectory );

			CwbBatch cwbBatch = new CwbBatch( i, batchName, logDirectory, batches.get( i ), this );
			cwbBatch.setGlue( getGlue() );
			cwbBatch.setProperties( properties );
			cwbBatch.setStartDelay( threadStartDelay * i );

			callables.add( cwbBatch );
		}

		List<String> failed = new ArrayList<String>();
		List<Future<List<String>>> results = executorService.invokeAll( callables );

		for ( Future<List<String>> result : results ) {
			failed.addAll( result.get() );
		}

		executorService.shutdownNow();

		callables.clear();

		FileUtils.writeLines( new File( output, directory + "/failed.txt" ), toOriginalFeatures( failed ) );

		stopWatch.stop();
		System.out.println(
				scenariosToRun.size() + " scenarios executed - " + failed.size() + " failed [" + stopWatch.toString() + "]" );

		return failed;
	}

	List<String> mergeScenariosFromSameFeature( List<String> scenarioSpecs ) {
		Map<String, String> scenarioSpecByFeature = new LinkedHashMap<String, String>();

		for ( String spec : scenarioSpecs ) {
			String fileName = retrieveFileName( spec );
			String current = scenarioSpecByFeature.get( fileName );

			if ( current != null ) {
				current = current + retrieveScenarioSpec( spec );
			}
			else {
				current = spec;
			}

			scenarioSpecByFeature.put( fileName, current );
		}

		return new ArrayList<String>( scenarioSpecByFeature.values() );
	}

	public String toOriginalFeature( String feature ) {
		String exact = featureToOriginalMap.get( feature );

		if ( exact == null ) {
			Matcher matcher = PATTERN.matcher( feature );

			if ( matcher.matches() ) {
				String before = matcher.group( 1 );
				String after = matcher.group( 2 );

				String original = featureToOriginalMap.get( new File( before ).getAbsolutePath() );

				return original + after;
			}
		}

		return exact != null ? exact : feature;
	}

	public List<String> toOriginalFeatures( List<String> features ) {
		List<String> list = new ArrayList<String>( features.size() );
		for ( String feature : features ) {
			list.add( toOriginalFeature( feature ) );
		}

		return list;
	}

	private static class RetryRecord implements Comparable<RetryRecord>
	{
		private String name;
		private int retries;

		public RetryRecord( String name, int retries ) {
			this.name = name;
			this.retries = retries;
		}

		@Override
		public String toString() {
			return retries + "\t" + name;
		}

		@Override
		public int compareTo( RetryRecord other ) {
			int value = Integer.compare( other.retries, retries );

			return value != 0 ? value : name.compareTo( other.name );
		}
	}

	private void writeAggregatedLogs( File output ) throws IOException {
		LogAggregator aggregator = new LogAggregator( batchLogs );

		// First write JUnit XML
		FileUtils.write( new File( output, "cucumber-report.xml" ), aggregator.aggregatedXml(), "UTF-8" );
		FileUtils.write( new File( output, "cucumber-report.json" ), aggregator.aggregatedJson(), "UTF-8" );

		if ( !retryCounts.isEmpty() ) {
			List<String> lines = new ArrayList<String>( retryCounts.size() );
			SortedSet<RetryRecord> records = new TreeSet<RetryRecord>();
			lines.add( "retries\tscenario" );

			for ( Map.Entry<String, Integer> retry : retryCounts.entrySet() ) {
				records.add( new RetryRecord( retry.getKey(), retry.getValue() ) );

			}

			for ( RetryRecord record : records ) {
				lines.add( record.toString() );
			}

			FileUtils.writeLines( new File( output, "retries.txt" ), lines );
		}

		Collection<LogAggregator.TrackingRecord> scenarios = aggregator.getScenarioTrackingRecords();

		List<String> lines = new ArrayList<String>( scenarios.size() );
		lines.add( "total\tcount\tavg\tmin\tmax\tname" );
		for ( LogAggregator.TrackingRecord scenario : scenarios ) {
			String line = Double.valueOf( scenario.getTotalDuration() ).longValue()
					+ "\t" + scenario.getCount()
					+ "\t" + Double.valueOf( scenario.getAverageDuration() ).longValue()
					+ "\t" + Double.valueOf( scenario.getMinDuration() ).longValue()
					+ "\t" + Double.valueOf( scenario.getMaxDuration() ).longValue()
					+ "\t" + toOriginalFeature( scenario.getName() );

			lines.add( line );
		}

		FileUtils.writeLines( new File( output, "scenario-stats.txt" ), lines );

		Collection<LogAggregator.TrackingRecord> steps = aggregator.getStepTrackingRecords();

		lines.clear();
		lines.add( "total\tcount\tavg\tmin\tmax\tname" );
		for ( LogAggregator.TrackingRecord step : steps ) {
			if ( lines.size() <= 200 ) {
				String line = Double.valueOf( step.getTotalDuration() ).longValue()
						+ "\t" + step.getCount()
						+ "\t" + Double.valueOf( step.getAverageDuration() ).longValue()
						+ "\t" + Double.valueOf( step.getMinDuration() ).longValue()
						+ "\t" + Double.valueOf( step.getMaxDuration() ).longValue()
						+ "\t" + step.getName();

				lines.add( line );
			}
		}

		FileUtils.writeLines( new File( output, "scenario-steps-top-200.txt" ), lines );
	}

	public static void main( String[] args ) throws Exception {
		Options options = new Options();
		options.addOption( "help", false, "print out this message" );
		options.addOption( "output", true, "REQUIRED base directory for generated logs and test results" );
		options.addOption( "threads", true,
		                   "REQUIRED number of threads for parallel execution - every thread will start a new process" );
		options.addOption( "startDelay", true, "ms delay between starting threads (default: 250)" );
		options.addOption( "retries", true, "REQUIRED number of retries for failed tests" );
		options.addOption( "glue", true, "package containing glue classes (multiple allowed)" );
		options.addOption( "tags", true, "tags the scenarios should match (multiple allowed) - " +
				"separate options mean AND, comma separated values mean OR" );
		options.addOption( "distribution", true,
		                   "batch distribution mechanism, CHUNK or ROUND_ROBIN (default: ROUND_ROBIN)" );
		options.addOption( "processTimeout", true,
		                   "timeout in seconds before a batch process will be considered blocked and killed off forcefully (default: 900)" );
		options.addOption( "disableHtmlReports", false, "do not generate the html reports" );
		options.addOption( "scenarioRetryLimit", true,
		                   "maximum number of scenarios that are allowed to be retried (default: 10) - set to 0 to retry everything" );
		options.addOption( "batchThreshold", true,
		                   "minimum scenarios that are required before multiple threads should be used (default: 1)" );
		options.addOption( "keepScenarioOutlines", false, "do not separate scenario outline examples across batches" );
		options.addOption( "encoding", true, "file encoding to use for feature files (default: UTF-8)" );
		options.addOption( "batchClassPath", true,
		                   "classpath to be passed to the processes executing a batch of tests" );
		options.addOption( "folders", true,
		                   "comma separated list of folders in the <features-directory> - will be executed as separate batches and only if they keep succeeding" );

		options.addOption(
				OptionBuilder.withArgName( "property=value" )
						.hasArgs( 2 )
						.withValueSeparator()
						.withDescription( "pass system property" )
						.create( "P" )
		);

		options.getOption( "output" ).setRequired( true );

		CommandLineParser parser = new BasicParser();
		CommandLine cmd = parser.parse( options, args );

		if ( cmd.hasOption( "help" ) || cmd.getArgs().length < 1 ) {
			HelpFormatter helpFormatter = new HelpFormatter();
			helpFormatter.printHelp( "com.foreach.cuke.core.cli.ConsoleRunner <features-directory>", options );
		}
		else {
			if ( cmd.getArgs().length != 1 ) {
				throw new IllegalArgumentException(
						"ConsoleRunner requires exactly 1 argument which is the directory containing all features." );
			}

			String[] folders = new String[] { "" };

			if ( cmd.hasOption( "folders" ) ) {
				String concatenatedFolders = cmd.getOptionValue( "folders" );
				if ( !StringUtils.isBlank( concatenatedFolders ) && !".".equals( concatenatedFolders ) ) {
					folders = StringUtils.split( concatenatedFolders, "," );
				}
			}

			for ( String folder : folders ) {
				String outputDirectory = StringUtils.isBlank( folder ) ? cmd.getOptionValue( "output" )
						: new File( cmd.getOptionValue( "output" ), folder ).getAbsolutePath();
				String inputFileOrDirectory = StringUtils.isBlank( folder )
						? cmd.getArgs()[0] : new File( cmd.getArgs()[0], folder ).getAbsolutePath();

				ConsoleRunner runner = new ConsoleRunner( outputDirectory, inputFileOrDirectory );
				runner.setNumberOfThreads( Integer.parseInt( cmd.getOptionValue( "threads" ) ) );
				runner.setNumberOfRetries( Integer.parseInt( cmd.getOptionValue( "retries" ) ) );
				runner.setGlue( cmd.getOptionValues( "glue" ) );
				runner.setTags( cmd.getOptionValues( "tags" ) );
				runner.setProperties( cmd.getOptionProperties( "P" ) );

				if ( cmd.hasOption( "startDelay" ) ) {
					runner.setThreadStartDelay( Integer.parseInt( cmd.getOptionValue( "startDelay" ) ) );
				}
				if ( cmd.hasOption( "distribution" ) ) {
					runner.setDistribution(
							FeatureBatchBuilder.Distribution.valueOf( cmd.getOptionValue( "distribution" ) ) );
				}
				if ( cmd.hasOption( "processTimeout" ) ) {
					runner.setThreadBlockedTimeout(
							Integer.parseInt( cmd.getOptionValue( "processTimeout" ) ) * 1000L );
				}
				if ( cmd.hasOption( "scenarioRetryLimit" ) ) {
					runner.setMaxScenariosToRetry( Integer.parseInt( cmd.getOptionValue( "scenarioRetryLimit" ) ) );
				}
				if ( cmd.hasOption( "batchThreshold" ) ) {
					runner.setBatchThreshold( Integer.parseInt( cmd.getOptionValue( "batchThreshold" ) ) );
				}
				if ( cmd.hasOption( "encoding" ) ) {
					runner.setEncoding( cmd.getOptionValue( "encoding" ) );
				}
				if ( cmd.hasOption( "batchClassPath" ) ) {
					runner.setClassPath( cmd.getOptionValue( "batchClassPath" ) );
				}

				runner.setKeepScenarioOutlines( cmd.hasOption( "keepScenarioOutlines" ) );
				runner.setGenerateHtmlReports( !cmd.hasOption( "disableHtmlReports" ) );

				if ( StringUtils.isBlank( folder ) ) {
					System.out.println( "Starting CWB console runner " + VERSION );
				}
				else {
					System.out.println( "Starting CWB Console runner " + VERSION + " for subfolder: " + folder );
					System.out.println( "Location: " + inputFileOrDirectory );
				}

				int statusCode = runner.start();

				if ( statusCode != 0 ) {
					System.exit( statusCode );
				}
			}

			System.exit( 0 );
		}
	}
}
