/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.fail;

@Component
public class FileResourceResolver
{
	protected final Logger LOG = LoggerFactory.getLogger( getClass() );

	public FileResource resolve( String fileToUpload ) {
		try {
			String alternateName = null;
			String fileName = StringUtils.trim( fileToUpload );

			if ( fileToUpload.contains( "::" ) ) {
				String[] parts = fileToUpload.split( "::" );
				alternateName = StringUtils.trim( parts[0] );
				fileName = StringUtils.trim( parts[1] );
			}

			LOG.debug( "Uploading file {} as {}", fileName, alternateName );

			File absoluteFile;

			if ( !fileName.contains( ":" ) ) {
				// Relative path to resource file
				Resource resource = new ClassPathResource( fileName );
				absoluteFile = resource.getFile();

				if ( !resource.exists() ) {
					fail( "Did not find the file to upload: " + absoluteFile.getAbsolutePath() );
				}

				LOG.debug( "Uploading file {}", absoluteFile.getAbsolutePath() );
			}
			else {
				absoluteFile = new File( fileName );
			}

			// If an alternate name is specified, create a copy and upload that file
			if ( StringUtils.isNotBlank( alternateName ) ) {
				String tempDir = System.getProperty( "java.io.tmpdir" );

				File destination = new File( tempDir, alternateName );
				FileUtils.copyFile( absoluteFile, destination );
				Path path = Paths.get( destination.toURI() );
				return new FileResource( destination.getAbsolutePath(), destination.getName(), Files.readAllBytes( path ) );
			}
			else {
				Path path = Paths.get( absoluteFile.toURI() );
				return new FileResource( absoluteFile.getAbsolutePath(), absoluteFile.getName(), Files.readAllBytes( path ) );
			}
		}
		catch ( Exception e ) {
			throw new RuntimeException( e );
		}
	}
}
