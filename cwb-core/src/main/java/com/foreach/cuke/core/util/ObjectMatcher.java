/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.util;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ObjectMatcher
{
	@Autowired
	private StringMatcher stringMatcher;

	public boolean matches( Object expected, Object actual ) {
		if ( expected == null && actual == null ) {
			return true;
		}
		else if ( actual == null ) {
			return false;
		}
		if ( expected instanceof Boolean ) {
			actual = Boolean.valueOf( ObjectUtils.toString( actual ) );
		}
		else if ( expected instanceof Number ) {
			expected = expected.toString();
			if( actual instanceof Number ) {
				actual =  actual.toString();
			}
		}
		else if ( expected instanceof String ) {
			return stringMatcher.equals( (String) expected, ObjectUtils.toString( actual ) );
		}

		return ObjectUtils.equals( expected, actual );
	}
}
