/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertiesFactoryBean;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public class SystemPropertiesOverridenPropertiesFactoryBean extends PropertiesFactoryBean
{
	private static final Logger LOG = LoggerFactory.getLogger(SystemPropertiesOverridenPropertiesFactoryBean.class);

	protected Properties createProperties() throws IOException {
		Properties createdProperties = super.createProperties();
		for( Map.Entry<Object, Object> entry : createdProperties.entrySet() ) {
			String key = (String) entry.getKey();
			String systemPropertyValue = System.getProperty( key );
			if( systemPropertyValue != null ) {
				LOG.info( "Overriding property {} with value {} because it is specified as a system property", key, systemPropertyValue );
				createdProperties.setProperty( key, systemPropertyValue );
			}
		}
		return createdProperties;
	}
}
