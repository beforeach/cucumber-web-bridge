/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;

/**
 * All methods allow the expected value to be specified as a sahi regex:
 * <ul>
 * <li>expected = non-regex: normal action equality/contains/starts with/end with</li>
 * <li>expected/i = non-regex: normal action but ignore case</li>
 * <li>/expected/ = regex: original should contain expected</li>
 * <li>/^expected/ = regex: original should start with expected</li>
 * <li>/expected$/ = regex: original should end with expected</li>
 * <li>/^EXPECTED$/i = regex: original should equal expected but casing is not important</li>
 * </ul>
 */
public class StringMatcher
{
	private static final Logger LOG = LoggerFactory.getLogger( StringMatcher.class );

	private static final class Cleaned
	{
		private final String originalText;
		private final String cleanedText;
		private final boolean shouldIgnoreCase;
		private final boolean isRegex;

		private Cleaned( String originalText, String cleanedText, boolean shouldIgnoreCase, boolean regex ) {
			this.originalText = originalText;
			this.cleanedText = cleanedText;
			this.shouldIgnoreCase = shouldIgnoreCase;
			isRegex = regex;
		}
	}

	private boolean ignoreCaseByDefault = true;

	public StringMatcher() {
		this( true );
	}

	public StringMatcher( boolean ignoreCaseByDefault ) {
		this.ignoreCaseByDefault = ignoreCaseByDefault;
	}

	public boolean isIgnoreCaseByDefault() {
		return ignoreCaseByDefault;
	}

	public void setIgnoreCaseByDefault( boolean ignoreCaseByDefault ) {
		this.ignoreCaseByDefault = ignoreCaseByDefault;
	}

	public boolean equals( String original, String expected ) {
		Cleaned cleaned = clean( expected );

		if ( cleaned.isRegex ) {
			return matches( original, cleaned );
		}

		LOG.trace( "** string matching: [{}] equals [{}]", original, cleaned.originalText );

		return cleaned.shouldIgnoreCase ? StringUtils.equalsIgnoreCase( original,
		                                                                cleaned.cleanedText ) : StringUtils.equals(
				original, cleaned.cleanedText );
	}

	public boolean contains( String original, String expected ) {
		Cleaned cleaned = clean( expected );

		if ( cleaned.isRegex ) {
			return matches( original, cleaned );
		}

		LOG.debug( "** string matching: [{}] contains [{}]", original, cleaned.originalText );

		return cleaned.shouldIgnoreCase ? StringUtils.containsIgnoreCase( original,
		                                                                  cleaned.cleanedText ) : StringUtils.contains(
				original, cleaned.cleanedText );
	}

	public boolean startsWith( String original, String expected ) {
		Cleaned cleaned = clean( expected );

		if ( cleaned.isRegex ) {
			return matches( original, cleaned );
		}

		LOG.debug( "** string matching: [{}] starts with [{}]", original, cleaned.originalText );

		return cleaned.shouldIgnoreCase ? StringUtils.startsWithIgnoreCase( original,
		                                                                    cleaned.cleanedText ) : StringUtils.startsWith(
				original, cleaned.cleanedText );
	}

	public boolean endsWith( String original, String expected ) {
		Cleaned cleaned = clean( expected );

		if ( cleaned.isRegex ) {
			return matches( original, cleaned );
		}

		LOG.debug( "** string matching: [{}] ends with [{}]", original, cleaned.originalText );

		return cleaned.shouldIgnoreCase ? StringUtils.endsWithIgnoreCase( original,
		                                                                  cleaned.cleanedText ) : StringUtils.endsWith(
				original, cleaned.cleanedText );
	}

	public boolean matches( String original, String expected ) {
		return matches( original, clean( expected ) );
	}

	private boolean matches( String original, Cleaned cleaned ) {
		String converted = cleaned.cleanedText;

		if ( !StringUtils.startsWith( converted, "^" ) ) {
			converted = ".*" + converted;
		}
		if ( !StringUtils.endsWith( converted, "$" ) ) {
			converted = converted + ".*";
		}

		Pattern pattern;

		LOG.debug( "** string matching: [{}] matches [{}] [{} - ci: {}]", original, cleaned.originalText, converted,
		           cleaned.shouldIgnoreCase );

		if ( cleaned.shouldIgnoreCase ) {
			pattern = Pattern.compile( converted, Pattern.CASE_INSENSITIVE );

			return pattern.matcher( original ).matches();
		}
		else {
			return Pattern.matches( converted, original );
		}
	}

	private Cleaned clean( String expected ) {
		boolean isRegex = StringUtils.startsWith( expected, "/" ) && StringUtils.endsWithAny( expected, "/", "/i" );
		boolean shouldIgnoreCase = StringUtils.endsWith( expected, "/i" );

		String cleaned = expected;

		if ( isRegex ) {
			cleaned = StringUtils.strip( expected, "/" );
		}

		if ( shouldIgnoreCase ) {
			cleaned = StringUtils.stripEnd( cleaned, "/i" );
		}

		return new Cleaned( expected, cleaned, shouldIgnoreCase || ignoreCaseByDefault, isRegex );
	}
}
