/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.cli;

import jlibs.core.lang.JavaProcessBuilder;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * Single Cucumber batch of features that returns the list of features that should be retried.
 *
 * @author Arne Vandamme
 */
public class CwbBatch implements Callable<List<String>>
{
	private final int number;
	private final long timeout;
	private final String name;
	private final File outputDirectory;
	private final List<String> features;
	private final ConsoleRunner consoleRunner;

	private int startDelay = 0;
	private String[] glue = new String[0];
	private Properties properties = new Properties();

	private ExecuteWatchdog watchdog;

	public CwbBatch( int number,
	                 String name,
	                 File outputDirectory,
	                 List<String> features,
	                 ConsoleRunner consoleRunner ) {
		this.number = number;
		this.consoleRunner = consoleRunner;
		this.name = name;
		this.features = features;
		this.outputDirectory = outputDirectory;
		this.timeout = consoleRunner.getThreadBlockedTimeout();
	}

	public void setGlue( String[] glue ) {
		this.glue = glue;
	}

	public void setProperties( Properties properties ) {
		this.properties = properties;
	}

	public void setStartDelay( int startDelay ) {
		this.startDelay = startDelay;
	}

	public void destroy() {
		if ( watchdog != null && watchdog.isWatching() ) {
			System.err.println( "Shutdown in process - manual destruction of " + name );
			watchdog.destroyProcess();
		}
	}

	@Override
	public List<String> call() throws Exception {
		List<String> failedInBatch = Collections.emptyList();

		try {
			Thread.sleep( startDelay );

			StopWatch sw = new StopWatch();
			sw.start();

			System.out.println( "Executing batch: " + name + " - " + features.size() + " scenarios" );

			outputDirectory.mkdirs();

			FileUtils.writeLines( new File( outputDirectory, "executed.txt" ),
			                      consoleRunner.toOriginalFeatures( features ) );

			JavaProcessBuilder jvm = new JavaProcessBuilder();

			jvm.classpath( consoleRunner.getClassPath() );
			jvm.mainClass( "cucumber.api.cli.Main" );
			jvm.workingDir( outputDirectory );

			jvm.arg( "--format com.foreach.cuke.core.formatter.ConsoleReporter" );
			jvm.arg( "--format json:" + new File( outputDirectory, "cucumber-report.json" ).getAbsolutePath() );
			jvm.arg( "--format com.foreach.cuke.core.formatter.ExtendedJUnitFormatter:"
					         + new File( outputDirectory, "cucumber-report.xml" ).getAbsolutePath() );
			jvm.arg( "--format com.foreach.cuke.core.formatter.SingleLineRerunFormatter:_rerun.txt" );

			if ( glue.length == 0 ) {
				jvm.arg( "--glue com.foreach.cuke" );
			}
			else {
				for ( String g : glue ) {
					jvm.arg( "--glue " + g );
				}
			}
			if ( !properties.containsKey( "nopause" ) ) {
				properties.put( "nopause", "true" );
			}

			jvm.systemProperty( "file.encoding", consoleRunner.getEncoding() );
			jvm.systemProperty( "threadNo", String.valueOf( number ) );

			for ( Map.Entry<Object, Object> property : properties.entrySet() ) {
				jvm.systemProperty( (String) property.getKey(), (String) property.getValue() );
			}

			File cucumberInput = new File( outputDirectory, "_input.txt" );

			FileUtils.writeStringToFile( cucumberInput,
			                             StringUtils.join( consoleRunner.mergeScenariosFromSameFeature( features ),
			                                               " " ),
			                             "UTF-8" );
			jvm.arg( "@" + cucumberInput.getAbsolutePath() );

			FileOutputStream fos = new FileOutputStream( new File( outputDirectory, "cucumber-batch.log" ), false );

			String cmd = StringUtils.join( jvm.command(), " " );

			List<String> cmdLog = new ArrayList<String>();
			cmdLog.add( cmd );
			for ( Map.Entry<String, String> entry : jvm.systemProperties().entrySet() ) {
				cmdLog.add( "-D" + entry.getKey() + "=" + entry.getValue() );
			}

			FileUtils.writeLines( new File( outputDirectory, "command.txt" ), cmdLog );

			// todo: add props

			try {
				DefaultExecutor executor = new DefaultExecutor();
				executor.setWorkingDirectory( outputDirectory );
				executor.setStreamHandler( new PumpStreamHandler( fos ) );
				executor.setExitValues( new int[] { 0, -1, 1 } );

				watchdog = new ExecuteWatchdog( timeout );
				executor.setWatchdog( watchdog );

				executor.execute( CommandLine.parse( cmd ), jvm.systemProperties() );

				if ( watchdog.killedProcess() ) {
					System.err.println( "Killed batch process: " + name + " (timeout of " + timeout + " exceeded)" );
				}

			}
			finally {
				IOUtils.closeQuietly( fos );
			}

			File failed = new File( outputDirectory, "_rerun.txt" );

			if ( failed.exists() ) {
				failedInBatch = FileUtils.readLines( failed, "UTF-8" );
			}
			else {
				// Assume all have failed
				failedInBatch = features;
			}

			FileUtils.writeLines( new File( outputDirectory, "failed.txt" ),
			                      consoleRunner.toOriginalFeatures( failedInBatch ) );

			sw.stop();
			System.out.println(
					"Batch finished: " + name + " - " + failedInBatch.size() + " failed [" + sw.toString() + "]" );
		}
		catch ( Exception e ) {
			System.err.println( "Exception occurred in batch: " + name );
			e.printStackTrace();
		}

		return failedInBatch;
	}
}
