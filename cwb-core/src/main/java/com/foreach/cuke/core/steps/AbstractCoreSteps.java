/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.steps;

import com.foreach.cuke.core.spel.SpelEvaluationContext;
import com.foreach.cuke.core.spel.SpelExpressionExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration("classpath:cucumber.xml")
public abstract class AbstractCoreSteps
{
	protected final Logger LOG = LoggerFactory.getLogger( getClass() );

	@Autowired
	protected SpelEvaluationContext context;

	@Autowired
	protected SpelExpressionExecutor spel;

}
