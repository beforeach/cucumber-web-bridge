/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.spel;

import com.foreach.cuke.core.context.DateValues;
import com.foreach.cuke.core.context.IdValues;
import cucumber.api.Scenario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.expression.AccessException;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.PropertyAccessor;
import org.springframework.expression.TypedValue;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Component
public final class SpelEvaluationContext
{
	private static final Logger LOG = LoggerFactory.getLogger( SpelEvaluationContext.class);

	@Value("${environment}")
	private String environment;

	@Autowired
	@Qualifier("dataProperties")
	private Properties properties;

	private DateValues dateValues = new DateValues();
	private IdValues idValues = new IdValues();

	private Map<String, Object> scenarioVariables = new HashMap<String, Object>();

	public String getEnvironment() {
		return StringUtils.isEmpty( environment ) ? "default" : environment;
	}

	public Properties getProperties() {
		return properties;
	}

	public Map<String, Object> getVariables() {
		return scenarioVariables;
	}

	public IdValues getId() {
		return idValues;
	}

	public DateValues getDate() {
		return dateValues;
	}

	public void put( String key, Object object ) {
		LOG.debug( "saving scenario variable: {} - {}", key, object );
		scenarioVariables.put( key, object );
	}

	@SuppressWarnings( "unchecked" )
	public <T> T lookup( String name ) {
		T result = (T) ( scenarioVariables.containsKey( name ) ? scenarioVariables.get( name ) : properties.getProperty(
				name ) );

		LOG.debug( "looking up scenario variable {}: {}", name, result );
		return result;
	}

	public String lookupString( String name ) {
		return lookup( name );
	}

	public void reset( Scenario scenario ) {
		scenarioVariables.clear();
		idValues.updateScenario( scenario );

		LOG.info( "(generated scenario id: {})", idValues.getScenario() );
	}

	/**
	 * @return PropertyAccessor that allows accessing scenario variables as properties.
	 */
	public PropertyAccessor getPropertyAccessor() {
		return new PropertyAccessor()
		{
			@Override
			public Class<?>[] getSpecificTargetClasses() {
				return new Class[] { SpelEvaluationContext.class };
			}

			@Override
			public boolean canRead( EvaluationContext context, Object target, String name ) throws AccessException {
				return scenarioVariables.containsKey( name );
			}

			@Override
			public TypedValue read( EvaluationContext context, Object target, String name ) throws AccessException {
				return new TypedValue( scenarioVariables.get( name ) );
			}

			@Override
			public boolean canWrite( EvaluationContext context, Object target, String name ) throws AccessException {
				return false;
			}

			@Override
			public void write( EvaluationContext context,
			                   Object target,
			                   String name,
			                   Object newValue ) throws AccessException {

			}
		};
	}
}
