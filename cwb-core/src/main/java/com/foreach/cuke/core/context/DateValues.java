/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.context;

import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class DateValues
{
	public DateWrapper getNow() {
		return new DateWrapper( new Date() );
	}

	public DateWrapper getToday() {
		return new DateWrapper( DateUtils.truncate( new Date(), Calendar.DATE ) );
	}

	public DateWrapper getYesterday() {
		return getToday().addDays( -1 );
	}

	public DateWrapper getTomorrow() {
		return getToday().addDays( 1 );
	}

	public DateWrapper getNextMonth() {
		return getThisMonth().addMonths( 1 );
	}

	public DateWrapper getLastMonth() {
		return getThisMonth().addMonths( -1 );
	}

	public DateWrapper getThisMonth() {
		return new DateWrapper( DateUtils.truncate( new Date(), Calendar.MONTH ) );
	}

	public DateWrapper getNextYear() {
		return getThisYear().addYears( 1 );
	}

	public DateWrapper getThisYear() {
		return new DateWrapper( DateUtils.truncate( new Date(), Calendar.YEAR ) );
	}

	public DateWrapper getLastYear() {
		return getThisYear().addYears( -1 );
	}

	public DateWrapper getNextMonday() {
		return getToday().getNextMonday();
	}

	public DateWrapper getNextTuesday() {
		return getToday().getNextTuesday();
	}

	public DateWrapper getNextWednesday() {
		return getToday().getNextWednesday();
	}

	public DateWrapper getNextThursday() {
		return getToday().getNextThursday();
	}

	public DateWrapper getNextFriday() {
		return getToday().getNextFriday();
	}

	public DateWrapper getNextSaturday() {
		return getToday().getNextSaturday();
	}

	public DateWrapper getNextSunday() {
		return getToday().getNextSunday();
	}

	public DateWrapper getLastMonday() {
		return getToday().getLastMonday();
	}

	public DateWrapper getLastTuesday() {
		return getToday().getLastTuesday();
	}

	public DateWrapper getLastWednesday() {
		return getToday().getLastWednesday();
	}

	public DateWrapper getLastThursday() {
		return getToday().getLastThursday();
	}

	public DateWrapper getLastFriday() {
		return getToday().getLastFriday();
	}

	public DateWrapper getLastSaturday() {
		return getToday().getLastSaturday();
	}

	public DateWrapper getLastSunday() {
		return getToday().getLastSunday();
	}

	public DateWrapper parse( String text ) throws ParseException {
		return new DateWrapper( DateUtils.parseDate( text, "yyyy-MM-dd", "yyyy-MM-dd HH:mm", "yyyy-MM-dd HH:mm:ss" ) );
	}
}
