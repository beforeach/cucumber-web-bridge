@runall
Feature: Simple feature with dummy scenarios

  Scenario: Simple scenario
    # does nothing

  @ignore
  Scenario: Other simple scenario
    # also does nothing

  @slow
  Scenario Outline: Third scenario
    Given i save "<number>" as "value"

  Examples:
    | number |
    | 1      |
    | 2      |
    | 3      |

  @keepTogether
  Scenario Outline: Scenario four kept together at all times
    Given i save "<number>" as "value"

  Examples:
    | number |
    | 1      |
    | 2      |

