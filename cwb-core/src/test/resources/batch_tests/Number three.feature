@keepTogether
Feature: Yet another simple feature

  Scenario: Step one
    # does nothing

  @ignore
  Scenario: Step two
    # also does nothing

  Scenario Outline: Step three
    Given i save "<number>" as "value"

  Examples:
    | number |
    | 1      |
    | 2      |
