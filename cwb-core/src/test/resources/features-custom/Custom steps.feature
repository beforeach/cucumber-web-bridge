Feature: Executes the custom step and verifies the inherited variable
  Should be executed via the TestConsoleRunner which passes the correct value.
  Requires the glue com.foreach.custom to be present.

  Scenario: Execute a custom step and verify inherited variable
    Given the custom step is called
