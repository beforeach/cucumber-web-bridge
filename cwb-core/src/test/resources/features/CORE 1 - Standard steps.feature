Feature: Prints the variable inheritance
  The following scenarios require that environment system property is set to test.
  Uses Spring EL to perform some tests... not really the preferred way the final framework should be used.

  Background:
    Given that "#{environment != null}"

  Scenario: If no variables specified then property should be returned.
    Given that "#{environment == 'test'}"
    Then ensure that "#{properties['url.override'] == 'http://www.foreach.be'}"
    And ensure that "#{lookup('url.override')}" equals "http://www.foreach.be"
    And ensure that "#{properties['default.only'] == 'original value'}"
    And ensure that "#{lookup('default.only') == 'original value'}"
    And ensure that "#{properties['test.only'] == 'test value'}"
    And ensure that "#{lookup('test.only') == 'test value'}"
    And ensure that "#{variables['url.override'] == null}"
    And ensure that "#{variables['default.only'] == null}"
    And ensure that "#{variables['test.only'] == null}"

  Scenario: A set variable overrides a value from properties.
    Given that "#{environment == 'test'}"
    And i save "some test value" as "url.override"
    Then ensure that "#{variables['url.override']}" equals "some test value"
    And ensure that "#{properties['url.override'] == 'http://www.foreach.be'}"
    And ensure that "#{lookup('url.override') == 'some test value'}"
    And ensure that "#{lookup('default.only')}" equals "original value"

  Scenario: Variables and properties can also be accessed through the shorthand notation.
    Given that "#{environment == 'test'}"
    And i save "some test value" as "myvariable"
    And i save "other test value" as "default.only"
    Then ensure that "${url.override}" equals "http://www.foreach.be"
    And ensure that "${url.override}" equals "#{lookup('url.override')}"
    And ensure that "http://${url.override}/test" equals "http://#{lookup('url.override')}/test"
    And ensure that "${myvariable} !" equals "some test value !"
    And ensure that "#{myvariable} !" equals "some test value !"
    And ensure that "#{lookup('myvariable')} #{variables['myvariable']} #{properties['myvariable']}" equals "some test value some test value "
    And ensure that "${default.only}" equals "other test value"
    And ensure that "#{lookup('default.only')} #{variables['default.only']} #{properties['default.only']}" equals "other test value other test value original value"

  Scenario: Generated id variables and properties
    * ensure that "#{id.today}" equals "#{id.today}"
    * ensure that "#{not id.today.empty}"
    * ensure that "#{id.scenario}" equals "#{id.scenario}"
    * ensure that "#{not id.scenario.empty}"
    * ensure that "#{id.testrun}" equals "#{id.testrun}"
    * ensure that "#{not id.testrun.empty}"
    * ensure that "#{not id.next.empty}"
    * ensure that "#{id.next}" does not equal "#{id.next}"
    * ensure that "#{id.nextNumber > 0 and id.nextNumber != id.nextNumber}"

  Scenario: Save multiple variables at once
    Given that "#{environment == 'test'}"
    And i save variables:
      | myvariable   | some test value  |
      | default.only | other test value |
    Then ensure that "${url.override}" equals "http://www.foreach.be"
    And ensure that "${url.override}" equals "#{lookup('url.override')}"
    And ensure that "http://${url.override}/test" equals "http://#{lookup('url.override')}/test"
    And ensure that "${myvariable} !" equals "some test value !"
    And ensure that "#{lookup('myvariable')} #{variables['myvariable']} #{properties['myvariable']}" equals "some test value some test value "
    And ensure that "${default.only}" equals "other test value"
    And ensure that "#{lookup('default.only')} #{variables['default.only']} #{properties['default.only']}" equals "other test value other test value original value"

  Scenario: Using utf-8 characters
    Given i save "^&|éµ$¨_{}" as "utf8string"
    Then ensure that "${utf8string}" equals "^&|éµ$¨_{}"

  Scenario: Check complex object properties
    Given i save "#{new com.foreach.cuke.core.objects.ComplexObject()}" as "complexObject"
    Then ensure that "#{lookup('complexObject').code}" equals "code"
    Then ensure that "#{lookup('complexObject').options[0].key}" equals "option 1"
    Then ensure that "#{lookup('complexObject')['code']}" equals "code"

  Scenario: Ensure that object contains
    Given i save "#{new com.foreach.cuke.core.objects.ComplexObject()}" as "complexObject"
    Then ensure that "#{lookup('complexObject')}" contains "code"
    Then ensure that "#{lookup('complexObject')}" contains "code" with value "code"
    Then ensure that "#{lookup('complexObject')}" contains "options[0].key" with value "option 1"
    Then ensure that "#{lookup('complexObject')}" contains "options[].key" with value "option 1"
    Then ensure that "#{lookup('complexObject')}" contains:
      | id        |
      | code      |
      | options[] |
    Then ensure that "#{lookup('complexObject')}" contains:
      | id             | -1000    |
      | code           | code     |
      | options[0].key | option 1 |
      | options[].key  | option 1 |
