/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.cli;

import org.junit.Test;

import java.util.Properties;

import static org.junit.Assert.assertEquals;

/**
 * @author Arne Vandamme
 */
public class TestConsoleRunner
{
	@Test
	public void runCustomStepsTest() throws Exception {
		ConsoleRunner consoleRunner =
				new ConsoleRunner( "target/cucumber-custom", "src/test/resources/features-custom" );
		consoleRunner.setGlue( new String[] { "com.foreach.cuke", "com.foreach.custom" } );
		Properties props = new Properties();
		props.setProperty( "systemProperty", "passed-value" );
		consoleRunner.setProperties( props );

		assertEquals( 0, consoleRunner.start() );
	}
}
