/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.objects;

import java.util.ArrayList;
import java.util.List;

public class ComplexObject
{
	private Integer id;
	private String code;
	private List<SimpleObject> options;

	public ComplexObject() {
		this.id = -1000;
		this.code = "code";
		List<SimpleObject> options = new ArrayList<SimpleObject>();
		options.add( new SimpleObject( "option 1", "this is option 1" ) );
		this.options = options;
	}

	public Integer getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public List<SimpleObject> getOptions() {
		return options;
	}
}
