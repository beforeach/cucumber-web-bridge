/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.cli;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Arne Vandamme
 */
@Ignore
public class TestLogAggregator
{
	private List<File> dirs = Arrays.asList(
			new File( "cwb-core/src/test/logs/initial/batch-1" ),
			new File( "cwb-core/src/test/logs/initial/batch-2" ),
			new File( "cwb-core/src/test/logs/retry-1/batch-1" )
	);

	@Test
	public void jsonAggregation() {
		LogAggregator aggregator = new LogAggregator( dirs );
		String aggregated = aggregator.aggregatedJson();

		assertNotNull( aggregated );
	}

	@Test
	public void xmlAggregation() throws Exception {
		LogAggregator aggregator = new LogAggregator(
				Arrays.asList(
						new File( "cwb-core/src/test/logs/initial/batch-1" ),
						new File( "cwb-core/src/test/logs/initial/batch-2" )
				)
		);
		String aggregated = aggregator.aggregatedXml();

		assertNotNull( aggregated );
		//assertTrue( StringUtils.contains( aggregated, "skipped=\"0\" tests=\"1969\" failure=\"14\"" ) );
	}

	@Test
	public void scenarioTracker() throws Exception {
		LogAggregator aggregator = new LogAggregator(
				Arrays.asList(
						new File( "cwb-core/src/test/logs-2/initial/batch-1" ),
						new File( "cwb-core/src/test/logs-2/initial/batch-2" ),
						new File( "cwb-core/src/test/logs-2/initial/batch-3" ),
						new File( "cwb-core/src/test/logs-2/retry-1/batch-1" ),
						new File( "cwb-core/src/test/logs-2/retry-1/batch-2" )
				)
		);
		aggregator.aggregatedJson();

		Collection<LogAggregator.TrackingRecord> records = aggregator.getScenarioTrackingRecords();

		boolean found = false;
		for ( LogAggregator.TrackingRecord record : records ) {
			if ( record.getName().contains( "b5f1af48-86b2-4835-85a2-65cd7c05a343.feature:129" ) ) {
				found = true;
				assertEquals( 2, record.getCount() );
			}
		}

		assertTrue( found );
	}
}
