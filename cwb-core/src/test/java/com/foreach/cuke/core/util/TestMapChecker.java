/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.yaml.snakeyaml.Yaml;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:cucumber.xml")
@SuppressWarnings("unchecked")
public class TestMapChecker
{
	@Autowired
	private MapChecker mapChecker;

	private Map entity;
	private Map<String, Object> values;

	@Before
	public void reset() {
		entity = new HashMap();
		values = new HashMap<String, Object>();
	}

	@Test
	public void emptyEntityThrowsAssertionError() throws Exception {
		entity = null;
		failContains( "Source data is null for map checking." );
	}

	@Test
	public void withExistingKeyAndNullValueReturnsTrue() throws Exception {
		entity.put( "key", "value" );
		values.put( "key", null );

		okContains();
	}

	@Test
	public void withNonExistingKeyThrowsAssertionError() throws Exception {
		entity.put( "key", "value" );
		values.put( "nonExisting", null );

		failContains( "Field [nonExisting] not found" );
	}

	@Test
	public void withExistingKeyAndNonExistingValueThrowsAssertionError() throws Exception {
		entity.put( "key", "value" );
		values.put( "key", "nonExisting" );

		failContains( "Field [key] failed: expected [nonExisting] but got [value]" );
	}

	@Test
	public void withMultipleExistingKeysAndNullValuesReturnsTrue() throws Exception {
		entity.put( "key", "value" );
		entity.put( "key2", "value" );
		entity.put( "key5", "value" );

		values.put( "key", null );
		values.put( "key2", null );

		okContains();
	}

	@Test
	public void withMultipleNonExistingKeysAndNullValuesThrowsAssertionError() throws Exception {
		entity.put( "key", "value" );
		entity.put( "key2", "value" );
		entity.put( "key5", "value" );

		values.put( "key", null );
		values.put( "key16", null );

		failContains( "Field [key16] not found" );
	}

	@Test
	public void withMultipleExistingKeysAndExistingValuesReturnsTrue() throws Exception {
		entity.put( "key", "value" );
		entity.put( "key2", "value2" );
		entity.put( "key5", "value5" );

		values.put( "key", "value" );
		values.put( "key2", "value2" );

		okContains();
	}

	@Test
	public void withMultipleExistingKeysAndNonExistingValuesThrowsAssertionError() throws Exception {
		entity.put( "key", "value" );
		entity.put( "key2", "value2" );
		entity.put( "key5", "value5" );

		values.put( "key", "value35" );
		values.put( "key5", "value5" );

		failContains( "Field [key] failed: expected [value35] but got [value]" );
	}

	@Test
	public void containsShouldSucceedWithAllExistingValues() throws Exception {
		HashMap<String, String> propertyMap = new HashMap<String, String>();
		propertyMap.put( "username", "username" );
		propertyMap.put( "email", "email" );
		entity.put( "user", propertyMap );

		HashMap<String, String> instancePropertyMap = new HashMap<String, String>();
		instancePropertyMap.put( "username", "username" );
		instancePropertyMap.put( "email", "email" );
		values.put( "user", instancePropertyMap );

		okContains();
	}

	@Test
	public void containsShouldFailWithNonExistingValueOfMap() throws Exception {
		HashMap<String, String> propertyMap = new HashMap<String, String>();
		propertyMap.put( "username", "username" );
		propertyMap.put( "email", "email" );
		entity.put( "user", propertyMap );

		HashMap<String, String> instancePropertyMap = new HashMap<String, String>();
		instancePropertyMap.put( "username", "failed" );
		instancePropertyMap.put( "email", "email" );
		values.put( "user", instancePropertyMap );

		failContains( "Field [user.username] failed: expected [failed] but got [username]" );
	}

	@Test
	public void containsShouldFailWithNonExistingKeyOfMap() throws Exception {
		HashMap<String, String> propertyMap = new HashMap<String, String>();
		propertyMap.put( "username", "username" );
		propertyMap.put( "email", "email" );
		entity.put( "user", propertyMap );

		HashMap<String, String> instancePropertyMap = new HashMap<String, String>();
		instancePropertyMap.put( "username", "username" );
		instancePropertyMap.put( "confirmemail", "email" );
		values.put( "user", instancePropertyMap );

		failContains( "Field [user.confirmemail] not found" );
	}

	@Test
	public void containsShouldSucceedWithSomeExistingValues() throws Exception {
		HashMap<String, String> propertyMap = new HashMap<String, String>();
		propertyMap.put( "username", "username" );
		propertyMap.put( "email", "email" );
		entity.put( "user", propertyMap );

		HashMap<String, String> instancePropertyMap = new HashMap<String, String>();
		instancePropertyMap.put( "email", "email" );
		values.put( "user", instancePropertyMap );

		okContains();
	}

	@Test
	public void containsShouldSucceedWithAllExistingValuesAndKeysInOtherOrder() throws Exception {
		HashMap<String, String> propertyMap = new HashMap<String, String>();
		propertyMap.put( "username", "username" );
		propertyMap.put( "email", "email" );
		entity.put( "user", propertyMap );

		HashMap<String, String> instancePropertyMap = new HashMap<String, String>();
		instancePropertyMap.put( "email", "email" );
		instancePropertyMap.put( "username", "username" );
		values.put( "user", instancePropertyMap );

		okContains();
	}

	@Test
	public void containsShouldSucceedWhenAnObjectContainsAnotherObjectThatIsAMap() {
		List list = new ArrayList<HashMap<String, String>>();
		Map<String, String> item1Object = new HashMap<String, String>();
		Map<String, Object> item1 = new HashMap<String, Object>();
		item1.put( "object", item1Object );
		item1Object.put( "id", "-1000003" );

		Map<String, String> item2Object = new HashMap<String, String>();
		Map<String, Object> item2 = new HashMap<String, Object>();
		item2.put( "object", item2Object );
		item2Object.put( "id", "-1000000" );

		list.add( item1 );
		list.add( item2 );

		entity.put( "items", list );

		List itemlist = new ArrayList<HashMap<String, String>>();
		values.put( "items", itemlist );

		Map<String, Object> instanceItem = new HashMap<String, Object>();
		Map<String, String> instanceItemObject = new HashMap<String, String>();

		instanceItemObject.put( "id", "-1000000" );
		instanceItem.put( "object", instanceItemObject );
		itemlist.add( instanceItem );

		okContains();
	}

	@Test
	public void containsShouldNotSucceedWhenAnObjectContainsAnotherObjectThatIsAMap() {
		List list = new ArrayList<HashMap<String, String>>();

		Map<String, String> item1Object = new HashMap<String, String>();
		Map<String, Object> item1 = new HashMap<String, Object>();
		item1.put( "object", item1Object );
		item1Object.put( "id", "-1000003" );

		Map<String, String> item2Object = new HashMap<String, String>();
		Map<String, Object> item2 = new HashMap<String, Object>();
		item2.put( "object", item2Object );
		item2Object.put( "id", "-1000000" );

		list.add( item1 );
		list.add( item2 );

		entity.put( "items", list );

		List itemlist = new ArrayList<HashMap<String, String>>();
		values.put( "items", itemlist );

		Map<String, Object> instanceItem = new HashMap<String, Object>();
		Map<String, String> instanceItemObject = new HashMap<String, String>();

		instanceItemObject.put( "id", "-1000009" );
		instanceItem.put( "object", instanceItemObject );
		itemlist.add( instanceItem );

		failContains( "Field [items[0]] failed: no match found" );
	}

	@Test
	public void containsBasedOnASubsetShouldSucceed() {
		entity.put( "name", "A" );
		entity.put( "email", "B" );

		Map<String, Object> address = new HashMap<String, Object>();
		address.put( "street", "some street" );
		address.put( "number", 123 );

		entity.put( "address", address );

		List<Object> list = new LinkedList<Object>();
		Map<String, Object> itemOne = new HashMap<String, Object>();
		itemOne.put( "key", "keyOne" );
		list.add( itemOne );

		Map<String, Object> itemTwo = new HashMap<String, Object>();
		itemTwo.put( "key2", "keyTwo" );
		list.add( itemTwo );

		Map<String, Object> itemThree = new HashMap<String, Object>();
		itemThree.put( "key", "keyThree" );
		list.add( itemThree );

		entity.put( "list", list );

		values = parseYaml(
				"name: A\nemail: B\nlist:\n  - key: keyOne\n  - key2: keyTwo"
		);

		okContains();
	}

	@Test
	public void aListItemShouldOnlyCountOnce() {
		entity = parseYaml(
				"list:\n  -\n   key: keyOne\n   value: test"
		);
		values = parseYaml(
				"list:\n  -\n   key: keyOne\n   value: test\n  - key: keyOne"
		);

		failContains( "Field [list] failed: unable to match all items" );
	}

	@Test
	public void withMapAsValueAndSpelCheckReturnsTrue() throws Exception {
		entity.put( "key", "value" );
		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put( "keyInMap", "valueOfKeyInMap" );
		entity.put( "keyWithMap", valueMap );

		values.put( "key", "value" );
		values.put( "keyWithMap.keyInMap", "valueOfKeyInMap" );

		okContains();
	}

	@Test
	public void withListAsValueAndSpelCheckReturnsTrue() throws Exception {
		entity.put( "key", "value" );
		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put( "keyInMap", "valueOfKeyInMap" );
		List<Object> objectList = new ArrayList<Object>();
		objectList.add( valueMap );
		entity.put( "keyWithList", objectList );

		values.put( "key", "value" );
		values.put( "keyWithList[0].keyInMap", "valueOfKeyInMap" );

		okContains();
	}

	@Test
	public void simpleSquareBracketNotationWithExistingListReturnsTrue() throws Exception {
		entity.put( "key", "value" );
		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put( "keyInMap", "valueOfKeyInMap" );
		Map<String, String> otherValueMap = new HashMap<String, String>();
		otherValueMap.put( "keyInMap", "otherValueOfKeyInMap" );

		List<Object> objectList = new ArrayList<Object>();
		objectList.add( otherValueMap );
		objectList.add( valueMap );
		entity.put( "keyWithList", objectList );

		values.put( "key", "value" );
		values.put( "keyWithList[].keyInMap", "otherValueOfKeyInMap" );

		okContains();
	}

	@Test
	public void simpleSquareBracketNotationWithNonExistingListThrowsAssertionError() throws Exception {
		entity.put( "key", "value" );

		values.put( "key", "value" );
		values.put( "keyWithList[].keyInMap", "valueOfKeyInMap" );

		failContains( "Field [keyWithList[].keyInMap] not found" );
	}

	@Test
	public void simpleSquareBracketNotationWithNonExistingValueFails() throws Exception {
		entity.put( "key", "value" );
		Map<String, String> otherValueMap = new HashMap<String, String>();
		otherValueMap.put( "keyInMap", "otherValueOfKeyInMap" );

		List<Object> objectList = new ArrayList<Object>();
		objectList.add( otherValueMap );
		entity.put( "keyWithList", objectList );

		values.put( "key", "value" );
		values.put( "keyWithList[].keyInMap", "valueOfKeyInMap" );

		failContains(
				"Field [keyWithList[].keyInMap] failed: expected [valueOfKeyInMap] but got [otherValueOfKeyInMap]" );
	}

	@Test
	/**
	 And the response entity "csv" should contain 21 entities
	 And the response entity "csv[0]" should contain:
	 | Title | Title |
	 | "1"    | "1" |
	 And the response entity "csv[1]" should contain:
	 | Title | SomeTitle |
	 | "1"    | "0" |
	 */
	public void entitiesWithIntegersAsKeysShouldNotParseSpel() throws Exception {
		List<Map<String, String>> csvEntityItems = new ArrayList<Map<String, String>>();
		List<Map<String, String>> csvItems = new ArrayList<Map<String, String>>();

		// Entities
		{
			// This is the header
			Map<String, String> csvItem = new LinkedHashMap<String, String>();
			csvItem.put( "Title", "Title" );
			csvItem.put( "1", "1" );
			csvEntityItems.add( csvItem );
		}
		{
			// This is the first line
			Map<String, String> csvItem = new LinkedHashMap<String, String>();
			csvItem.put( "Title", "Title" );
			csvItem.put( "1", "25" );
			csvEntityItems.add( csvItem );
		}
		{
			// This is the second line
			Map<String, String> csvItem = new LinkedHashMap<String, String>();
			csvItem.put( "Title", "Title" );
			csvItem.put( "1", "50" );
			csvEntityItems.add( csvItem );
		}

		Map<String, String> valueItem = new LinkedHashMap<String, String>();
		valueItem.put( "Title", "Title" );
		valueItem.put( "1", "25" );

		entity.put( "csv", csvEntityItems );
		values.put( "csv", Collections.singletonList( valueItem ) );

		okContains();

		Object fetched = mapChecker.retrieveSingleValue( entity, "csv[1]" );
		assertTrue( fetched instanceof Map );
		mapChecker.contains( (Map) fetched, valueItem, true );
	}

	@Test
	/**
	 And the response entity "csv" should contain 21 entities
	 And the response should contain data:
	 """
	 csv:
	 -
	 Title: Title
	 1: 1
	 -
	 Title: Title
	 1: 0
	 """
	 */
	public void entitiesWithListsShouldVerifyAllValuesCorrectly() throws Exception {
		List<Map<String, String>> csvEntityItems = new ArrayList<Map<String, String>>();
		List<Map<String, String>> csvItems = new ArrayList<Map<String, String>>();

		// Entities
		{
			// This is the header
			Map<String, String> csvItem = new LinkedHashMap<String, String>();
			csvItem.put( "Title", "Title" );
			csvItem.put( "1", "1" );
			csvEntityItems.add( csvItem );
		}
		{
			// This is the first line
			Map<String, String> csvItem = new LinkedHashMap<String, String>();
			csvItem.put( "Title", "Title" );
			csvItem.put( "1", "25" );
			csvEntityItems.add( csvItem );
		}
		{
			// This is the second line
			Map<String, String> csvItem = new LinkedHashMap<String, String>();
			csvItem.put( "Title", "Title" );
			csvItem.put( "1", "50" );
			csvEntityItems.add( csvItem );
		}

		// Values
		{
			// This is the header
			Map<String, String> csvItem = new LinkedHashMap<String, String>();
			csvItem.put( "Title", "Title" );
			csvItem.put( "1", "1" );
			csvItems.add( csvItem );
		}
		{
			// This is the first line
			Map<String, String> csvItem = new LinkedHashMap<String, String>();
			csvItem.put( "Title", "Title" );
			csvItem.put( "1", "99" );
			csvItems.add( csvItem );
		}

		entity.put( "csv", csvEntityItems );
		values.put( "csv", csvItems );

		failContains( "Field [csv[1]] failed: no match found" );
	}

	@Test
	public void fetchEntityWithSquareBracketsReturnsEntity() throws Exception {
		Object expected = "test";
		List values = new ArrayList();
		Map map = new HashMap();
		map.put( "test", "test" );
		values.add( map );
		Map rootMap = new HashMap();
		rootMap.put( "root", values );
		Object actual = mapChecker.retrieveSingleValue( rootMap, "root[].test" );
		assertEquals( expected, actual );
	}

	@Test
	public void fetchEntityFromMapWithExistingEntityReturnsEntity() throws Exception {
		Object expected = "test";
		Map values = new HashMap();
		values.put( "test", expected );
		Object actual = mapChecker.retrieveSingleValue( values, "test" );
		assertEquals( expected, actual );
	}

	@Test
	public void fetchEntityFromListWithExistingEntityReturnsEntity() throws Exception {
		Object expected = "test";
		List values = new ArrayList();
		Map map = new HashMap();
		map.put( "test", "test" );
		values.add( map );
		Object actual = mapChecker.retrieveSingleValue( values, "[0].test" );
		assertEquals( expected, actual );
	}

	@Test
	public void fetchEntityFromMapWithSpelReturnsEntity() throws Exception {
		Object expected = "test";
		Map values = new HashMap();
		values.put( "test", expected );
		Map rootMap = new HashMap();
		rootMap.put( "root", values );
		Object actual = mapChecker.retrieveSingleValue( rootMap, "root.test" );
		assertEquals( expected, actual );
	}

	@Test
	public void fetchEntityFromListWithSpelReturnsEntity() throws Exception {
		Object expected = "test";
		List values = new ArrayList();
		Map map = new HashMap();
		map.put( "test", "test" );
		values.add( map );
		Map rootMap = new HashMap();
		rootMap.put( "root", values );
		Object actual = mapChecker.retrieveSingleValue( rootMap, "root[0].test" );
		assertEquals( expected, actual );
	}

	@Test
	public void doesNotContainWithEmptyEntityReturnsTrue() throws Exception {
		Map<String, ?> values = new HashMap<String, Object>();
		boolean doesNotContain = mapChecker.doesNotContain( (Map) null, values, true );
		assertTrue( doesNotContain );
	}

	@Test
	public void doesNotContainWithNonExistingKeyReturnsTrue() throws Exception {
		Map<String, String> values = new HashMap<String, String>();
		values.put( "nonExistingKey", null );
		Map entity = new HashMap();
		entity.put( "key", "value" );
		boolean doesNotContain = mapChecker.doesNotContain( entity, values, true );
		assertTrue( doesNotContain );
	}

	@Test(expected = AssertionError.class)
	public void doesNotContainWithExistingKeyAndNullValueThrowsAssertionError() throws Exception {
		Map<String, String> values = new HashMap<String, String>();
		values.put( "key", null );
		Map entity = new HashMap();
		entity.put( "key", "value" );
		mapChecker.doesNotContain( entity, values, true );
	}

	@Test
	public void doesNotContainWithExistingKeyAndNonExistingValueReturnsTrue() throws Exception {
		Map<String, String> values = new HashMap<String, String>();
		values.put( "key", "nonExistingValue" );
		Map entity = new HashMap();
		entity.put( "key", "value" );
		boolean doesNotContain = mapChecker.doesNotContain( entity, values, true );
		assertTrue( doesNotContain );
	}

	@Test(expected = AssertionError.class)
	public void doesNotContainWithExistingKeyAndExistingValueThrowsAssertionError() throws Exception {
		Map<String, String> values = new HashMap<String, String>();
		values.put( "key", "value" );
		Map entity = new HashMap();
		entity.put( "key", "value" );
		mapChecker.doesNotContain( entity, values, true );
	}

	private void okContains() {
		mapChecker.contains( entity, values, true );
	}

	private void failContains( String expectedMessage ) {
		boolean exceptionThrown = false;

		try {
			mapChecker.contains( entity, values, true );
		}
		catch ( AssertionError ae ) {
			exceptionThrown = true;
			assertEquals( expectedMessage, ae.getMessage() );
		}

		assertTrue( "Expected contains call to fail but it did not.", exceptionThrown );
	}

	private Map<String, Object> parseYaml( String data ) {
		Yaml yaml = new Yaml();
		return (Map<String, Object>) yaml.load( data );
	}
}
