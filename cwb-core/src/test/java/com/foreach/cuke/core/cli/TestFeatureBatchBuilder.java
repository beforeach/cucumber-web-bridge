/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.cli;

import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Arne Vandamme
 */
public class TestFeatureBatchBuilder
{
	@Test
	public void listScenarios() {
		List<String> scenarios =
				FeatureBatchBuilder.listScenarios( new File( absolute( "test/resources/batch_tests" ) ), true,
				                                   Collections.<String>emptyList() );

		assertEquals(
				Arrays.asList(
						absolute( "test/resources/batch_tests/Number one.feature:4" ),
						absolute( "test/resources/batch_tests/Number one.feature:8" ),
						absolute( "test/resources/batch_tests/Number one.feature:17:18:19" ),
						absolute( "test/resources/batch_tests/Number one.feature:27:28" ),
						absolute( "test/resources/batch_tests/Number three.feature:4:8:16:17" ),
						absolute( "test/resources/batch_tests/Number two.feature:3" )
				),
				scenarios
		);
	}

	@Test
	public void listOutlineExamplesAsSeparateScenarios() {
		List<String> scenarios =
				FeatureBatchBuilder.listScenarios( new File( absolute( "test/resources/batch_tests" ) ), false,
				                                   Collections.<String>emptyList() );

		assertEquals(
				Arrays.asList(
						absolute( "test/resources/batch_tests/Number one.feature:4" ),
						absolute( "test/resources/batch_tests/Number one.feature:8" ),
						absolute( "test/resources/batch_tests/Number one.feature:17" ),
						absolute( "test/resources/batch_tests/Number one.feature:18" ),
						absolute( "test/resources/batch_tests/Number one.feature:19" ),
						absolute( "test/resources/batch_tests/Number one.feature:27:28" ),
						absolute( "test/resources/batch_tests/Number three.feature:4:8:16:17" ),
						absolute( "test/resources/batch_tests/Number two.feature:3" )
				),
				scenarios
		);
	}

	@Test
	public void listScenariosWithTags() {
		List<String> scenarios =
				FeatureBatchBuilder.listScenarios( new File( absolute( "test/resources/batch_tests" ) ), true,
				                                   Arrays.asList( "~@ignore" ) );

		assertEquals(
				Arrays.asList(
						absolute( "test/resources/batch_tests/Number one.feature:4" ),
						absolute( "test/resources/batch_tests/Number one.feature:17:18:19" ),
						absolute( "test/resources/batch_tests/Number one.feature:27:28" ),
						absolute( "test/resources/batch_tests/Number three.feature:4:16:17" ),
						absolute( "test/resources/batch_tests/Number two.feature:3" )
				),
				scenarios
		);

		scenarios =
				FeatureBatchBuilder.listScenarios( new File( absolute( "test/resources/batch_tests" ) ), true,
				                                   Arrays.asList( "@slow" ) );

		assertEquals(
				Arrays.asList( absolute( "test/resources/batch_tests/Number one.feature:17:18:19" ) ), scenarios
		);

		scenarios =
				FeatureBatchBuilder.listScenarios( new File( absolute( "test/resources/batch_tests" ) ), true,
				                                   Arrays.asList( "~@ignore", "@runall" ) );

		assertEquals(
				Arrays.asList(
						absolute( "test/resources/batch_tests/Number one.feature:4" ),
						absolute( "test/resources/batch_tests/Number one.feature:17:18:19" ),
						absolute( "test/resources/batch_tests/Number one.feature:27:28" )
				),
				scenarios
		);

		scenarios =
				FeatureBatchBuilder.listScenarios( new File( absolute( "test/resources/batch_tests" ) ), false,
				                                   Arrays.asList( "~@ignore", "@runall" ) );

		assertEquals(
				Arrays.asList(
						absolute( "test/resources/batch_tests/Number one.feature:4" ),
						absolute( "test/resources/batch_tests/Number one.feature:17" ),
						absolute( "test/resources/batch_tests/Number one.feature:18" ),
						absolute( "test/resources/batch_tests/Number one.feature:19" ),
						absolute( "test/resources/batch_tests/Number one.feature:27:28" )
				),
				scenarios
		);

		scenarios =
				FeatureBatchBuilder.listScenarios( new File( absolute( "test/resources/batch_tests" ) ), false,
				                                   Arrays.asList( "~@ignore", "@keepTogether" ) );

		assertEquals(
				Arrays.asList(
						absolute( "test/resources/batch_tests/Number one.feature:27:28" ),
						absolute( "test/resources/batch_tests/Number three.feature:4:16:17" )
				),
				scenarios
		);
	}

	@Test
	public void chunkBuildSingleBatchFromDirectory() {
		List<List<String>> batches =
				FeatureBatchBuilder.distribute( FeatureBatchBuilder.list( path( "test/resources/features" ) ), 1,
				                                FeatureBatchBuilder.Distribution.CHUNK );

		assertEquals( 1, batches.size() );
		assertEquals(
				Arrays.asList(
						absolute( "test/resources/features/CORE 1 - Standard steps.feature" ),
						absolute( "test/resources/features/CORE 2 - Date values.feature" ),
						absolute( "test/resources/features/dummy features/Dummy feature.feature" )
				),
				batches.get( 0 )
		);
	}

	@Test
	public void roundRobinBuildSingleBatchFromDirectory() {
		List<List<String>> batches =
				FeatureBatchBuilder.distribute( FeatureBatchBuilder.list( path( "test/resources/features" ) ), 1,
				                                FeatureBatchBuilder.Distribution.ROUND_ROBIN );

		assertEquals( 1, batches.size() );
		assertEquals(
				Arrays.asList(
						absolute( "test/resources/features/CORE 1 - Standard steps.feature" ),
						absolute( "test/resources/features/CORE 2 - Date values.feature" ),
						absolute( "test/resources/features/dummy features/Dummy feature.feature" )
				),
				batches.get( 0 )
		);
	}

	@Test
	public void chunkBuildSingleEntryBatchesFromDirectory() {
		List<List<String>> batches =
				FeatureBatchBuilder.distribute( FeatureBatchBuilder.list( path( "test/resources/features" ) ), 5,
				                                FeatureBatchBuilder.Distribution.CHUNK );

		assertEquals( 3, batches.size() );
		assertEquals(
				Arrays.asList( absolute( "test/resources/features/CORE 1 - Standard steps.feature" ) ),
				batches.get( 0 )
		);
		assertEquals(
				Arrays.asList( absolute( "test/resources/features/CORE 2 - Date values.feature" ) ),
				batches.get( 1 )
		);
		assertEquals(
				Arrays.asList( absolute( "test/resources/features/dummy features/Dummy feature.feature" ) ),
				batches.get( 2 )
		);
	}

	@Test
	public void roundRobinBuildSingleEntryBatchesFromDirectory() {
		List<List<String>> batches =
				FeatureBatchBuilder.distribute( FeatureBatchBuilder.list( path( "test/resources/features" ) ), 5,
				                                FeatureBatchBuilder.Distribution.CHUNK );

		assertEquals( 3, batches.size() );
		assertEquals(
				Arrays.asList( absolute( "test/resources/features/CORE 1 - Standard steps.feature" ) ),
				batches.get( 0 )
		);
		assertEquals(
				Arrays.asList( absolute( "test/resources/features/CORE 2 - Date values.feature" ) ),
				batches.get( 1 )
		);
		assertEquals(
				Arrays.asList( absolute( "test/resources/features/dummy features/Dummy feature.feature" ) ),
				batches.get( 2 )
		);
	}

	@Test
	public void chunkBuildMultipleBatchesFromDirectory() {
		List<List<String>> batches =
				FeatureBatchBuilder.distribute( FeatureBatchBuilder.list( path( "test/resources/features" ) ), 2,
				                                FeatureBatchBuilder.Distribution.CHUNK );

		assertEquals( 2, batches.size() );
		assertEquals(
				Arrays.asList( absolute( "test/resources/features/CORE 1 - Standard steps.feature" ) ),
				batches.get( 0 )
		);
		assertEquals(
				Arrays.asList(
						absolute( "test/resources/features/CORE 2 - Date values.feature" ),
						absolute( "test/resources/features/dummy features/Dummy feature.feature" )
				),
				batches.get( 1 )
		);
	}

	@Test
	public void roundRobinBuildMultipleBatchesFromDirectory() {
		List<List<String>> batches =
				FeatureBatchBuilder.distribute( FeatureBatchBuilder.list( path( "test/resources/features" ) ), 2,
				                                FeatureBatchBuilder.Distribution.ROUND_ROBIN );

		assertEquals( 2, batches.size() );
		assertEquals(
				Arrays.asList(
						absolute( "test/resources/features/CORE 1 - Standard steps.feature" ),
						absolute( "test/resources/features/dummy features/Dummy feature.feature" )
				),
				batches.get( 0 )
		);
		assertEquals(
				Arrays.asList(
						absolute( "test/resources/features/CORE 2 - Date values.feature" )
				),
				batches.get( 1 )
		);
	}

	private String absolute( String relative ) {
		return new File( path( relative ) ).getAbsolutePath();
	}

	private String path( String relative ) {
		if ( new File( "." ).getAbsolutePath().contains( "cwb-core" ) ) {
			return "src/" + relative;
		}

		return "cwb-core/src/" + relative;
	}
}
