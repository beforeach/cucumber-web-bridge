/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.util;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestStringMatcher
{
	private StringMatcher matcher;

	@Before
	public void createMatcher() {
		matcher = new StringMatcher( false );
	}

	@Test
	public void equals() {
		assertTrue( matcher.equals( "my original string", "my original string" ) );
		assertFalse( matcher.equals( "my original string", "original" ) );
		assertFalse( matcher.equals( "my ORIGINAL string", "my original STRING" ) );
		assertTrue( matcher.equals( "my ORIGINAL string", "my original STRING/i" ) );
		assertTrue( matcher.equals( "my ORIGINAL string", "/my original STRING/i" ) );
		assertTrue( matcher.equals( "my ORIGINAL string", "/original/i" ) );

		matcher.setIgnoreCaseByDefault( true );
		assertTrue( matcher.equals( "my ORIGINAL string", "my original string" ) );
		assertTrue( matcher.equals( "my ORIGINAL string", "/my original string/" ) );
	}

	@Test
	public void contains() {
		assertTrue( matcher.contains( "my original string", "original" ) );
		assertFalse( matcher.contains( "my ORIGInal string", "origiNAL" ) );
		assertTrue( matcher.contains( "my ORIGInal string", "origiNAL/i" ) );

		assertFalse( matcher.contains( "my original string", "/ORIGINAL/" ) );
		assertTrue( matcher.contains( "my original string", "/ORIGINAL/i" ) );
		assertFalse( matcher.contains( "my ORIGINAL string", "/original/" ) );
		assertTrue( matcher.contains( "my ORIGINAL string", "/original/i" ) );

		matcher.setIgnoreCaseByDefault( true );
		assertTrue( matcher.contains( "my ORIGInal string", "origiNAL" ) );
	}

	@Test
	public void startsWith() {
		assertTrue( matcher.startsWith( "my original string", "my original" ) );
		assertFalse( matcher.startsWith( "my ORIGINAL string", "MY original" ) );
		assertTrue( matcher.startsWith( "my ORIGINAL string", "MY original/i" ) );

		assertTrue( matcher.startsWith( "my original string", "/^my original/" ) );
		assertFalse( matcher.startsWith( "not my original string", "/^my original/" ) );
		assertFalse( matcher.startsWith( "my original string", "/^my ORIGINAL/" ) );
		assertTrue( matcher.startsWith( "my original string", "/^MY original/i" ) );
		assertFalse( matcher.startsWith( "my ORIGINAL string", "/^my original/" ) );
		assertTrue( matcher.startsWith( "MY original string", "/^my original/i" ) );

		matcher.setIgnoreCaseByDefault( true );
		assertTrue( matcher.startsWith( "my ORIGINAL string", "MY original" ) );
	}

	@Test
	public void endsWith() {
		assertTrue( matcher.endsWith( "my original string", "original string" ) );
		assertFalse( matcher.endsWith( "my ORIGINAL string", "original STRING" ) );
		assertTrue( matcher.endsWith( "my ORIGINAL string", "original STRING/i" ) );

		assertTrue( matcher.endsWith( "my original string", "/original string$/" ) );
		assertFalse( matcher.endsWith( "my original string!", "/original string$/" ) );
		assertFalse( matcher.endsWith( "my original string", "/ORIGINAL string$/" ) );
		assertTrue( matcher.endsWith( "my original string", "/original STRING$/i" ) );
		assertFalse( matcher.endsWith( "my original STRING", "/original string$/" ) );
		assertTrue( matcher.endsWith( "MY ORIGINAL string", "/original string$/i" ) );

		matcher.setIgnoreCaseByDefault( true );
		assertTrue( matcher.endsWith( "my ORIGINAL string", "original STRING" ) );
	}

	@Test
	public void complexRegex() {
		assertTrue( matcher.matches( "my my my ORIGINAL string", "/^(my ?)+ original (?:s|d)tring$/i" ) );
		assertTrue( matcher.matches( "welcome Foreach user", "/^welcome (.+) user$/" ) );
	}
}
