/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.core.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:cucumber.xml")
public class TestObjectMatcher
{
	@Autowired
	private ObjectMatcher objectMatcher;

	@Test
	public void booleanAndString() throws Exception {
		assertTrue( objectMatcher.matches( Boolean.TRUE, "true" ) );
		assertFalse( objectMatcher.matches( Boolean.TRUE, "false" ) );
		assertTrue( objectMatcher.matches( Boolean.FALSE, "false" ) );
		assertFalse( objectMatcher.matches( Boolean.FALSE, "true" ) );

		assertTrue( objectMatcher.matches( Boolean.TRUE, "TRUE" ) );
		assertFalse( objectMatcher.matches( Boolean.TRUE, "FALSE" ) );
		assertTrue( objectMatcher.matches( Boolean.FALSE, "FALSE" ) );
		assertFalse( objectMatcher.matches( Boolean.FALSE, "TRUE" ) );

		assertTrue( objectMatcher.matches( "true", Boolean.TRUE ) );
		assertFalse( objectMatcher.matches( "false", Boolean.TRUE ) );
		assertTrue( objectMatcher.matches( "false", Boolean.FALSE ) );
		assertFalse( objectMatcher.matches( "true", Boolean.FALSE ) );

		assertTrue( objectMatcher.matches( "TRUE", Boolean.TRUE ) );
		assertFalse( objectMatcher.matches( "FALSE", Boolean.TRUE ) );
		assertTrue( objectMatcher.matches( "FALSE", Boolean.FALSE ) );
		assertFalse( objectMatcher.matches( "TRUE", Boolean.FALSE ) );

		assertFalse( objectMatcher.matches( "1", Boolean.TRUE ) );
		assertFalse( objectMatcher.matches( "0", Boolean.FALSE ) );
	}

	@Test
	public void numberAndString() throws Exception {
		assertTrue( objectMatcher.matches( -1000, "-1000" ) );
		assertTrue( objectMatcher.matches( -1000l, "-1000" ) );
	}

	@Test
	public void integers() throws Exception {
		assertTrue( objectMatcher.matches( Integer.valueOf( "-1000") , Integer.valueOf( "-1000") ) );
	}
}
