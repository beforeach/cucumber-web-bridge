/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.rest.util;

import org.supercsv.prefs.CsvPreference;

/**
 * @author Andy Somers
 */
public class CsvConfiguration
{
	private char delimiter = (char) CsvPreference.STANDARD_PREFERENCE.getDelimiterChar();
	private char quoteChar = (char) CsvPreference.STANDARD_PREFERENCE.getQuoteChar();
	private String endOfLineSymbols = CsvPreference.STANDARD_PREFERENCE.getEndOfLineSymbols();
	private boolean withHeaders = true;

	public void setDelimiter( char delimiter ) {
		this.delimiter = delimiter;
	}

	public void setQuoteChar( char quoteChar ) {
		this.quoteChar = quoteChar;
	}

	public void setEndOfLineSymbols( String endOfLineSymbols ) {
		this.endOfLineSymbols = endOfLineSymbols;
	}

	public boolean isWithHeaders() {
		return withHeaders;
	}

	public void setWithHeaders( boolean withHeaders ) {
		this.withHeaders = withHeaders;
	}

	public CsvPreference toCsvPreference() {
		return new CsvPreference.Builder( quoteChar, delimiter, endOfLineSymbols ).build();
	}
}
