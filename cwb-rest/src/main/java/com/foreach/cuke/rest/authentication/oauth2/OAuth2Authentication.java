/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.rest.authentication.oauth2;

import com.foreach.cuke.rest.authentication.Authentication;
import org.springframework.http.HttpHeaders;
import org.springframework.web.util.UriComponentsBuilder;

public class OAuth2Authentication implements Authentication
{
	private final String accessToken;
	private String refreshToken, scope;

	public OAuth2Authentication( String accessToken ) {
		this.accessToken = accessToken;
	}

	public void apply( UriComponentsBuilder url, HttpHeaders requestHeaders ) {
		requestHeaders.remove("Authorization");
		requestHeaders.add( "Authorization", "Bearer " + accessToken );
	}

	public String getAccessToken() {
		return accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken( String refreshToken ) {
		this.refreshToken = refreshToken;
	}

	public String getScope() {
		return scope;
	}

	public void setScope( String scope ) {
		this.scope = scope;
	}

	@Override
	public String toString() {
		return "OAuth2Authentication{" +
				"accessToken='" + accessToken + '\'' +
				", refreshToken='" + refreshToken + '\'' +
				", scope='" + scope + '\'' +
				'}';
	}
}
