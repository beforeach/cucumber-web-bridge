/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.rest.config;

import com.foreach.cuke.core.spel.SpelRootProperties;
import com.foreach.cuke.rest.util.RestRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.annotation.PostConstruct;
import java.net.URI;

@Configuration
public class CwbRestConfiguration
{
	@Autowired
	private RestRequest restRequest;

	@Autowired
	private SpelRootProperties rootProperties;

	@PostConstruct
	public void exposeResponse() {
		rootProperties.add( "response", new ResponseEntityProperty( restRequest ) );
	}

	static class ResponseEntityProperty
	{
		private final RestRequest request;

		ResponseEntityProperty( RestRequest request ) {
			this.request = request;
		}

		public HttpHeaders getHeaders() {
			return request.getResponseEntity().getHeaders();
		}

		public String getLocation() {
			URI location = getHeaders().getLocation();
			if ( location != null ) {
				return location.toString();
			}
			return null;
		}

		public Object getBody() {
			return request.getBody();
		}

		public HttpStatus getStatusCode() {
			return request.getStatusCode();
		}

	}
}
