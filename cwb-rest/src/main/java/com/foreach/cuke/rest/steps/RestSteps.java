/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.rest.steps;

import com.foreach.cuke.core.steps.AbstractCoreSteps;
import com.foreach.cuke.core.util.FileResource;
import com.foreach.cuke.core.util.FileResourceResolver;
import com.foreach.cuke.core.util.MapChecker;
import com.foreach.cuke.core.util.ObjectMatcher;
import com.foreach.cuke.rest.authentication.Authentication;
import com.foreach.cuke.rest.util.*;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.formatter.model.Comment;
import gherkin.formatter.model.DataTableRow;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.yaml.snakeyaml.Yaml;

import java.util.*;

import static org.junit.Assert.*;

@Component
public class RestSteps extends AbstractCoreSteps
{
	private static final String HTTP_METHODS =
			"get|post|put|head|delete|options|patch|trace|GET|POST|PUT|HEAD|DELETE|OPTIONS|PATCH|TRACE";

	private static final String COUNT_COMPARISON = "(?: (less than|more than|at least|at most))?";

	@Autowired
	private ObjectMatcher objectMatcher;

	@Autowired
	private RestUrlBuilder restUrlBuilder;

	@Autowired
	private RestRequest restRequest;

	@Autowired
	private MapChecker mapChecker;

	@Autowired
	private FileResourceResolver fileResourceResolver;

	@Before
	public void resetRequest() {
		restRequest.reset();
	}

	@Then("^(?:the )?response status (?:should be|is) (\\d+)$")
	public void the_response_status_should_be( int statusCode ) throws Throwable {
		assertEquals( statusCode, restRequest.getStatusCode().value() );
	}

	@Then("^(?:the )?response location (?:should be|is) \"([^\"]*)\"$")
	public void the_response_location_should_be( String location ) throws Throwable {
		assertEquals( location, restRequest.getLocation() );
	}

	@And("^(?:the )?response (?:should contain|contains) \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void the_response_should_contain_with_value( String propertyName, String propertyValue ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		values.put( propertyName, propertyValue );

		the_response_should_contain( values );
	}

	@And("^(?:the )?response (?:should contain|contains) \"([^\"]*)\"$")
	public void the_response_should_contain( List<String> properties ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		for ( String property : properties ) {
			values.put( property, null );
		}

		the_response_should_contain( values );
	}

	@And("^(?:the )?response (?:should contain|contains):$")
	public void the_response_should_contain( Map<String, String> values ) throws Throwable {
		Map body = restRequest.getBodyAsMap();
		mapChecker.contains( body, values, true );
	}

	@And("^(?:the )?response entity \"([^\"]*)\" (?:should contain|contains) \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void the_named_response_entity_should_contain_with_value( String entityName,
	                                                                 String propertyName,
	                                                                 String propertyValue ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		values.put( propertyName, propertyValue );

		named_response_entity_should_contain( entityName, values );
	}

	@And("^(?:the )?response entity \"([^\"]*)\" (?:should contain|contains)" + COUNT_COMPARISON + " (\\d+) entit(?:ies|y)$")
	public void the_named_response_entity_has_children( String entityName, String comparisonAction, int childCount ) {
		Map entities = restRequest.getBodyAsMap();
		Object entity = mapChecker.retrieveSingleValue( entities, entityName );

		if ( entity == null ) {
			fail( "could not find requested entity " + entityName );
		}

		if ( entity instanceof Collection ) {
			compareCounts( comparisonAction, childCount, ( (Collection) entity ).size() );
		}
		else {
			fail( "Entity " + entityName + " was not a collection of results." );
		}
	}

	@And("^(?:the )?response entity \"([^\"]*)\" (?:should contain|contains) \"([^\"]*)\"$")
	public void the_named_response_entity_should_contain( String entityName,
	                                                      List<String> properties ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		for ( String property : properties ) {
			values.put( property, null );
		}

		named_response_entity_should_contain( entityName, values );
	}

	@And("^(?:the )?response entity \"([^\"]*)\" (?:should contain|contains):$")
	public void named_response_entity_should_contain( String entityName, Map<String, String> values ) throws Throwable {
		Map entities = restRequest.getBodyAsMap();

		List<Object> candidates = mapChecker.retrieveValues( entities, entityName );

		if ( candidates == null ) {
			fail( "could not find requested entity " + entityName );
		}

		mapChecker.contains( entityName, candidates, values, true );
/*
		if ( entity instanceof Map ) {
			mapChecker.contains( (Map) entity, values, true );
		}
		*/
		/*else if ( entity instanceof List ) {
			mapChecker.listContainsValidMapLenient( (List) entity, values );
		}
		else {
			LOG.warn( "unable to check entity \"{}\" as it is not a map", entityName );
		}
		*/

	}

	@And("^(?:the )?response entity \"([^\"]*)\" (?:should not contain|does not contain) \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void the_named_response_entity_should_not_contain_with_value( String entityName,
	                                                                     String propertyName,
	                                                                     String propertyValue ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		values.put( propertyName, propertyValue );

		checkNamedResponseEntityShouldNotContain( entityName, values );
	}

	@And("^(?:the )?response entity \"([^\"]*)\" (?:should not contain|does not contain) \"([^\"]*)\"$")
	public void the_named_response_entity_should_not_contain( String entityName,
	                                                          List<String> properties ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		for ( String property : properties ) {
			values.put( property, null );
		}

		checkNamedResponseEntityShouldNotContain( entityName, values );
	}

	@And("^(?:the )?response entity \"([^\"]*)\" (?:should not contain|does not contain):$")
	public void named_response_entity_should_not_contain( String entityName, DataTable dataTable ) throws Throwable {
		int numberOfCells = dataTable.topCells().size();
		if ( numberOfCells != 1 && numberOfCells != 2 ) {
			fail( "should not contain only supports tables of 1 or 2 columns" );
		}
		Map<String, String> values = new HashMap<String, String>();
		if ( numberOfCells == 1 ) {
			List<String> datatablecells = dataTable.asList( String.class );
			for ( String cell : datatablecells ) {
				values.put( cell, null );
			}
		}
		if ( numberOfCells == 2 ) {
			values = dataTable.asMap( String.class, String.class );
		}
		checkNamedResponseEntityShouldNotContain( entityName, values );
	}

	@And("^(?:the )?response entity (\\d+) (?:should contain|contains)" + COUNT_COMPARISON + " (\\d+) entit(?:ies|y)$")
	public void the_response_entity_has_children( int entityIndex, String comparisonAction, int childCount ) {
		int actualIndex = entityIndex - 1;

		List entities = restRequest.getBodyAsList();
		Object entity = entities.get( actualIndex );

		if ( entity == null ) {
			fail( "could not find requested entity with index " + entityIndex );
		}

		if ( entity instanceof Collection ) {
			compareCounts( comparisonAction, childCount, ( (Collection) entity ).size() );
		}
		else {
			fail( "Entity with index " + entityIndex + " was not a collection of results." );
		}
	}

	@And("^response entity (\\d+) (?:should contain|contains) \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void the_response_entity_should_contain_with_value( int entityIndex,
	                                                           String propertyName,
	                                                           String propertyValue ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		values.put( propertyName, propertyValue );

		response_entity_should_contain( entityIndex, values );
	}

	@And("^response entity (\\d+) (?:should contain|contains) \"([^\"]*)\"$")
	public void the_response_entity_should_contain( int entityIndex, List<String> properties ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		for ( String property : properties ) {
			values.put( property, null );
		}

		response_entity_should_contain( entityIndex, values );
	}

	@And("^response entity (\\d+) (?:should contain|contains):$")
	public void response_entity_should_contain( int entityIndex, Map<String, String> values ) throws Throwable {
		int actualIndex = entityIndex - 1;

		List entities = restRequest.getBodyAsList();
		Object entity = entities.get( actualIndex );
		if ( entity == null ) {
			LOG.debug( "did not find entity at index {}, which is a good thing", entityIndex );
		}
		if ( entity instanceof Map ) {
			mapChecker.contains( (Map) entity, values, true );
		}/*
		else if ( entity instanceof List ) {
			mapChecker.listContainsValidMapLenient( (List) entity, values );
		}*/
		else {
			LOG.warn( "unable to check entity {} as it is not a map", actualIndex );
		}
	}

	@And("^response entity (\\d+) (?:should not contain|does not contain) \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void the_response_entity_should_not_contain_with_value( int entityIndex,
	                                                               String propertyName,
	                                                               String propertyValue ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		values.put( propertyName, propertyValue );

		response_entity_should_not_contain( entityIndex, values );
	}

	@And("^response entity (\\d+) (?:should not contain|does not contain) \"([^\"]*)\"$")
	public void the_response_entity_should_not_contain( int entityIndex, List<String> properties ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		for ( String property : properties ) {
			values.put( property, null );
		}

		response_entity_should_not_contain( entityIndex, values );
	}

	@And("^response entity (\\d+) (?:should not contain|does not contain):$")
	public void response_entity_should_not_contain( int entityIndex, Map<String, String> values ) throws Throwable {
		int actualIndex = entityIndex - 1;

		List entities = restRequest.getBodyAsList();
		Object entity = entities.get( actualIndex );

		if ( entity == null ) {
			LOG.debug( "No entity found at index {}, which is a good thing", entityIndex );
		}
		else if ( entity instanceof Map ) {
			mapChecker.doesNotContain( (Map) entity, values, true );
		}
		/*else if ( entity instanceof List ) {
			if ( mapChecker.listContainsValidMapLenient( (List) entity, values ) ) {
				fail( "Found entity" );
			}
		}*/
		else {
			LOG.warn( "unable to check entity {} as it is not an array or a map", actualIndex );
		}
	}

	@And("^(?:the )?response headers (?:should contain|contains) \"([^\"]*)\"$")
	public void the_response_headers_should_contain( List<String> properties ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		for ( String property : properties ) {
			values.put( property, null );
		}

		the_response_headers_should_contain( values );
	}

	@And("^(?:the )?response headers (?:should contain|contains):$")
	public void the_response_headers_should_contain( Map<String, String> values ) throws Throwable {
		HttpHeaders body = restRequest.getHeaders();
		Map<String, String> singleValueMap = body.toSingleValueMap();
		mapChecker.contains( singleValueMap, values, true );
	}

	@And("^(?:the )?response headers (?:should contain|contains) \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void the_response_headers_should_contain_with_value( String propertyName,
	                                                            String propertyValue ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		values.put( propertyName, propertyValue );

		the_response_headers_should_contain( values );
	}

	@And("^(?:the )?response (?:should not contain|does not contain) \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void the_response_should_not_contain_with_value( String propertyName,
	                                                        String propertyValue ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		values.put( propertyName, propertyValue );

		the_response_should_not_contain( values );
	}

	@And("^(?:the )?response (?:should not contain|does not contain):$")
	public void the_response_should_not_contain( Map<String, String> values ) throws Throwable {
		Map body = restRequest.getBodyAsMap();

		assertNotNull( "Response body of the request was null.", body );

		for ( Map.Entry<String, String> row : values.entrySet() ) {
			if ( body.containsKey( row.getKey() ) ) {
				if ( row.getValue() != null ) {
					Object value = spel.getValue( row.getValue() );

					if ( !( value instanceof String ) || !StringUtils.isBlank( (String) value ) ) {
						assertFalse( objectMatcher.matches( value, body.get( row.getKey() ) ) );
					}
				}
				else {
					fail( "Property " + row.getKey() + " is present with value " + body.get( row.getKey() ) );
				}
			}
		}
	}

	@And("^(?:the )?response (?:should not contain|does not contain) \"([^\"]*)\"$")
	public void the_response_should_not_contain( List<String> properties ) throws Throwable {
		Map<String, String> values = new HashMap<String, String>();
		for ( String property : properties ) {
			values.put( property, null );
		}

		the_response_should_not_contain( values );
	}

	@Given("^(?:i )?(?:call|request|execute) (" + HTTP_METHODS + ") \"([^\"]*)\" with parameters[:]$")
	public void i_execute_with_parameters( String httpMethodString,
	                                       String path,
	                                       DataTable dataTable ) throws Throwable {
		HttpMethod httpMethod = HttpMethod.valueOf( StringUtils.upperCase( httpMethodString ) );

		UriComponentsBuilder uri = UriComponentsBuilder.fromUriString( restUrlBuilder.build( path ) );

		HttpHeaders headers = new HttpHeaders();
		if ( Arrays.asList( HttpMethod.POST, HttpMethod.PUT ).contains( httpMethod ) ) {
			headers.setContentType( MediaType.APPLICATION_FORM_URLENCODED );
		}
		headers.setAccept( Arrays.asList( MediaType.APPLICATION_JSON ) );

		Authentication defaultAuthentication = context.lookup( "rest.base.auth" );

		if ( defaultAuthentication != null ) {
			LOG.debug( "applying default request authentication {}", defaultAuthentication );
			defaultAuthentication.apply( uri, headers );
		}

		CsvConfiguration csvConfiguration = new CsvConfiguration();
		boolean isCsvRequest = false;

		MultiValueMap<String, Object> values = new LinkedMultiValueMap<String, Object>();

		if ( dataTable != null ) {
			DataTable cleanedTable = fixTableHeader( dataTable );
			boolean hasTypeSpec = cleanedTable.topCells().size() > 2;

			for ( Map<String, String> row : cleanedTable.asMaps( String.class, String.class ) ) {
				String name = spel.getValueAsString( row.get( "name" ) );
				String value = spel.getValueAsString( row.get( "value" ) );
				String type = row.get( "type" );

				if ( type != null ) {
					type = spel.getValueAsString( type );
				}
				else {
					type = "data";
				}

				// Content-type is a special case
				if ( StringUtils.equalsIgnoreCase( "content-type", name ) ) {
					headers.setContentType( MediaType.valueOf( value ) );
				}

				if ( StringUtils.startsWithIgnoreCase( type, "query" ) ) {
					uri.queryParam( name, value );
				}
				else if ( StringUtils.equalsIgnoreCase( type, "header" ) ) {
					if ( !StringUtils.equalsIgnoreCase( "content-type", name ) ) {
						headers.add( name, value );
					}
				}
				else if ( StringUtils.startsWithIgnoreCase( type, "auth" )
						|| ( !hasTypeSpec
						&& ( StringUtils.equals( name, "auth" ) || StringUtils.equals( name, "authentication" ) ) )
						) {
					Authentication authentication = context.lookup( value );

					if ( authentication == null ) {
						fail( "No authentication configured under variable " + value );
					}

					LOG.debug( "applying request authentication {}", authentication );
					authentication.apply( uri, headers );
				}
				else if ( StringUtils.equalsIgnoreCase( type, "data" ) ) {
					switch ( httpMethod ) {
						case POST:
						case PUT:
							values.add( name, value );
							break;
						default:
							uri.queryParam( name, value );
							break;
					}
				}
				else if ( StringUtils.equalsIgnoreCase( type, "resource" ) ) {
					final FileResource fileResource = fileResourceResolver.resolve( value );
					switch ( httpMethod ) {
						case POST:
						case PUT:
							values.add( name, new ByteArrayResource( fileResource.getContent() )
							{
								@Override
								public String getFilename() {
									return fileResource.getFileName();
								}
							} );
							break;
						default:
							fail( "Parameter type resource is only usable with POST and PUT" );
					}
				}
				else if ( StringUtils.equalsIgnoreCase( type, "csv-config" ) ) {
					isCsvRequest = true;
					handleCsvConfig( csvConfiguration, name, value );
				}
				else {
					fail( "Unknown parameter type: " + type );
				}
			}
		}

		HttpEntity<?> request;

		switch ( httpMethod ) {
			case POST:
			case PUT:
				request = new HttpEntity<MultiValueMap<?, ?>>( values, headers );
				break;
			default:
				request = new HttpEntity<String>( headers );

		}

		if ( isCsvRequest ) {
			configureMessageConverters( csvConfiguration );
		}
		else {
			configureMessageConverters( new CsvConfiguration() );
		}
		restRequest.execute(
				uri.build().toUriString(),
				httpMethod,
				request
		);
	}

	private void configureMessageConverters( CsvConfiguration csvConfiguration ) {
		List<HttpMessageConverter<?>> messageConverters = restRequest.getRestTemplate().getMessageConverters();
		List<HttpMessageConverter<?>> newMessageConverters = new ArrayList<HttpMessageConverter<?>>();
		for ( HttpMessageConverter<?> messageConverter : messageConverters ) {
			if ( messageConverter instanceof CsvToMapHttpMessageConverter ) {
				newMessageConverters.add(
						new CsvToMapHttpMessageConverter( csvConfiguration ) );
			}
			else {
				newMessageConverters.add( messageConverter );
			}
		}

		restRequest.getRestTemplate().getMessageConverters().clear();
		restRequest.getRestTemplate().getMessageConverters().addAll( newMessageConverters );
	}

	@When("^(?:i )?(?:call|request|execute) (" + HTTP_METHODS + ") \"([^\"]*)\" with data(?:[:])?$")
	public void i_call_post_with_data( String httpMethodString, String path, String data ) throws Throwable {
		Yaml yaml = new Yaml();

		String spelData = spel.replaceExpressions( data );
		Object instance = yaml.load( spelData );

		HttpMethod httpMethod = HttpMethod.valueOf( StringUtils.upperCase( httpMethodString ) );

		UriComponentsBuilder uri = UriComponentsBuilder.fromUriString( restUrlBuilder.build( path ) );

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType( MediaType.APPLICATION_JSON );
		headers.setAccept( Arrays.asList( MediaType.APPLICATION_JSON ) );

		Authentication defaultAuthentication = context.lookup( "rest.base.auth" );

		if ( defaultAuthentication != null ) {
			LOG.debug( "applying default request authentication {}", defaultAuthentication );
			defaultAuthentication.apply( uri, headers );
		}

		HttpEntity<?> request = new HttpEntity<Object>( instance, headers );

		restRequest.execute(
				uri.build().toUriString(),
				httpMethod,
				request
		);
	}

	@Given("^(?:i )?(?:call|request|execute) (" + HTTP_METHODS + ") \"([^\"]*)\"$")
	public void i_execute( String httpMethodString, String path ) throws Throwable {
		i_execute_with_parameters( httpMethodString, path, null );
	}

	@And("^(?:the )?response (?:should contain|contains)" + COUNT_COMPARISON + " (\\d+) entit(?:ies|y)$")
	public void the_response_should_contain_entities( String comparisonAction, int entityCount ) throws Throwable {
		List entities = restRequest.getBodyAsList();
		LOG.debug( "response contains {} entities", entities.size() );

		compareCounts( comparisonAction, entityCount, entities.size() );
	}

	private void compareCounts( String comparison, int expected, int actual ) {
		if ( StringUtils.equals( "at least", comparison ) ) {
			assertTrue( actual >= expected );
		}
		else if ( StringUtils.equals( "at most", comparison ) ) {
			assertTrue( actual <= expected );
		}
		else if ( StringUtils.equals( "more than", comparison ) ) {
			assertTrue( actual > expected );
		}
		else if ( StringUtils.equals( "less than", comparison ) ) {
			assertTrue( actual < expected );
		}
		else {
			assertEquals( expected, actual );
		}
	}

	private void handleCsvConfig( CsvConfiguration csvConfiguration, String name, String value ) {
		CsvParameter csvParameter = CsvParameter.get( name );
		switch ( csvParameter ) {
			case DELIMITER:
				if ( StringUtils.isNotEmpty( value ) ) {
					csvConfiguration.setDelimiter( value.toCharArray()[0] );
				}
				break;
			case QUOTE_CHAR:
				if ( StringUtils.isNotEmpty( value ) ) {
					csvConfiguration.setQuoteChar( value.toCharArray()[0] );
				}
				break;
			case END_OF_LINE_SYMBOLS:
				csvConfiguration.setEndOfLineSymbols( value );
				break;
			case WITH_HEADER:
				csvConfiguration.setWithHeaders( BooleanUtils.toBoolean( value ) );
				break;
			default:
				fail( "Unknown configuration parameter for CSV." );
		}
	}

	@When("^(?:the )?response (?:should contain|contains|has) data(?:[:])?$")
	@SuppressWarnings("unchecked")
	public void the_response_should_contain_data( String data ) throws Throwable {
		String spelData = spel.replaceExpressions( data );
		Yaml yaml = new Yaml();
		Map<String, ?> instance = yaml.loadAs( spelData, Map.class );

		mapChecker.contains( restRequest.getBodyAsMap(), instance, true );

	}

	private DataTable fixTableHeader( DataTable original ) {
		if ( hasHeaderRow( original ) ) {
			return original;
		}

		String[] header =
				original.topCells().size() == 2 ? new String[] { "name", "value" } : new String[] { "type", "name",
				                                                                                    "value" };

		List<DataTableRow> newRows = new LinkedList<DataTableRow>();
		newRows.add( new DataTableRow( Collections.<Comment>emptyList(), Arrays.asList( header ), 0 ) );
		newRows.addAll( original.getGherkinRows() );

		return new DataTable( newRows, original.getTableConverter() );
	}

	private void checkNamedResponseEntityShouldNotContain( String entityName, Map<String, String> values ) {
		Map entities = restRequest.getBodyAsMap();
		Object entity = mapChecker.retrieveSingleValue( entities, entityName );
		if ( entity == null ) {
			LOG.debug( "did not find entity {}, which is a good thing", entityName );
		}
		else if ( entity instanceof Map ) {
			mapChecker.doesNotContain( (Map) entity, values, true );
		}
/*		else if ( entity instanceof List ) {
			if ( mapChecker.listContainsValidMapLenient( (List) entity, values ) ) {
				fail( "Found entity" );
			}
		}
		*/
		else {
			LOG.warn( "unable to check entity {} as it is not a map", entityName );
		}
	}

	private boolean hasHeaderRow( DataTable dataTable ) {
		List<String> header = dataTable.topCells();

		boolean isHeader = header.contains( "name" ) & header.contains( "value" );

		if ( header.size() > 2 ) {
			isHeader |= header.contains( "type" );
		}

		return isHeader;
	}

}
