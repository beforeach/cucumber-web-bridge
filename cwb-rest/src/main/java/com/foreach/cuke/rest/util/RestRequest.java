/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.rest.util;

import com.foreach.cuke.core.spel.SpelEvaluationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.List;
import java.util.Map;

@Component
public class RestRequest
{
	private static final Logger LOG = LoggerFactory.getLogger( RestRequest.class );

	private RestTemplate restTemplate;
	private ResponseEntity<Object> responseEntity;
	private HttpStatusCodeException statusCodeException;

	@Autowired
	private SpelEvaluationContext context;

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate( RestTemplate restTemplate ) {
		this.restTemplate = restTemplate;
	}

	public ResponseEntity<Object> getResponseEntity() {
		if ( responseEntity == null ) {
			if ( statusCodeException != null ) {
				throw new AssertionError( "Attempt to get response but an exception occurred on the request",
				                          statusCodeException );
			}
			else {
				throw new AssertionError( "Attempt to get the response when no request has been executed" );
			}
		}

		return responseEntity;
	}

	public void setResponseEntity( ResponseEntity<Object> responseEntity ) {
		this.responseEntity = responseEntity;
	}

	public Object getBody() {
		return getResponseEntity().getBody();
	}

	public Map getBodyAsMap() {
		Object body = getBody();

		if ( body instanceof List ) {
			throw new AssertionError( "Attempt to use the response body as map while it is an array." );
		}

		return (Map) body;
	}

	public List getBodyAsList() {
		Object body = getBody();

		if ( body instanceof Map ) {
			throw new AssertionError( "Attempt to use the response body as array while it is a map." );
		}

		return (List) body;
	}

	public HttpHeaders getHeaders() {
		return responseEntity.getHeaders();
	}

	public HttpStatusCodeException getStatusCodeException() {
		return statusCodeException;
	}

	public void setStatusCodeException( HttpStatusCodeException statusCodeException ) {
		this.statusCodeException = statusCodeException;
	}

	public HttpStatus getStatusCode() {
		return statusCodeException != null ? statusCodeException.getStatusCode() : responseEntity.getStatusCode();
	}

	public String getLocation() {
		HttpHeaders headers = responseEntity.getHeaders();
		URI location = headers.getLocation();
		if ( location != null ) {
			return location.toString();
		}
		return null;
	}

	public void execute( String url, HttpMethod method, HttpEntity<?> requestEntity ) {
		try {
			LOG.debug( "rest url: {}", url );

			responseEntity = restTemplate.exchange( url, method, requestEntity, Object.class );

			context.put( "response", responseEntity );
		}
		catch ( HttpStatusCodeException hsc ) {
			statusCodeException = hsc;
		}
	}

	@PostConstruct
	public void reset() {
		restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add( new StringToObjectHttpMessageConverter() );
		restTemplate.getMessageConverters().add( new CsvToMapHttpMessageConverter( new CsvConfiguration() ) );

		// Because we are using LoggingResponseErrorHandler and the data will be extracted from error response
		// We will need to set OutputStreaming false
		// @see http://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/http/client/SimpleClientHttpRequestFactory.html#setOutputStreaming-boolean-
		SimpleClientHttpRequestFactory factory = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
		factory.setOutputStreaming( false );

		restTemplate.setErrorHandler( new LoggingResponseErrorHandler() );

		responseEntity = null;
		statusCodeException = null;

		context.getVariables().remove( "response" );
	}
}
