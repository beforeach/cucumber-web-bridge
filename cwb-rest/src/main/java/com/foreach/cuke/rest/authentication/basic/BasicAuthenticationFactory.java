/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.rest.authentication.basic;

import com.foreach.cuke.core.spel.SpelExpressionExecutor;
import com.foreach.cuke.rest.authentication.Authentication;
import com.foreach.cuke.rest.authentication.AuthenticationFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class BasicAuthenticationFactory extends AuthenticationFactory
{
	@Autowired
	private SpelExpressionExecutor spel;

	public BasicAuthenticationFactory() {
		super( "basic" );
	}

	@Override
	public Authentication createAuthentication( Map<String, String> parameters ) {
		return new BasicAuthentication( parameters.get( "username" ), parameters.get( "password" ) );
	}
}
