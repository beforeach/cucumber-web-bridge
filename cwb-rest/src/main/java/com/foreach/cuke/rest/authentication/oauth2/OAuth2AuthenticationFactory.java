/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.rest.authentication.oauth2;

import com.foreach.cuke.core.spel.SpelEvaluationContext;
import com.foreach.cuke.rest.authentication.Authentication;
import com.foreach.cuke.rest.authentication.AuthenticationFactory;
import com.foreach.cuke.rest.util.LoggingResponseErrorHandler;
import com.foreach.cuke.rest.util.RestUrlBuilder;
import gherkin.deps.net.iharder.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

@Component
public class OAuth2AuthenticationFactory extends AuthenticationFactory
{
	private static final Logger LOG = LoggerFactory.getLogger( OAuth2AuthenticationFactory.class );

	private static final Collection<String> IGNORED_PARAMETERS = Arrays.asList( "url", "client_id", "client_secret" );

	@Autowired
	private RestUrlBuilder restUrlBuilder;

	@Autowired
	private SpelEvaluationContext context;

	public OAuth2AuthenticationFactory() {
		super( "oauth2" );
	}

	@Override
	public Authentication createAuthentication( Map<String, String> parameters ) {
		String urlPath = parameters.get( "url" );

		if ( urlPath == null ) {
			LOG.debug( "no oauth token url specified, assuming default /oauth/token on ${rest.base.url}" );
			urlPath = "/oauth/token";
		}

		String type = parameters.get( "type" );

		Map body;

		if ( StringUtils.equals( "manual", type ) ) {
			LOG.info( "manually creating an OAuth2 authentication token" );
			body = parameters;
		}
		else {

			UriComponentsBuilder uri = UriComponentsBuilder.fromUriString( restUrlBuilder.build( urlPath ) );

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType( MediaType.APPLICATION_JSON );
			headers.setAccept( Arrays.asList( MediaType.APPLICATION_JSON ) );

			if ( parameters.containsKey( "client_id" ) ) {
				LOG.info( "using client_id and client_secret from parameters" );
				String plainCreds = parameters.get( "client_id" )
						+ ":"
						+ parameters.get( "client_secret" );
				byte[] plainCredsBytes = plainCreds.getBytes();
				String base64Creds = Base64.encodeBytes( plainCredsBytes );

				headers.add( "Authorization", "Basic " + base64Creds );
			}
			else {
				Authentication defaultAuthentication = context.lookup( "rest.base.auth" );

				if ( defaultAuthentication != null ) {
					LOG.debug( "applying default request authentication {}", defaultAuthentication );
					defaultAuthentication.apply( uri, headers );
				}
			}

			// Build request
			for ( Map.Entry<String, String> parameter : parameters.entrySet() ) {
				if ( !IGNORED_PARAMETERS.contains( parameter.getKey() ) ) {
					uri.queryParam( parameter.getKey(), parameter.getValue() );
				}
			}

			RestTemplate template = new RestTemplate();
			template.setErrorHandler( new LoggingResponseErrorHandler() );
			HttpEntity<String> request = new HttpEntity<String>( headers );

			String url = uri.build().toUriString();
			LOG.debug( "calling oauth2 token endpoint {}", url );

			ResponseEntity<Map> response = template.exchange( url, HttpMethod.GET, request, Map.class );
			body = response.getBody();

			if ( response.getStatusCode() != HttpStatus.OK ) {
				throw new RuntimeException(
						"Fetching OAuth2 token failed with status code: " + response.getStatusCode() );
			}
		}

		String accessToken = (String) body.get( "access_token" );

		OAuth2Authentication oAuth2Authentication = new OAuth2Authentication( accessToken );
		oAuth2Authentication.setRefreshToken( (String) body.get( "refresh_token" ) );
		oAuth2Authentication.setScope( (String) body.get( "scope" ) );

		LOG.debug( "created OAuth2 authentication {}", oAuth2Authentication );

		return oAuth2Authentication;
	}
}
