/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.rest.util;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.supercsv.io.CsvListReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;

public class CsvToMapHttpMessageConverter implements HttpMessageConverter
{
	private CsvConfiguration csvConfiguration;

	public CsvToMapHttpMessageConverter( CsvConfiguration csvConfiguration ) {
		this.csvConfiguration = csvConfiguration;
	}

	@Override
	public boolean canRead( Class clazz, MediaType mediaType ) {
		for ( MediaType supportedMediaType : getSupportedMediaTypes() ) {
			if ( supportedMediaType.includes( mediaType ) ) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean canWrite( Class clazz, MediaType mediaType ) {
		return false;
	}

	@Override
	public List<MediaType> getSupportedMediaTypes() {
		return Arrays.asList( MediaType.parseMediaType( "text/csv" ) );
	}

	@Override
	public Object read( Class clazz, HttpInputMessage inputMessage )
			throws IOException, HttpMessageNotReadableException {
		List<Map<String, String>> items = new ArrayList<Map<String, String>>();
		Reader isReader = null;
		CsvListReader reader = null;
		try {
			isReader = new InputStreamReader( inputMessage.getBody() );

			reader = new CsvListReader( isReader, csvConfiguration.toCsvPreference() );
			List<String> values = reader.read();
			List<String> headerValues;
			if ( csvConfiguration.isWithHeaders() ) {
				headerValues = new ArrayList<String>( values );
			}
			else {
				headerValues = new LinkedList<String>();
				for ( int i = 0; i < values.size(); i++ ) {
					headerValues.add( String.valueOf( i ) );
				}
			}
			while ( values != null ) {
				Map<String, String> entities = new LinkedHashMap<String, String>();
				int i = 0;
				for ( String value : values ) {
					entities.put( headerValues.get( i ), value );
					i++;
				}
				items.add( entities );
				values = reader.read();
			}
		}
		finally {
			IOUtils.closeQuietly( reader );
			IOUtils.closeQuietly( isReader );

		}

		return Collections.singletonMap( "csv", items );
	}

	@Override
	public void write( Object o,
	                   MediaType contentType,
	                   HttpOutputMessage outputMessage ) throws IOException, HttpMessageNotWritableException {

	}
}
