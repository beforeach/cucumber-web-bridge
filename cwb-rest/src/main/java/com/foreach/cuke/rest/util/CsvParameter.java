/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.rest.util;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Andy Somers
 */
public enum CsvParameter
{
	DELIMITER( "delimiter" ),
	QUOTE_CHAR( "quote-char", "quotechar" ),
	END_OF_LINE_SYMBOLS( "end-of-line-symbols", "eol-symbols", "end-of-line" ),
	WITH_HEADER( "with-header" );

	private String[] name;

	CsvParameter( String... name ) {
		this.name = name;
	}

	public static CsvParameter get( String parameter ) {
		for ( CsvParameter csvParameter : values() ) {
			for ( String name : csvParameter.name ) {
				String checkName = StringUtils.remove( name, "-" );
				String checkParam = StringUtils.remove( parameter, "-" );
				checkParam = StringUtils.remove( checkParam, "_" );
				if ( StringUtils.equalsIgnoreCase( checkName, checkParam ) ) {
					return csvParameter;
				}
			}
		}
		return null;
	}
}
