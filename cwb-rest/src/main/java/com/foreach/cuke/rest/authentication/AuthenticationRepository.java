/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.rest.authentication;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class AuthenticationRepository
{
	private Map<AuthenticationKey, Authentication> cache = new HashMap<AuthenticationKey, Authentication>();

	@Autowired
	private Collection<AuthenticationFactory> authenticationFactories;

	/**
	 * Returns an authentication instance for the given type and parameters.
	 */
	public Authentication get( String authenticationType, Map<String, String> parameters ) {
		AuthenticationKey key = new AuthenticationKey( authenticationType, parameters );
		Authentication existing = cache.get( key );

		if ( existing == null ) {
			existing = create( authenticationType, parameters );
			cache.put( key, existing );
		}

		return existing;
	}

	private Authentication create( String authenticationType, Map<String, String> parameters ) {
		for ( AuthenticationFactory authenticationFactory : authenticationFactories ) {
			if ( StringUtils.equals( authenticationType, authenticationFactory.getAuthenticationType() ) ) {
				return authenticationFactory.createAuthentication( parameters );
			}
		}

		return null;
	}

	private static class AuthenticationKey
	{
		private final String authenticationType;
		private final Map<String, String> parameters;

		private AuthenticationKey( String authenticationType, Map<String, String> parameters ) {
			this.authenticationType = authenticationType;
			this.parameters = parameters;
		}

		@Override
		public boolean equals( Object o ) {
			if ( this == o ) {
				return true;
			}
			if ( o == null || getClass() != o.getClass() ) {
				return false;
			}

			AuthenticationKey that = (AuthenticationKey) o;

			if ( authenticationType != null ? !authenticationType.equals(
					that.authenticationType ) : that.authenticationType != null ) {
				return false;
			}
			if ( parameters != null ? !parameters.equals( that.parameters ) : that.parameters != null ) {
				return false;
			}

			return true;
		}

		@Override
		public int hashCode() {
			int result = authenticationType != null ? authenticationType.hashCode() : 0;
			result = 31 * result + ( parameters != null ? parameters.hashCode() : 0 );
			return result;
		}
	}
}
