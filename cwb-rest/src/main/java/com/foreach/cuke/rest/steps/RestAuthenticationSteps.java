/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.rest.steps;

import com.foreach.cuke.core.steps.AbstractCoreSteps;
import com.foreach.cuke.rest.authentication.Authentication;
import com.foreach.cuke.rest.authentication.AuthenticationRepository;
import cucumber.api.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class RestAuthenticationSteps extends AbstractCoreSteps
{
	@Autowired
	private AuthenticationRepository authenticationRepository;

	@Given("^(?:i )?(?:create |set )?(\\w+) authentication as default with parameters:$")
	public void authentication_as_default( String authenticationType,
	                                       Map<String, String> parameters ) throws Throwable {
		LOG.debug( "creating new default authentication of type {}, saved in variable rest.base.auth",
		           authenticationType );
		authentication_as_with_parameters( authenticationType, "rest.base.auth", parameters );
	}

	@Given("^(?:i )?(?:create |set )?(\\w+) authentication as \"([^\"]*)\" with parameters:$")
	public void authentication_as_with_parameters( String authenticationType,
	                                               String variableName,
	                                               Map<String, String> parameters ) throws Throwable {
		Authentication authentication =
				authenticationRepository.get( authenticationType, spel.replaceValues( parameters ) );

		context.put( variableName, authentication );
	}
}
