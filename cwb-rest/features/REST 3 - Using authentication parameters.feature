Feature: Authentication parameters can be used as properties

  Background:
    * save "${demosite.main}" as "rest.base.url"

  Scenario: Username and password can be fetched from a basic authentication
    Given basic authentication as "basic-auth" with parameters:
      | username | demo_client |
      | password | demo        |
    Then ensure that "#{lookup('basic-auth').username}" equals "demo_client"
    And ensure that "#{lookup('basic-auth').password}" equals "demo"

  Scenario: Tokens and details can be fetched from oauth authentication
    Given oauth2 authentication as "oauth2-auth" with parameters:
      | client_id     | demo_client        |
      | client_secret | demo               |
      | grant_type    | client_credentials |
      | scope         | full               |
    Then ensure that "#{lookup('oauth2-auth').accessToken.length() > 10}"
    And ensure that "#{lookup('oauth2-auth').refreshToken == null}"
    And ensure that "#{lookup('oauth2-auth').scope}" equals "full"
    Given oauth2 authentication as "oauth2-auth-other" with parameters:
      | client_id     | demo_client |
      | client_secret | demo        |
      | grant_type    | password    |
      | scope         | full        |
      | username      | admin       |
      | password      | admin       |
    Then ensure that "#{lookup('oauth2-auth-other').accessToken.length() > 10}"
    And ensure that "#{lookup('oauth2-auth-other').refreshToken.length() > 10}"
    And ensure that "#{lookup('oauth2-auth-other').scope}" equals "full"
    And ensure that "#{lookup('oauth2-auth-other').accessToken}" does not equal "#{lookup('oauth2-auth').accessToken}"
