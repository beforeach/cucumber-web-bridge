Feature: Csv responses

  Background:
    * save "${demosite.main}" as "rest.base.url"

  Scenario: Use standard preferences
    Given i call get "/rest/csv?withHeader=true" with parameters:
      | content-type | text/csv |
    Then the response status should be 200
    And the response entity "csv[1]" should contain "firstName" with value "John"

  Scenario Outline: Set delimiter
    Given i call get "/rest/csv?withHeader=true&delimiter=<delimiter>" with parameters:
      | name         | value       | type       |
      | content-type | text/csv    | header     |
      | delimiter    | <delimiter> | csv-config |
    Then the response status should be 200
    And the response entity "csv[1]" should contain "firstName" with value "John"

  Examples:
    | delimiter |
    | ;         |
    | \|        |
    | ,         |
    | \t        |

  Scenario Outline: Set quote character
    Given i call get "/rest/csv?withHeader=true&quoteChar=<quoteChar>" with parameters:
      | name         | value       | type       |
      | content-type | text/csv    | header     |
      | quoteChar    | <quoteChar> | csv-config |
    Then the response status should be 200
    And the response entity "csv[1]" should contain "firstName" with value "John"

  Examples:
    | quoteChar |
    | '         |
    | #         |
    | \|        |

  Scenario Outline: Set end of line string
    Given i call get "/rest/csv?withHeader=true&endOfLineSymbols=<eol>" with parameters:
      | name         | value    | type       |
      | content-type | text/csv | header     |
      | end-of-line  | <eol>    | csv-config |
    Then the response status should be 200
    And the response entity "csv[1]" should contain "firstName" with value "John"

  Examples:
    | eol  |
    | \r\n |
    | \n   |

  Scenario: Support line index for csv without headers
    Given i call get "/rest/csv?withHeader=false" with parameters:
      | name         | value    | type       |
      | content-type | text/csv | header     |
      | with-header  | false    | csv-config |
    Then the response status should be 200
    And the response entity "csv[0]" should contain "1" with value "John"
    And the response entity "csv[1]" should contain "1" with value "Bob"

  Scenario Outline: Combine different options
    Given i call get "/rest/csv?withHeader=true&endOfLineSymbols=<eol>&delimiter=<delimiter>&quoteChar=<quoteChar>" with parameters:
      | name         | value       | type       |
      | content-type | text/csv    | header     |
      | end-of-line  | <eol>       | csv-config |
      | delimiter    | <delimiter> | csv-config |
      | quoteChar    | <quoteChar> | csv-config |
    Then the response status should be 200
    And the response entity "csv[1]" should contain "firstName" with value "John"

  Examples:
    | eol  | delimiter | quoteChar |
    | \r\n | ;         | '         |
    | \n   | \|        | #         |

  Scenario: Make different consecutive calls
    Given i call get "/rest/csv?withHeader=false" with parameters:
      | name         | value    | type       |
      | content-type | text/csv | header     |
      | with-header  | false    | csv-config |
    Then the response status should be 200
    And the response entity "csv[0]" should contain "1" with value "John"
    And the response entity "csv[1]" should contain "1" with value "Bob"

    When i call get "/rest/simpleObject"
    Then the response status should be 200
    And response entity "user" should contain:
      | username | regular-one       |
      | email    | regular@gmail.com |
    And the response should contain data:
    """
    user:
      username: regular-one
      email: regular@gmail.com
    """

    Given i call get "/rest/csv?withHeader=false" with parameters:
      | name         | value    | type       |
      | content-type | text/csv | header     |
      | with-header  | false    | csv-config |
    Then the response status should be 200
    And the response entity "csv[0]" should contain "1" with value "John"
    And the response entity "csv[1]" should contain "1" with value "Bob"

  Scenario: Parse csv without specifying media type
    Given i call get "/rest/csv?withHeader=true"
    Then the response status should be 200
    And the response entity "csv[1]" should contain "firstName" with value "John"
    Given i call get "/rest/csv?withHeader=true&delimiter=;" with parameters:
      | name      | value | type       |
      | delimiter | ;     | csv-config |
    Then the response status should be 200
    And the response entity "csv[1]" should contain "firstName" with value "John"
