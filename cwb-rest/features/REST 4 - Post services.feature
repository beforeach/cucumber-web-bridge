Feature: POST urls

  Background:
    Given i save "${demosite.main}" as "rest.base.url"

  Scenario: Post YAML data
    When i call post "/rest/post" with data
    """
      username: #{id.scenario}
      password: #{id.scenario}
      email: #{id.scenario}@test.com
    """
    Then the response status should be 200
    And the response should contain "user"
    And the response contains:
      | user.username | #{id.scenario}          |
      | user.password | #{id.scenario}          |
      | user.email    | #{id.scenario}@test.com |
    And the response should contain data:
    """
      user:
        username: #{id.scenario}
        password: #{id.scenario}
        email: #{id.scenario}@test.com
    """

  Scenario: Get YAML data
    When i call get "/rest/simpleObject"
    Then the response status should be 200
    And response entity "user" should contain:
      | username | regular-one       |
      | email    | regular@gmail.com |
    And the response should contain data:
    """
    user:
      username: regular-one
      email: regular@gmail.com
    """

  Scenario: support complex lists - default SpEL
    And i call get "/rest/complexList"
    Then response entity 1 should contain "['detail']['answers']['answer']" with value "this is answer 1"
    And response entity 1 should contain "['options'][0]['option1']" with value "this is option 1"
    And response entity 1 should contain:
      | ['options'][0]['option1']       | this is option 1 |
      | ['detail']['answers']['answer'] | this is answer 1 |
    And response entity 1 should contain "['listOfLists'][0][0]['option1']" with value "option 1"

  Scenario: support complex lists not contains - default SpEL
    And i call get "/rest/complexList"
    Then response entity 1 should not contain "['detail']['answers']['answer2']" with value "this is answer 1"
    And response entity 1 should not contain "['detail']['answers']['answer1']" with value "this is answer 2"
    And response entity 1 should not contain "['detail']['answers']['answer4']"
    And response entity 1 should not contain "['optins'][5]"
    And response entity 1 should not contain "['optins'][5]['option1']"
    And response entity 1 should not contain "['optins'][1]['optin3']"

  Scenario: support list of lists - default SpEL
    And i call get "/rest/listOfLists"
    Then response entity 1 should contain "[0]['option1']" with value "option 2"

  Scenario: support complex lists - dot notation
    And i call get "/rest/complexList"
    Then response entity 1 should contain "detail.answers.answer" with value "this is answer 1"
    And response entity 1 should contain "options[1].option2" with value "this is option 2"
    And response entity 1 should contain:
      | options[0].option1    | this is option 1 |
      | detail.answers.answer | this is answer 1 |
    And response entity 1 should contain "listOfLists[0][0].option1" with value "option 1"

  Scenario: support complex lists not contains - dot notation
    And i call get "/rest/complexList"
    Then response entity 1 should not contain "detail.answers.answer2" with value "this is answer 1"
    Then response entity 1 should not contain "detail.answers.answer1" with value "this is answer 2"
    And response entity 1 should not contain "detail.answers.answer4"
    And response entity 1 should not contain "optins[5]"
    And response entity 1 should not contain "optins[5].optin1"
    And response entity 1 should not contain "optins[1].optin3"

  Scenario: support list of lists - dot notation
    And i call get "/rest/listOfLists"
    Then response entity 1 should contain "[0].option1" with value "option 2"

  Scenario: support complex objects - default SpEL
    And i call get "/rest/complexObject"
    Then response entity "['detail']['answers']" should contain "['answer']"
    And response entity "['detail']['answers']" should contain "['answer']" with value "this is answer 1"
    And response entity "['detail']" should contain "['answers']['answer']" with value "this is answer 1"
    And response entity "['options'][]" should contain:
      | option2 | this is option 2 |
    And response entity "['detail']" should contain:
      | ['question']          | this is a question |
      | ['answers']['answer'] | this is answer 1   |

  Scenario: support complex objects not contains - default SpEL
    And i call get "/rest/complexObject"
    Then response entity "['detail']['answers']" should not contain "['answer5']"
    And response entity "['detail']['answers']" should not contain "['answer1']" with value "this is answer 2"
    And response entity "['detail']" should not contain "['answers']['answer1']" with value "this is answer 2"
    And response entity "['optins']" should not contain:
      | optin3      | this is optin 3       |
      | selected    | true                  |
      | description | this is a description |
    And response entity "['detail']" should not contain:
      | ['question']           | this is NOT a question |
      | ['answers']['answer1'] | this is not an answer  |

  Scenario: support complex objects - dot notation
    And i call get "/rest/complexObject"
    Then response entity "detail.answers" should contain "answer"
    And response entity "detail.answers" should contain "answer" with value "this is answer 1"
    And response entity "detail" should contain "answers.answer" with value "this is answer 1"
    And response entity "options[]" should contain:
      | option2 | this is option 2 |
    And response entity "options" should contain:
      | [].option2 | this is option 2 |
    And response entity "detail" should contain:
      | question       | this is a question |
      | answers.answer | this is answer 1   |

  Scenario: support complex objects not contains - dot notation
    And i call get "/rest/complexObject"
    Then response entity "detail.answers" should not contain "answer51"
    And response entity "detail.answers" should not contain "answer1" with value "this is not answer 1"
    And response entity "detail" should not contain "answers.answer1" with value "this is not answer 1"
    And response entity "options" should not contain:
      | optin3      | this is not optin 3       |
      | selected    | true                      |
      | description | this is not a description |
    And response entity "detail" should not contain:
      | question        | this is not a question |
      | answers.answer1 | this is not answer 1   |

  Scenario: support complex lists - square bracket notation
    When i call get "/rest/complexList"
    Then response entity 1 should contain "['options'][]['option1']" with value "this is option 1"
    And response entity 1 should contain "options[].option1" with value "this is option 1"

  Scenario: support complex objects - square bracket notation
    When i call get "/rest/complexObject"
    Then response entity "['options'][]" should contain "option1" with value "this is option 1"
    And response entity "options" should contain 2 entities
    And response entity "options" should contain at least 2 entities
    And response entity "options" should contain at most 2 entities
    And response entity "options" should contain at least 1 entity
    And response entity "options" should contain at most 3 entities
    And response entity "options" should contain more than 1 entity
    And response entity "options" should contain less than 3 entities
    And response entity "['options'][]" should not contain "option5"
    And response entity "['options'][]" should not contain:
      | option5 |
    And response entity "options[]" should contain "option1" with value "this is option 1"
    And response entity "options[]" should not contain "option5"

  Scenario: support list of lists - square bracket notation
    Given i call get "/rest/listOfLists"
    Then the response should contain 2 entities
    And the response contains at least 2 entities
    And the response should contain at most 4 entities
    And the response should contain at least 2 entities
    And the response contains at most 2 entities
    And the response contains more than 1 entity
    And the response should contain less than 3 entities
    And response entity 1 should contain 2 entities
    And response entity 1 should contain at least 2 entities
    And response entity 1 should contain at most 2 entities
    And response entity 1 should contain at least 1 entities
    And response entity 1 should contain at most 3 entities
    And response entity 1 should contain more than 0 entities
    And response entity 1 should contain less than 3 entities
    And response entity 1 should contain 2 entities
    And response entity 1 should contain 2 entities
    And response entity 2 should contain 2 entity
    And response entity 1 should contain "option1" with value "option 2"
    And response entity 1 should not contain "option5"
    And response entity 1 should contain "['option1']" with value "option 2"
    And response entity 1 should not contain "['option5']"

  Scenario: POST a file with resource notation
    When i call post "/rest/upload" with parameters:
      | type     | name         | value                                         |
      | header   | content-type | multipart/form-data                           |
      | resource | content      | #{id.scenario}-2.jpeg::/files/test-image.jpeg |
    Then the response status should be 200
