Feature: Authentication creation for rest services

  Background:
    * save "${demosite.main}" as "rest.base.url"
    * basic authentication as "rest.base.auth" with parameters:
      | username | demo_client |
      | password | demo        |

  Scenario: Explicit basic authentication for requesting oauth token
    Given basic authentication as "my-authentication" with parameters:
      | username | demo_client |
      | password | demo        |
    When i call get "/oauth/token" with parameters:
      | type  | name       | value              |
      | auth  | -          | my-authentication  |
      | query | grant_type | client_credentials |
      | query | scope      | full               |
    Then the response status should be 200

  Scenario: Explicit basic authentication but with wrong credentials
    Given basic authentication as "my-other-auth" with parameters:
      | username | demo_client.NOT |
      | password | demo            |
    When i call get "/oauth/token" with parameters:
      | type  | name       | value              |
      | auth  | -          | my-other-auth      |
      | query | grant_type | client_credentials |
      | query | scope      | full               |
    Then the response status should be 401

  Scenario: Basic authentication as the default configured in Background
    When i call get "/oauth/token" with parameters:
      | type  | name       | value              |
      | query | grant_type | client_credentials |
      | query | scope      | full               |
    Then the response status should be 200

  Scenario: OAuth2 access token to get the current user
    Given oauth2 authentication as "oauth2-auth" with parameters:
      | client_id     | demo_client |
      | client_secret | demo        |
      | grant_type    | password    |
      | scope         | full        |
      | username      | admin       |
      | password      | admin       |
    When i call get "/api/principal" with parameters:
      | authentication | oauth2-auth |
    Then the response status should be 200
    And the response should contain:
      | username            | admin |
      | authentication_type | user  |
