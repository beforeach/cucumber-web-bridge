Feature: REST webservices without authentication

  Background:
    * save "${demosite.main}" as "rest.base.url"

  Scenario: Without any parameters the response should be unauthorized
    When i call get "/oauth/token"
    Then the response status should be 401

  Scenario: With missing parameters it should be a wrong request
    * basic authentication as "rest.base.auth" with parameters:
      | username | demo_client |
      | password | demo        |
    When i call get "/oauth/token"
    Then the response status should be 400

  Scenario: Valid oauth token call with querystring parameters
    When i call get "/oauth/token" with parameters:
      | name          | value              |
      | client_id     | demo_client        |
      | client_secret | demo               |
      | grant_type    | client_credentials |
      | scope         | full               |
    Then the response status should be 200

  Scenario: Valid oauth token call with querystring and header combined
    When i call get "/oauth/token" with parameters:
      | type   | name          | value                          |
      | header | Authorization | Basic ZGVtb19jbGllbnQ6ZGVtbw== |
      | query  | grant_type    | client_credentials             |
      | query  | scope         | full                           |
    Then the response status should be 200
    And the response should contain:
      | token_type   | bearer |
      | scope        | full   |
      | access_token |        |
      | expires_in   |        |
    And the response should contain "token_type" with value "bearer"
    And the response contains "expires_in"
    And response should contain "expires_in,token_type"
    But the response should not contain:
      | token_type    | bla |
      | refresh_token |     |
    And the response should not contain "scope" with value "FULL"
    And the response should not contain "scopé,tokén_type"


