Cucumber Web Bridge
=======================

About
-----

Cucumber Web Framework is a Java library providing Cucumber steps for (simple) web functions.

It provides the following facilities:

* SAHI bridge using SAHI as an implementation for the browser calls and provides a framework for defining a browser to use,
configuring different target environments etc.
* REST bridge using Java Spring REST templates to execute and query REST webservices.

More information: [https://foreach.atlassian.net/wiki/display/CWB/Cucumber+Web+Bridge+Home](https://foreach.atlassian.net/wiki/display/CWB/Cucumber+Web+Bridge+Home)

### Contributing
Contributions in the form of pull requests are greatly appreciated.

### Building from source
The source can be built using [Maven][] with JDK 8.

### License
Licensed under version 2.0 of the [Apache License][].

[Maven]: http://maven.apache.org
[Apache License]: http://www.apache.org/licenses/LICENSE-2.0