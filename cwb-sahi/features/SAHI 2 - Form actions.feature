Feature: Form actions
   With these steps you can:

   * Fill in value in a texbox
   * Fill in value in a password textbox
   * Choose a radiobutton
   * Select a value from a dropdown
   * Uncheck a field
   * Check a field

  Background:
    Given i am on "${demosite.main}/secure/login"
    When i fill in text "Username" with "admin"
    And i fill in password "Password" with "admin"
    And i press "Log in"
    Then i should be on "${demosite.main}/secure/"

  Scenario: Fill in small form
    Given i am on "${demosite.main}/secure/users/create"
    When i fill in the following:
      | locator     | type     | value                        |
      | /username/i | textbox  | testuser#{id.scenario}       |
      | /password/i | password | test                         |
      | /email/i    | textbox  | test-#{id.scenario}@test.com |
      | Confirmed   | radio    | check                        |
    And i check "/deleted/i"
    And i press "Save"
    And i go to "${demosite.main}/secure/users"
    And i click on link "edit" 1 in row "/(.*)testuser#{id.scenario}(.*)/i"
    Then textbox "/username/i" should contain "testuser#{id.scenario}"

  Scenario: Focus can be put on and removed from certain elements
    * i focus on textbox "First Name"
    * i wait for 1000 ms
    * i remove focus from textbox "firstName"
    * i wait for 1000 ms
    * i put the focus on textbox "Last Name"
    * i wait for 1000 ms
    * remove the focus from textbox "/last name/i"
    * i wait for 1000 ms

  Scenario: Using labels is slower but allows less specific form element specification.
    Given i am on "${demosite.main}/secure/users/create"
    When i fill in "Username" with "testuser#{id.scenario}"
    And i fill in password "/Password/" with "1234"
    And i fill in "Email" 1 with "test-#{id.scenario}@test.com"
    And i fill in textbox "First Name" 1 with "firstname"
    And i fill in "Last Name" 1 with "lastname"
    And i fill in "/display name/i" with "displayName"
    And i check "LOCKED"
    And i check radio "Confirmed"
    And i select "Manager" from "Roles"
    And i select "Registered user" from "Roles"
    And i press "Save"
    And i go to "${demosite.main}/secure/users"
    And i click on link "edit" 1 in row "/(.*)testuser#{id.scenario}(.*)/i"
    Then the "/Username/" field should contain "testuser#{id.scenario}"
    And textbox "/Username/" should contain "testuser#{id.scenario}"
    And the "Last Name" field contains "lastname"
    And textbox "Last Name" contains "lastname"
    And the "displayName" field should contain "displayName"
    And "LOCKED" should be checked
    And "Confirmed" should be chosen
    And "Manager" should be selected in "/roles/i"
    And "Registered user" should be selected in "Roles"

  Scenario: Test visible elements on the form and element values
    Given i am on "${demosite.main}/secure/users/create"
    Then the following elements are visible:
      | locator         | type     |
      | firstName       | textbox  |
      | Password        | password |
      | /Restrictions/i | label    |
      | /Deleted?/i     | label    |
      | Save            | button   |
    And the following elements are not visible:
      | locator                     | type     |
      | /username cannot be empty/i | listItem |
      | really nothing              | link     |
    When i press "/Save/i"
    Then i should see listItem "/username cannot be empty/i"

  Scenario: Bulk entry and validation
    Given i am on "${demosite.main}/secure/users/create"
    When i fill in the following:
      | locator      | type     | value                        |
      | /username/i  | textbox  | testuser#{id.scenario}       |
      | /password/i  | password | test                         |
      | /email/i     | textbox  | test-#{id.scenario}@test.com |
      | Unconfirmed  | radio    | choose                       |
      | First Name   | text     | firstname                    |
      | Last Name    | text     | lastname                     |
      | Display Name | text     | displayname                  |
      | /disabled/i  | checkbox | check                        |
      | Roles        | select   | Administrator                |
      | Roles        | select   | Manager                      |
    And i press "/Save/i"
    Then the field values should be:
      | locator      | type     | value                        |
      | /username/i  | textbox  | testuser#{id.scenario}       |
      | /email/i     | textbox  | test-#{id.scenario}@test.com |
      | Unconfirmed  | radio    | chosen                       |
      | First Name   | text     | firstname                    |
      | Last Name    | text     | lastname                     |
      | Display Name | text     | displayname                  |
      | /disabled/i  | checkbox | checked                      |
      | /Roles/i     | select   | Administrator                |
      | /Roles/i     | select   | Manager                      |

  Scenario: If using the id then the field type must be specified unless regular textbox.
    Given i am on "${demosite.main}/secure/users/create"
    When i fill in password "password" with "testpwd"
    Then password "password" should contain "testpwd"

  Scenario Outline: Fill in html 5 elements
    Given i am on "${demosite.main}/cwb/html5"
    When i fill in <inputElement> "<name>" with "<inputValue>"
    Then <inputElement> "<name>" should contain "<inputValue>"

  Examples:
    | inputElement | name      | inputValue          |
    | tel          | telephone | 555 90210           |
    | search       | search    | search example      |
    | url          | url       | http://test.org     |
    | range        | range     | 6                   |
    | number       | quantity  | 11                  |
    | date         | inputDate | 11/11/2014          |
    | time         | inputTime | 11:11 AM            |
    | email        | email     | example@example.org |