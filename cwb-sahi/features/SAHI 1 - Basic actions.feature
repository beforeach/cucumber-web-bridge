Feature: Basic actions

  Scenario: Navigate around on the demo website
    Given i am on "${demosite.main}"
    When i follow "OAuth2"
    Then i should be on "${demosite.main}/test/oauth2"
    When i go to "${demosite.main}/"
    And i click on link "dedicated page"
    Then i should not see div "alert-danger"
    And link "/api/principal" is visible
    And i see link "/api/principal"

  Scenario: Click the browser back button
    Given i am on "${demosite.main}"
    When i follow "OAuth2"
    Then i follow "/api/principal"
    And i go back 1 page
    Then i should be on "${demosite.main}/test/oauth2"
    And i should see listItem "Private section"
    When i go forward 1 page
    Then i should be on "${demosite.main}/api/principal"
    And i should not see listItem "Private section"

  Scenario: Visibility wait steps
    * go to "${demosite.main}"
    * wait until listItem "/panel-list-item/[1]" in list "panel-list" is visible
    * wait 500 ms or until listItem "/panel-list-item/[1]" in list "panel-list" is visible
    * wait until listItem "/panel-list-item/[0]" is visible
    * wait 500 ms until listItem "/panel-list-item/" is visible
    * wait 5000 ms until listItem "some-not-existing-list-item" in list "some-not-existing-list-somewhere" is not visible

  Scenario: Exist wait steps
    * go to "${demosite.main}"
    * i wait until listItem "/panel-list-item/[1]" in list "panel-list" exists
    * i wait 500 ms until listItem "/panel-list-item/[1]" in list "panel-list" exists
    * wait until listItem "/panel-list-item/[1]" in list "panel-list" exists
    * wait 500 ms or until listItem "/panel-list-item/[1]" in list "panel-list" exists
    * i wait until listItem "/panel-list-item/" 2 exists
    * i wait 500 ms until listItem "/panel-list-item/" 2 exists
    * wait until listItem "/panel-list-item/[0]" exists
    * wait 500 ms until listItem "/panel-list-item/" exists
    * wait 5000 ms until listItem "some-not-existing-list-item" in list "some-not-existing-list-somewhere" does not exist

  Scenario: Go to demo website and pause
    Given i am on "${demosite.main}"
    Then pause

  Scenario: Go to demo website, highlight the first main block and pause
    Given i am on "${demosite.main}"
    Then highlight listItem "/panel-list-item/[0]"
    And highlight listItem "/panel-list-item/[1]" in list "panel-list"
