Feature: Complex actions
  Illustrates direct use of the SAHI Browser API to perform the same actions.

  Scenario: Do some form actions using EL statements.
    Given i save "${demosite.main}/secure/login" as "demosite login"
    Given i execute "#{browser.navigateTo(lookup('demosite login'))}"
    When i execute "#{browser.textbox('username').value = 'test name'}"
    Then ensure that "#{browser.textbox('/username/i').value}" equals "test name"
    And i execute "#{browser.waitFor(2000)}"

  Scenario: Positioned and context element selection with EL statements.
    Given i am on "${demosite.main}"
    Then ensure that "#{browser.listItem(1).in(browser.div('panel-body[0]')).text}" equals "OAuth2"
    And ensure that "#{browser.listItem('/secure/i[0]').text}" equals "/secure/: the administration section for users with the administrative permission"
    And i execute "#{browser.waitFor(1000)}"

  Scenario: Positioned and context element selection using regular steps.
    Given i am on "${demosite.main}"
    When i save "#{browser.div('panel-body[0]')}" as "parent"
    Then ensure that "#{browser.listItem(1).in(lookup('parent')).text}" equals "OAuth2"
    When i save div "panel-body[0]" as "otherParent"
    Then ensure that "#{browser.listItem(1).in(lookup('otherParent')).text}" equals "OAuth2"
    And listItem "/users/i[1]" should equal "/secure/: the administration section for users with the administrative permission"
    And ensure that listItem "/users/i" 2 equals "/secure/: the administration section for users with the administrative permission"

  Scenario: Nested parents through variables
    Given i am on "${demosite.main}"
    When i save div "panel-body" in div "panel panel-primary" as "panel body"
    Then element "${panel body}" should be visible
    And i should see list "panel-list" in parent "${panel body}"
    When i save list "panel-list" in element "${panel body}" as "menu items"
    Then i should see listItem 1 in element "${menu items}"
    And i should not see element "${some element that does not exist}"

  Scenario: Force link to open in popup
    Given i am on "${demosite.main}/"
    When i open "OAuth2" in popup "myPopup"
    And i switch to popup "myPopup"
    Then i should be on "${demosite.main}/test/oauth2"
    When i close popup "myPopup"
    And i go to "${demosite.main}/"
    Then i should be on "${demosite.main}/"