Feature: Lists all general steps and their alternatives
"""
  When there is an element selector they can be specified as:
  * type (link, textbox, list, listItem...)
  * locator (name of the link, regular expression - any SAHI locator should work)
  * position (in case there are multiple elements matching, which one do you want?)

  Both locator and position can be specified, but only one is required.  The following work:
  * div "mydiv"       # will select the div containing text "mydiv"
  * div 1             # will select the first div
  * div "mydiv" 1     # will select the first div containing text "mydiv"

  Position starts at 1 and not 0!  If you use the SAHI nested index it should start at 0.
  The following are equivalent:
  * div "mydiv" 1
  * div "mydiv[0]"

  As the locators are valid SAHI locators, it matches on multiple values/attributes depending on the
  element type selected.  See more details in the SAHI documentation:
  http://sahi.co.in/w/browser-accessor-apis
  """

  Background: All scenario's run on the Across demo website
    Given i am on "${demosite.main}"

  Scenario: Select the home link in many different ways
  """
    Note that SAHI lookups appear to be case insensitive by default in for example firefox, but to ensure
    cross-browser testing you should specify the case insensitive flag or rely on the actual case.
    Also combining a sahi regex with an indexed position can lead to unexpected results (especially if the
    regex is not specific enough it appears).
    """
    * link "Home" is visible
    * link "/home/i" is visible
    * link "/^Hom/" is visible
    * link "/^HOM/i" is visible
    * link "/hom$/" is not visible
    * link "/ome$/" is visible
    * link "/OME$/i" is visible
    * link "/^ome/" is not visible
    * link "Home[0]" is visible
    * link "Home" 1 is visible
    * link "Home[1]" is not visible
    * link "Home[0]" equals "#{browser.link('Home').text}"
    * link "Home" 1 equals "#{browser.link('Home').text}"
    * link 1 in list "panel-list" is visible
    * link 1 in list "panel-list" equals "#{browser.link('Home').text}"
    * link "Home" in list "panel-list" equals "#{browser.link('Home').text}"
    * link "Home" 1 in list "panel-list" equals "#{browser.link('Home').text}"

  Scenario: Click on the home link in many different ways
  """
    All core actions should allow an element to be identified by locator and/or position,
    with the option to add a parent element.
    """
    * i click on link "Home"
    * i click on link 1 in list "panel-list"
    * i click on link "Home" 1 in list "panel-list" 1
    * i double click on link "Home"
    * i double click on link "Home" 1 in list "panel-list" 1
    * i right click on link "Home"
    * i right click on link "Home" 1 in list "panel-list" 1

  Scenario: Steps to check visibility of elements
  """
    There are always a couple of formulations for the same steps.  Purpose is to improve
    readability in scenarios.  There are always i centric statements and more generic statements.
    Depending on the style of the scenario and the keyword at the front a different formulation can be used.
    """
    * link "Home" is visible
    * link "Home" 1 is visible
    * link "Home" in list "panel-list" is visible
    * link "Home" 1 in list "panel-list" 1 is visible
    * link "Home" 2 is not visible
    * link "Home" 2 in list "panel-list" 1 is not visible
    * link "Home" 2 in div "not-existing-nav" 1 is not visible
    * link "Home" 1 should be visible
    * link "Home" 1 in list "panel-list" 1 should be visible
    * link "Home" 2 should not be visible
    * link "Home" 2 in list "panel-list" 1 should not be visible
    * select "Some select" should not be visible
    * select "Some select" in list "panel-list" 1 should not be visible
    * select "Some select" in div "not-existing-nav" 1 is not visible
    * i see link "Home" 1
    * i should see link "Home" 1
    * i see link "Home" 1 in list "panel-list" 1
    * i should see link "Home" 1 in list "panel-list" 1
    * i do not see link "Home" 2
    * i should not see link "Home" 2
    * i do not see link "Home" 2 in list "panel-list" 1
    * i should not see link "Home" 2 in list "panel-list" 1
  # When using tables, either locator or position (or both) columns can be specified.
  # Only requirement is that the first row contains the correct header.
    * the following elements are visible:
      | locator | type |
      | Home    | link |
    * the following elements should be visible:
      | type | position |
      | link | 1        |
    * the following elements are not visible:
      | locator | type | position |
      | Home    | link | 2        |
    * the following elements should not be visible:
      | locator             | type |
      | some unexisting div | div  |
    * the following elements in list "panel-list" 1 are visible:
      | locator | type | position |
      | Home    | link | 1        |
    * the following fields in list "panel-list" 1 should be visible:
      | locator | type | position |
      | Home    | link | 1        |
    * the following elements in list "panel-list" 1 are not visible:
      | locator | type | position |
      | Home    | link | 2        |
    * the following fields in list "not-existing-nav" 1 are not visible:
      | locator | type | position |
      | Home    | link | 2        |
    * the following elements in list "panel-list" 1 should not be visible:
      | locator | type | position |
      | Home    | link | 2        |

  Scenario: Steps to check existence of elements
  """
      In this case the check is only if the element is present in the dom.
      """
    * link "Home" exists
    * link "Home" 1 should exist
    * link "Home" in list "panel-list" exists
    * link "Home" 1 in list "panel-list" 1 should exist
    * link "Home" 2 does not exist
    * link "Home" 2 in list "panel-list" 1 does not exist
    * select "Some select" should not exist
    * select "Some select" in list "panel-list" 1 should not exist
    * select "Some select" in div "not-existing-nav" 1 does not exist
  # When using tables, either locator or position (or both) columns can be specified.
  # Only requirement is that the first row contains the correct header.
    * the following elements exist:
      | locator | type |
      | Home    | link |
    * the following elements should exist:
      | type | position |
      | link | 1        |
    * the following elements do not exist:
      | locator | type | position |
      | Home    | link | 2        |
    * the following elements should not exist:
      | locator             | type |
      | some unexisting div | div  |
    * the following elements in list "panel-list" 1 exist:
      | locator | type | position |
      | Home    | link | 1        |
    * the following fields in list "panel-list" 1 should exist:
      | locator | type | position |
      | Home    | link | 1        |
    * the following elements in list "panel-list" 1 do not exist:
      | locator | type | position |
      | Home    | link | 2        |
    * the following fields in list "not-existing-nav" 1 should not exist:
      | locator | type | position |
      | Home    | link | 2        |

  Scenario: Element value matching
  """
    Default string comparison depends on the string.ignore.case property in the ctf.properties file.
    If it is true (default), then matching will always be case insensitive.  If the property is false,
    you must specify case insensitive matching by providing the /i flag at the end of the string.

    Note that if you have a leading and trailing slash (or trailing /i), then you are forcing a regular
    expression match.  A regular expression match will always apply to part of the string unless
    beginning and end of line characters are present.
    """
    * link "Private section" matches "Private section"
    * link "Private section" does not match "about/i"
    * link "Private section" matches "/priva/i"
    * link "Private section" does not match "/bore/i"
    * link "Private section" matches "/^Priva/i"
    * link "Private section" does not match "/Priva$/i"
    * link "Private section" matches "/section$/i"
    * link "Private section" does not match "/^section/i"
    * link "Private section" matches "/^Private section$/i"
    * link "Private section" does not match "/^my private section$/i"

  Scenario: Element value selection
  """
    Almost all statements support equals, contains, starts with, ends with and matches.
    If you specify the expected value as a regular expression, then the behavior of all methods is the same
    and will do a full regex match. A regular expression takes precedence over contains, starts with etc.

    Depending on the property "string.ignore.case" in the ctf.properties file the casing of text
    will be ignored by default or not.

    Note: for this test to succeed in all browsers the string.ignorecase property should be set to true.
    """
    * link "Private section" should equal "Private section"
    * link "Private section" 1 equals "Private section"
    * link "Private section" in div "panel-body" should equal "Private section"
    * link "Private section" 1 in div "panel-body" 1 equals "Private section"
    * link "Private section" should not equal "about"
    * link "Private section" 1 does not equal "about"
    * link "Private section" in div "panel-body" should not equal "about"
    * link "Private section" 1 in div "panel-body" 1 does not equal "about"
    * link "Private section" should contain "secti"
    * link "Private section" 1 in div "panel-body" 1 contains "secti"
    * link "Private section" does not contain "abo"
    * link "Private section" 1 in div "panel-body" 1 should not contain "abo"
    * link "Private section" starts with "Priv"
    * link "Private section" 1 in div "panel-body" 1 should start with "Priv"
    * link "Private section" does not start with "foreach"
    * link "Private section" 1 in div "panel-body" 1 should not start with "foreach"
    * link "Private section" ends with "section"
    * link "Private section" 1 in div "panel-body" 1 should end with "section"
    * link "Private section" does not end with "info"
    * link "Private section" 1 in div "panel-body" 1 should not end with "info"
    * link "Private section" matches "/^Private section$/"
    * link "Private section" 1 in div "panel-body" 1 should match "/^Private section$/"
    * link "Private section" does not match "/^about$/"
    * link "Private section" 1 in div "panel-body" 1 should not match "/^about$/"
  # Table operations, note that the following are alternative keywords:
  # * element, field
  # * equal, are, should be, should equal
  # * do not equal, are not, should not be, should not equal
  # * contain, should contain
  # * do not contain, should not contain
  # * start with, should start with
  # * do not start with, should not start with
  # * end with, should end with
  # * do not end with, should not end with
  # * match, should match
  # * do not match, should not match
    * the element values should equal:
      | locator         | type | value           |
      | Private section | link | Private section |
    * the element values equal:
      | locator         | type | value           |
      | Private section | link | Private section |
    * the element values are:
      | locator         | type | position | value           |
      | Private section | link | 1        | Private section |
    * the element values should be:
      | locator         | type | position | value           |
      | Private section | link | 1        | Private section |
    * the element values in div "panel-body" 1 are:
      | locator         | type | value           |
      | Private section | link | Private section |
    * the field values are:
      | locator         | type | value           |
      | Private section | link | Private section |
    * the element values do not equal:
      | locator         | type | value |
      | Private section | link | about |
    * the element values in div "panel-body" are not:
      | locator         | type | value |
      | Private section | link | about |
    * the field values in div "panel-body" 1 should not be:
      | locator         | type | value |
      | Private section | link | about |
    * the element values should contain:
      | locator         | type | value |
      | Private section | link | secti |
    * the field values in div "panel-body" 1 contain:
      | locator         | type | value |
      | Private section | link | secti |
    * the element values should not contain:
      | locator         | type | value |
      | Private section | link | abo   |
    * the field values in div "panel-body" 1 do not contain:
      | locator         | type | value |
      | Private section | link | abo   |
    * the element values should start with:
      | locator         | type | value |
      | Private section | link | Priv  |
    * the field values in div "panel-body" 1 start with:
      | locator         | type | value |
      | Private section | link | Priv  |
    * the element values should not start with:
      | locator         | type | value   |
      | Private section | link | section |
    * the field values in div "panel-body" 1 do not start with:
      | locator         | type | value   |
      | Private section | link | section |
    * the element values should end with:
      | locator         | type | value   |
      | Private section | link | section |
    * the field values in div "panel-body" 1 end with:
      | locator         | type | value   |
      | Private section | link | section |
    * the element values should not end with:
      | locator         | type | value |
      | Private section | link | Priv  |
    * the field values in div "panel-body" 1 do not end with:
      | locator         | type | value |
      | Private section | link | Priv  |
    * the element values should match:
      | locator         | type | value               |
      | Private section | link | /^Private section$/ |
    * the field values in div "panel-body" 1 match:
      | locator         | type | value               |
      | Private section | link | /^Private section$/ |
    * the element values should not match:
      | locator         | type | value     |
      | Private section | link | /^about$/ |
    * the field values in div "panel-body" 1 do not match:
      | locator         | type | value     |
      | Private section | link | /^about$/ |

  Scenario: Element attribute matching
  """
    Elements allow their attributes to be matched.  The class step is additional because
    it looks for the class value possibly separated by whitespace.

    Note that retrieving the class as an attribute requires you to use className instead.
    """
    * i am on "${demosite.main}/cwb/tableoverview"
    * table "resultTable" has attribute "id"
    * table 1 in div "container" should have attribute "className"
    * table "resultTable" in div "container" does not have attribute "data-item"
    * table "resultTable" should not have attribute "someother"
    * table "resultTable" has attribute "id" with value "resultTable"
    * table 1 in div "container" should have attribute "class" with value "/table-striped/"
    * table "resultTable" does not have attribute "className" with value "table-striped"
    * table 1 in div "container" should not have attribute "id" with value "/resultb/"
    * table "resultTable" should have the following attributes:
      | id        | resultTable                     |
      | className | /table-striped/                 |
      | id        |                                 |
      | className | table table-striped table-hover |
    * table 1 in div "container" has the following attributes:
      | id        |
      | className |
    * table 1 in div "container" does not have the following attributes:
      | for       |                  |
      | className | /notable/        |
      | id        | otherResultTable |
      | data-item |                  |
    * table "resultTable" should not have the following attributes:
      | data-item |
      | name      |
  # Class checking
    * table "resultTable" should have class "table-striped"
    * table 1 in div "container" has class "table table-hover"
    * table "resultTable" does not have class "table-condensed"
    * table 1 in div "container" should not have class "has-error table"
