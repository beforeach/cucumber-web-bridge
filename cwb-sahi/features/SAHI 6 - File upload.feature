Feature: Test file upload

  Scenario: Upload a file
    Given i am on "${demosite.main}/cwb/upload"
    Then i should see file "file"
    When i fill in file "file" with "/files/test-image.jpeg"
    And i press "Upload"
    Then i should see row "/test-image.jpeg/" in table "file-details"
    Then i should see row "/48113 bytes/" in table "file-details"