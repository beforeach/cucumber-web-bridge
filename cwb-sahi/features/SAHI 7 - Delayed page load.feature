Feature: Supporting asynchronous loaders

  Scenario: SAHI should only start after page loads
    Given i am on "${demosite.main}/cwb/requirejs"
    When i click on link "event link"
    Then div "text-output" should contain "RequireJS loaded!"