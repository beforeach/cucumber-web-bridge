Feature: Feature number one

  Scenario: Navigate around on the demo website
    Given i am on "${demosite.main}"
    When i follow "OAuth2"
    Then i should be on "${demosite.main}/test/oauth2"
    When i go to "${demosite.main}/"
    And i click on link "dedicated page"
    Then i should not see div "alert-danger"
    And link "/api/principal" is visible
    And i see link "/api/principal"

  Scenario: Click the browser back button
    Given i am on "${demosite.main}"
    When i follow "OAuth2"
    Then i follow "/api/principal"
    And i go back 1 page
    Then i should be on "${demosite.main}/test/oauth2"
    And i should see listItem "Private section"
    When i go forward 1 page
    Then i should be on "${demosite.main}/api/principal"
    And i should not see listItem "Private section"

  Scenario: Fail
    Given i am on "${demosite.main}"
    Then i should see div "idonotexist"
