/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.steps;

import com.foreach.cuke.core.steps.StandardSteps;
import com.foreach.cuke.sahi.element.ElementDescriptor;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.junit.Assert.fail;

@Component
public final class StandardSahiSteps extends AbstractSahiSteps
{
	public static final int DEFAULT_WAIT_TIMEOUT = 10000;
	public static final int WAIT_INCREMENT = 300;

	public static final String ELEMENT_REGEX = "(?!response)(element|[\\w]+)(?: \"([^\"]*)\")?(?: (\\d+))?";
	public static final String ELEMENT_WITH_PARENT_REGEX = ELEMENT_REGEX + " in " + ELEMENT_REGEX;

	@Autowired
	private StandardSteps standardSteps;

	@Before
	public void before() {
		// Clear any possible popup
		browser.clearWindow();
	}

	@And("^(?:i )?highlight " + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + "$")
	public void highlight_in_parent( String type,
	                                 String locator,
	                                 Integer position,
	                                 String parentType,
	                                 String parentLocator,
	                                 Integer parentPosition ) throws Throwable {
		find( type, locator, position, find( parentType, parentLocator, parentPosition ) ).highlight();

		standardSteps.pause();
	}

	@And("^(?:i )?highlight " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void highlight( String type, String locator, Integer position ) throws Throwable {
		find( type, locator, position ).highlight();

		standardSteps.pause();
	}

	@And("^(?:i )?go back (\\d+) (?:page|pages)?$")
	public void go_back_in_browser_history( Integer numberOfPages ) {
		Integer history = -numberOfPages; // browser history are negative numbers
		manipulateBrowserHistory( history );
	}

	@And("^(?:i )?go forward (\\d+) (?:page|pages)?$")
	public void go_forward_in_browser_history( Integer numberOfPages ) {
		manipulateBrowserHistory( numberOfPages );
	}

	@And("^(?:i )?wait (?:(\\d+) ms (?:or )?)?until " + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (is visible|is not visible|exists|does not exist)?$")
	public void wait_until_exists_or_visible( Integer timeToWait,
	                                          String type,
	                                          String locator,
	                                          Integer position,
	                                          String parentType,
	                                          String parentLocator,
	                                          Integer parentPosition,
	                                          String action ) {
		boolean checkVisibility = StringUtils.containsIgnoreCase( action, "visible" );
		boolean status = !StringUtils.containsIgnoreCase( action, "not" );
		waitUntilExistsOrVisible( timeToWait, type, locator, position, parentType, parentLocator, parentPosition,
		                          status, checkVisibility );
	}

	@And("^(?:i )?wait (?:(\\d+) ms (?:or )?)?until " + StandardSahiSteps.ELEMENT_REGEX + " (is visible|is not visible|exists|does not exist)?$")
	public void wait_until_exists_or_visible( Integer timeToWait,
	                                          String type,
	                                          String locator,
	                                          Integer position,
	                                          String action ) {
		boolean checkVisibility = StringUtils.containsIgnoreCase( action, "visible" );
		boolean status = !StringUtils.containsIgnoreCase( action, "not" );
		waitUntilExistsOrVisible( timeToWait, type, locator, position, null, null, null, status, checkVisibility );
	}

	public void waitUntilExistsOrVisible( Integer timeToWait,
	                                      String type,
	                                      String locator,
	                                      Integer position,
	                                      String parentType,
	                                      String parentLocator,
	                                      Integer parentPosition,
	                                      boolean expectedStatus,
	                                      boolean checkVisibility ) {
		int actualTimeToWait = timeToWait == null || timeToWait <= 0 ? DEFAULT_WAIT_TIMEOUT : timeToWait;

		int totalWait = 0;
		boolean status;

		ElementDescriptor descriptor = new ElementDescriptor( locator, position, type );
		ElementDescriptor parent =
				parentType != null ? new ElementDescriptor( parentLocator, parentPosition, parentType ) : null;

		String successMessage = "exist";
		String failedMessage = "did not exist";

		if ( checkVisibility && expectedStatus ) {
			successMessage = "appear";
			failedMessage = "did not appear";
		}
		else if ( checkVisibility ) {
			successMessage = "disappear";
			failedMessage = "did not disappear";
		}
		else if ( !expectedStatus ) {
			successMessage = "not exist";
			failedMessage = "dit not stop existing";
		}

		LOG.debug( "Waiting for {}{} to " + successMessage, descriptor, parent != null ? " in " + parent : "" );

		do {
			if ( parent != null ) {
				ElementStub parentStub = elementHandleService.find( parent );

				if ( exists( parentStub ) ) {
					descriptor.setParent( parentStub );
					status = checkVisibility ? isVisible( elementHandleService.find( descriptor ) ) : exists(
							elementHandleService.find( descriptor ) );
				}
				else {
					status = false;
				}
			}
			else {
				status = checkVisibility ? isVisible( elementHandleService.find( descriptor ) ) : exists(
						elementHandleService.find( descriptor ) );
			}

			if ( status != expectedStatus ) {
				browser.waitFor( WAIT_INCREMENT );
				totalWait += WAIT_INCREMENT;
			}
		}
		while ( status != expectedStatus && totalWait < actualTimeToWait );

		if ( status != expectedStatus ) {
			LOG.warn( "{}{} " + failedMessage + " within {} ms", descriptor, parent != null ? " in " + parent : "",
			          actualTimeToWait );
			fail( descriptor.toString() + ( parent != null ? " in " + parent.toString() : "" ) + " " + failedMessage + " within " + actualTimeToWait + " ms" );
		}
		else {
			LOG.debug( "{}{} " + successMessage + "ed after {} ms", descriptor, parent != null ? " in " + parent : "",
			           totalWait );
		}

	}

	@Then("^(?:i )?wait for (\\d+) ms$")
	public void wait_for_ms( int timeToWait ) throws Throwable {
		browser.waitFor( timeToWait );
	}

	@And("^(?:i )?save " + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " as \"([^\"]*)\"$")
	public void save_as( String type,
	                     String locator,
	                     Integer position,
	                     String parentType,
	                     String parentLocator,
	                     Integer parentPosition,
	                     String variableName ) throws Throwable {
		context.put( variableName, find( type, locator, position, find( parentType, parentLocator, parentPosition ) ) );
	}

	@And("^(?:i )?save " + StandardSahiSteps.ELEMENT_REGEX + " as \"([^\"]*)\"$")
	public void save_as( String type, String locator, Integer position, String variableName ) throws Throwable {
		context.put( variableName, find( type, locator, position ) );
	}

	private void manipulateBrowserHistory( Integer history ) {
		browser.execute( "_sahi._call(history.go(" + history + "));" );
	}
}
