/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.scenario;

import com.foreach.cuke.core.scenario.ScenarioCallback;
import cucumber.api.Scenario;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

@Component
public class ScreenshotOnFailureCallback implements ScenarioCallback
{
	private static final Logger LOG = LoggerFactory.getLogger( ScreenshotOnFailureCallback.class );
	private static final String TARGET_DIR = "target/cucumber/pretty-html-reports/";

	@Value("#{systemProperties['takeScreenShotOnFailure']}")
	private Boolean takeScreenShotOnFailure;

	@Override
	public boolean accept( Scenario scenario ) {
		return scenario.isFailed();
	}

	@Override
	public void before( Scenario scenario ) {

	}

	@Override
	public void after( Scenario scenario ) {
		if ( takeScreenShotOnFailure == Boolean.TRUE ) {
			takeScreenshotOfScreen( scenario );
		}
	}

	private void takeScreenshotOfScreen( Scenario scenario ) {
		//Take a screenshot of every monitor
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		LOG.debug( "GraphicsEnvironment is headless: " + ge.isHeadlessInstance() );
		GraphicsDevice[] screens = ge.getScreenDevices();

		for ( int i = 0; i < screens.length; i++ ) {
			GraphicsDevice screen = screens[i];
			LOG.debug( "Screen " + screen.getIDstring() + " properties:" );
			DisplayMode displayMode = screen.getDisplayMode();
			LOG.debug( "DisplayMode: " + displayMode.getWidth() + "x" + displayMode.getHeight() );
			GraphicsConfiguration defaultConfiguration = screen.getDefaultConfiguration();
			Rectangle bounds = defaultConfiguration.getBounds();
			LOG.debug( "Bounds WxH: " + bounds.getWidth() + "x" + bounds.getHeight() );
			LOG.debug( "Bounds x&y: " + bounds.getX() + " - " + bounds.getY() );
			LOG.debug( "Bounds size: " + bounds.getSize().getWidth() + "x" + bounds.getSize().getHeight() );
			Robot robotForScreen;
			try {
				robotForScreen = new Robot( screen );

				//The screen bounds have an annoying tendency to have an offset x/y; we want (0,0) => (width, height)
				bounds.x = 0;
				bounds.y = 0;
				LOG.debug( "Bounds x&y after reset: " + bounds.getX() + " - " + bounds.getY() );
				LOG.debug( "Robot properties: " + robotForScreen.toString() );
				BufferedImage screenShot = robotForScreen.createScreenCapture( bounds );
				writeToFile( i, scenario, screenShot );
			}
			catch ( AWTException e ) {
				LOG.error( "Unable to create java.awt.Robot", e );
			}
		}
	}

	private void writeToFile( int screenId, Scenario scenario, BufferedImage screenShot ) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			ImageIO.write( screenShot, "png", outputStream );
			byte[] bytes = outputStream.toByteArray();
			String name = scenario.getName();
			String fileName = TARGET_DIR + name + "_" + screenId + ".png";
			LOG.debug( "Writing " + bytes.length + " bytes to " + fileName );
			FileUtils.writeByteArrayToFile( new File( fileName ), bytes );
			scenario.embed( bytes, "image/png" );
		}
		catch ( IOException e ) {
			LOG.error( "Unable to write image content to file", e );
		}
		finally {
			try {
				outputStream.flush();
			}
			catch ( IOException e ) {
				// do nothing
			}
			IOUtils.closeQuietly( outputStream );
		}
	}
}
