/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.browser;

import com.foreach.cuke.sahi.config.CwbSahiConfiguration;
import net.sf.sahi.client.Browser;
import net.sf.sahi.client.BrowserCondition;
import net.sf.sahi.client.ElementStub;
import net.sf.sahi.client.ExecutionException;

public class SahiBrowserDelegate extends SahiBrowser
{
	private final CwbSahiConfiguration.SahiBrowserFactory browserFactory;

	private SahiBrowser currentBrowser;

	public SahiBrowserDelegate( CwbSahiConfiguration.SahiBrowserFactory browserFactory ) {
		this.browserFactory = browserFactory;
	}

	private SahiBrowser currentBrowser() {
		if ( currentBrowser == null ) {
			currentBrowser = browserFactory.actualBrowser();
		}

		return currentBrowser;
	}

	public void setWindow( String windowName ) {
		currentBrowser().setWindow( windowName );
	}

	public void clearWindow() {
		if ( currentBrowser != null ) {
			currentBrowser.clearWindow();
		}
	}

	public boolean isPopup() {
		return currentBrowser().isPopup();
	}

	public boolean isPopup( String windowName ) {
		return currentBrowser().isPopup( windowName );
	}

	@Override
	public void restartPlayback() {
		currentBrowser().restartPlayback();
	}

	@Override
	public void navigateTo( String url ) throws ExecutionException {
		currentBrowser().navigateTo( url );
	}

	@Override
	public void navigateTo( String url, boolean forceReload ) throws ExecutionException {
		currentBrowser().navigateTo( url, forceReload );
	}

	@Override
	public void setXHRReadyStatesToWaitFor( String s ) {
		currentBrowser().setXHRReadyStatesToWaitFor( s );
	}

	@Override
	public void execute( String step ) throws ExecutionException {
		currentBrowser().execute( step );
	}

	@Override
	public void executeStep( String step ) throws ExecutionException {
		currentBrowser().executeStep( step );
	}

	@Override
	public void open() {
		currentBrowser().open();
	}

	@Override
	public void close() {
		if ( currentBrowser != null ) {
			currentBrowser.close();
		}
	}

	@Override
	public void kill() {
		if ( currentBrowser != null ) {
			currentBrowser.kill();
		}
	}

	public void reset(){
		this.kill();
		this.currentBrowser = null;
	}

	@Override
	public void setValue( ElementStub textbox, String value ) throws ExecutionException {
		currentBrowser().setValue( textbox, value );
	}

	@Override
	public void setFile( ElementStub textbox, String value ) throws ExecutionException {
		currentBrowser().setFile( textbox, value );
	}

	@Override
	public void setFile( ElementStub textbox, String value, String URL ) throws ExecutionException {
		currentBrowser().setFile( textbox, value, URL );
	}

	@Override
	public void keyDown( ElementStub elementStub, String keySequence, String combo ) throws ExecutionException {
		currentBrowser().keyDown( elementStub, keySequence, combo );
	}

	@Override
	public void keyDown( ElementStub elementStub, String keySequence ) throws ExecutionException {
		currentBrowser().keyDown( elementStub, keySequence );
	}

	@Override
	public void highlight( ElementStub element ) throws ExecutionException {
		currentBrowser().highlight( element );
	}

	@Override
	public void keyPress( ElementStub elementStub, String keySequence, String combo ) throws ExecutionException {
		currentBrowser().keyPress( elementStub, keySequence, combo );
	}

	@Override
	public void keyPress( ElementStub elementStub, String keySequence ) throws ExecutionException {
		currentBrowser().keyPress( elementStub, keySequence );
	}

	@Override
	public void click( ElementStub element ) throws ExecutionException {
		currentBrowser().click( element );
	}

	@Override
	public void blur( ElementStub elementStub ) {
		currentBrowser().blur( elementStub );
	}

	@Override
	public String getAttribute( ElementStub el, String attribute ) throws ExecutionException {
		return currentBrowser().getAttribute( el, attribute );
	}

	@Override
	public void doubleClick( ElementStub element ) throws ExecutionException {
		currentBrowser().doubleClick( element );
	}

	@Override
	public void rightClick( ElementStub element ) throws ExecutionException {
		currentBrowser().rightClick( element );
	}

	@Override
	public void check( ElementStub element ) throws ExecutionException {
		currentBrowser().check( element );
	}

	@Override
	public void uncheck( ElementStub element ) throws ExecutionException {
		currentBrowser().uncheck( element );
	}

	@Override
	public void focus( ElementStub element ) throws ExecutionException {
		currentBrowser().focus( element );
	}

	@Override
	public void removeFocus( ElementStub element ) throws ExecutionException {
		currentBrowser().removeFocus( element );
	}

	@Override
	public void mouseOver( ElementStub element ) throws ExecutionException {
		currentBrowser().mouseOver( element );
	}

	@Override
	public void mouseDown( ElementStub element ) throws ExecutionException {
		currentBrowser().mouseDown( element );
	}

	@Override
	public void mouseUp( ElementStub element ) throws ExecutionException {
		currentBrowser().mouseUp( element );
	}

	@Override
	public void dragDrop( ElementStub dragElement, ElementStub dropElement ) throws ExecutionException {
		currentBrowser().dragDrop( dragElement, dropElement );
	}

	@Override
	public void dragDropXY( ElementStub dragElement, int x, int y ) throws ExecutionException {
		currentBrowser().dragDropXY( dragElement, x, y );
	}

	@Override
	public String fetch( String expression ) throws ExecutionException {
		return currentBrowser().fetch( expression );
	}

	@Override
	public String fetch( ElementStub el ) throws ExecutionException {
		return currentBrowser().fetch( el );
	}

	@Override
	public String getText( ElementStub el ) throws ExecutionException {
		return currentBrowser().getText( el );
	}

	@Override
	public String getValue( ElementStub el ) throws ExecutionException {
		return currentBrowser().getValue( el );
	}

	@Override
	public boolean exists( ElementStub el ) {
		return currentBrowser().exists( el );
	}

	@Override
	public boolean exists( ElementStub el, boolean optimistic ) {
		return currentBrowser().exists( el, optimistic );
	}

	@Override
	public boolean isVisible( ElementStub el ) throws ExecutionException {
		return currentBrowser().isVisible( el );
	}

	@Override
	public boolean isVisible( ElementStub el, boolean optimistic ) throws ExecutionException {
		return currentBrowser().isVisible( el, optimistic );
	}

	@Override
	public String getSelectedText( ElementStub el ) throws ExecutionException {
		return currentBrowser().getSelectedText( el );
	}

	@Override
	public Browser popup( String popupName ) {
		return currentBrowser().popup( popupName );
	}

	@Override
	public Browser domain( String domainName ) {
		return currentBrowser().domain( domainName );
	}

	@Override
	public void waitFor( long timeout ) {
		currentBrowser().waitFor( timeout );
	}

	@Override
	public void waitFor( BrowserCondition condition, int timeout ) {
		currentBrowser().waitFor( condition, timeout );
	}

	@Override
	public void choose( ElementStub elementStub, String value, boolean append ) throws ExecutionException {
		currentBrowser().choose( elementStub, value, append );
	}

	@Override
	public void choose( ElementStub elementStub, String[] values, boolean append ) throws ExecutionException {
		currentBrowser().choose( elementStub, values, append );
	}

	@Override
	public void startRecording() {
		currentBrowser().startRecording();
	}

	@Override
	public void stopRecording() {
		currentBrowser().stopRecording();
	}

	@Override
	public String[] getRecordedSteps() {
		return currentBrowser().getRecordedSteps();
	}

	@Override
	public String lastAlert() throws ExecutionException {
		return currentBrowser().lastAlert();
	}

	@Override
	public String lastConfirm() throws ExecutionException {
		return currentBrowser().lastConfirm();
	}

	@Override
	public void expectPrompt( String message, String input ) throws ExecutionException {
		currentBrowser().expectPrompt( message, input );
	}

	@Override
	public void expectConfirm( String message, boolean input ) throws ExecutionException {
		currentBrowser().expectConfirm( message, input );
	}

	@Override
	public String lastPrompt() throws ExecutionException {
		return currentBrowser().lastPrompt();
	}

	@Override
	public void clearLastAlert() throws ExecutionException {
		currentBrowser().clearLastAlert();
	}

	@Override
	public void clearLastPrompt() throws ExecutionException {
		currentBrowser().clearLastPrompt();
	}

	@Override
	public void clearLastConfirm() throws ExecutionException {
		currentBrowser().clearLastConfirm();
	}

	@Override
	public String title() throws ExecutionException {
		return currentBrowser().title();
	}

	@Override
	public boolean checked( ElementStub el ) throws ExecutionException {
		return currentBrowser().checked( el );
	}

	@Override
	public void addURLMock( String urlPattern ) {
		currentBrowser().addURLMock( urlPattern );
	}

	@Override
	public void addURLMock( String urlPattern, String responseClass_method ) {
		currentBrowser().addURLMock( urlPattern, responseClass_method );
	}

	@Override
	public void removeURLMock( String urlPattern ) {
		currentBrowser().removeURLMock( urlPattern );
	}

	@Override
	public String lastDownloadedFileName() {
		return currentBrowser().lastDownloadedFileName();
	}

	@Override
	public void clearLastDownloadedFileName() {
		currentBrowser().clearLastDownloadedFileName();
	}

	@Override
	public void saveDownloadedAs( String newFilePath ) {
		currentBrowser().saveDownloadedAs( newFilePath );
	}

	@Override
	public void setSpeed( int interval ) {
		currentBrowser().setSpeed( interval );
	}

	@Override
	public void setStrictVisibilityCheck( boolean check ) {
		currentBrowser().setStrictVisibilityCheck( check );
	}

	@Override
	public boolean containsText( ElementStub el, String text ) {
		return currentBrowser().containsText( el, text );
	}

	@Override
	public boolean containsHTML( ElementStub el, String html ) {
		return currentBrowser().containsHTML( el, html );
	}

	@Override
	public String style( ElementStub el, String attribute ) {
		return currentBrowser().style( el, attribute );
	}

	@Override
	public void setSessionId( String sessionId ) {
		currentBrowser().setSessionId( sessionId );
	}

	@Override
	public String sessionId() {
		return currentBrowser().sessionId();
	}

	@Override
	public void rteWrite( ElementStub rte, String value ) {
		currentBrowser().rteWrite( rte, value );
	}

	@Override
	public void setBrowserJS( String browserJS ) {
		currentBrowser().setBrowserJS( browserJS );
	}

	@Override
	public boolean isChrome() {
		return currentBrowser().isChrome();
	}

	@Override
	public boolean isFirefox() {
		return currentBrowser().isFirefox();
	}

	@Override
	public boolean isFF() {
		return currentBrowser().isFF();
	}

	@Override
	public boolean isIE() {
		return currentBrowser().isIE();
	}

	@Override
	public boolean isSafari() {
		return currentBrowser().isSafari();
	}

	@Override
	public boolean isOpera() {
		return currentBrowser().isOpera();
	}

	@Override
	public int count( Object... args ) {
		return currentBrowser().count( args );
	}

	@Override
	public void keyDown( ElementStub element, int keyCode, int charCode ) {
		currentBrowser().keyDown( element, keyCode, charCode );
	}

	@Override
	public void keyUp( ElementStub element, int keyCode, int charCode ) {
		currentBrowser().keyUp( element, keyCode, charCode );
	}

	@Override
	public String clearPrintCalled() {
		return currentBrowser().clearPrintCalled();
	}

	@Override
	public Boolean printCalled() {
		return currentBrowser().printCalled();
	}

	@Override
	public ElementStub accessor( Object... args ) {
		return currentBrowser().accessor( args );
	}

	@Override
	public ElementStub button( Object... args ) {
		return currentBrowser().button( args );
	}

	@Override
	public ElementStub checkbox( Object... args ) {
		return currentBrowser().checkbox( args );
	}

	@Override
	public ElementStub image( Object... args ) {
		return currentBrowser().image( args );
	}

	@Override
	public ElementStub imageSubmitButton( Object... args ) {
		return currentBrowser().imageSubmitButton( args );
	}

	@Override
	public ElementStub link( Object... args ) {
		return currentBrowser().link( args );
	}

	@Override
	public ElementStub password( Object... args ) {
		return currentBrowser().password( args );
	}

	@Override
	public ElementStub radio( Object... args ) {
		return currentBrowser().radio( args );
	}

	@Override
	public ElementStub select( Object... args ) {
		return currentBrowser().select( args );
	}

	@Override
	public ElementStub submit( Object... args ) {
		return currentBrowser().submit( args );
	}

	@Override
	public ElementStub textarea( Object... args ) {
		return currentBrowser().textarea( args );
	}

	@Override
	public ElementStub textbox( Object... args ) {
		return currentBrowser().textbox( args );
	}

	@Override
	public ElementStub cell( Object... args ) {
		return currentBrowser().cell( args );
	}

	@Override
	public ElementStub table( Object... args ) {
		return currentBrowser().table( args );
	}

	@Override
	public ElementStub byId( Object... args ) {
		return currentBrowser().byId( args );
	}

	@Override
	public ElementStub byClassName( Object... args ) {
		return currentBrowser().byClassName( args );
	}

	@Override
	public ElementStub byXPath( Object... args ) {
		return currentBrowser().byXPath( args );
	}

	@Override
	public ElementStub bySeleniumLocator( Object... args ) {
		return currentBrowser().bySeleniumLocator( args );
	}

	@Override
	public ElementStub row( Object... args ) {
		return currentBrowser().row( args );
	}

	@Override
	public ElementStub div( Object... args ) {
		return currentBrowser().div( args );
	}

	@Override
	public ElementStub span( Object... args ) {
		return currentBrowser().span( args );
	}

	@Override
	public ElementStub activeElement( Object... args ) {
		return currentBrowser().activeElement( args );
	}

	@Override
	public ElementStub dList( Object... args ) {
		return currentBrowser().dList( args );
	}

	@Override
	public ElementStub dTerm( Object... args ) {
		return currentBrowser().dTerm( args );
	}

	@Override
	public ElementStub dDesc( Object... args ) {
		return currentBrowser().dDesc( args );
	}

	@Override
	public ElementStub abbr( Object... args ) {
		return currentBrowser().abbr( args );
	}

	@Override
	public ElementStub paragraph( Object... args ) {
		return currentBrowser().paragraph( args );
	}

	/**
	 * @deprecated
	 */
	@Override
	public ElementStub spandiv( Object... args ) {
		return currentBrowser().spandiv( args );
	}

	@Override
	public ElementStub option( Object... args ) {
		return currentBrowser().option( args );
	}

	@Override
	public ElementStub reset( Object... args ) {
		return currentBrowser().reset( args );
	}

	@Override
	public ElementStub file( Object... args ) {
		return currentBrowser().file( args );
	}

	@Override
	public ElementStub byText( Object... args ) {
		return currentBrowser().byText( args );
	}

	@Override
	public ElementStub cookie( Object... args ) {
		return currentBrowser().cookie( args );
	}

	@Override
	public ElementStub position( Object... args ) {
		return currentBrowser().position( args );
	}

	@Override
	public ElementStub label( Object... args ) {
		return currentBrowser().label( args );
	}

	@Override
	public ElementStub list( Object... args ) {
		return currentBrowser().list( args );
	}

	@Override
	public ElementStub listItem( Object... args ) {
		return currentBrowser().listItem( args );
	}

	@Override
	public ElementStub parentNode( Object... args ) {
		return currentBrowser().parentNode( args );
	}

	@Override
	public ElementStub parentCell( Object... args ) {
		return currentBrowser().parentCell( args );
	}

	@Override
	public ElementStub parentRow( Object... args ) {
		return currentBrowser().parentRow( args );
	}

	@Override
	public ElementStub parentTable( Object... args ) {
		return currentBrowser().parentTable( args );
	}

	@Override
	public ElementStub rte( Object... args ) {
		return currentBrowser().rte( args );
	}

	@Override
	public ElementStub iframe( Object... args ) {
		return currentBrowser().iframe( args );
	}

	@Override
	public ElementStub tableHeader( Object... args ) {
		return currentBrowser().tableHeader( args );
	}

	@Override
	public ElementStub heading1( Object... args ) {
		return currentBrowser().heading1( args );
	}

	@Override
	public ElementStub heading2( Object... args ) {
		return currentBrowser().heading2( args );
	}

	@Override
	public ElementStub heading3( Object... args ) {
		return currentBrowser().heading3( args );
	}

	@Override
	public ElementStub heading4( Object... args ) {
		return currentBrowser().heading4( args );
	}

	@Override
	public ElementStub heading5( Object... args ) {
		return currentBrowser().heading5( args );
	}

	@Override
	public ElementStub heading6( Object... args ) {
		return currentBrowser().heading6( args );
	}

	@Override
	public ElementStub hidden( Object... args ) {
		return currentBrowser().hidden( args );
	}

	@Override
	public ElementStub area( Object... args ) {
		return currentBrowser().area( args );
	}

	@Override
	public ElementStub map( Object... args ) {
		return currentBrowser().map( args );
	}

	@Override
	public ElementStub italic( Object... args ) {
		return currentBrowser().italic( args );
	}

	@Override
	public ElementStub bold( Object... args ) {
		return currentBrowser().bold( args );
	}

	@Override
	public ElementStub emphasis( Object... args ) {
		return currentBrowser().emphasis( args );
	}

	@Override
	public ElementStub strong( Object... args ) {
		return currentBrowser().strong( args );
	}

	@Override
	public ElementStub preformatted( Object... args ) {
		return currentBrowser().preformatted( args );
	}

	@Override
	public ElementStub code( Object... args ) {
		return currentBrowser().code( args );
	}

	@Override
	public ElementStub blockquote( Object... args ) {
		return currentBrowser().blockquote( args );
	}

	@Override
	public ElementStub xy( Object... args ) {
		return currentBrowser().xy( args );
	}

	@Override
	public ElementStub datebox( Object... args ) {
		return currentBrowser().datebox( args );
	}

	@Override
	public ElementStub datetimebox( Object... args ) {
		return currentBrowser().datetimebox( args );
	}

	@Override
	public ElementStub datetimelocalbox( Object... args ) {
		return currentBrowser().datetimelocalbox( args );
	}

	@Override
	public ElementStub emailbox( Object... args ) {
		return currentBrowser().emailbox( args );
	}

	@Override
	public ElementStub monthbox( Object... args ) {
		return currentBrowser().monthbox( args );
	}

	@Override
	public ElementStub numberbox( Object... args ) {
		return currentBrowser().numberbox( args );
	}

	@Override
	public ElementStub rangebox( Object... args ) {
		return currentBrowser().rangebox( args );
	}

	@Override
	public ElementStub searchbox( Object... args ) {
		return currentBrowser().searchbox( args );
	}

	@Override
	public ElementStub telbox( Object... args ) {
		return currentBrowser().telbox( args );
	}

	@Override
	public ElementStub timebox( Object... args ) {
		return currentBrowser().timebox( args );
	}

	@Override
	public ElementStub urlbox( Object... args ) {
		return currentBrowser().urlbox( args );
	}

	@Override
	public ElementStub weekbox( Object... args ) {
		return currentBrowser().weekbox( args );
	}

	@Override
	public int hashCode() {
		return currentBrowser().hashCode();
	}

	@Override
	public boolean equals( Object obj ) {
		return currentBrowser().equals( obj );
	}

	@Override
	public String toString() {
		return currentBrowser().toString();
	}
}
