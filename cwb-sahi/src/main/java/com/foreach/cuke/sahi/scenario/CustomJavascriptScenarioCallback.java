package com.foreach.cuke.sahi.scenario;

import com.foreach.cuke.core.scenario.CompositeScenarioCallback;
import com.foreach.cuke.core.scenario.SimpleScenarioCallback;
import com.foreach.cuke.core.scenario.TagScenarioCallback;
import com.foreach.cuke.core.spel.SpelEvaluationContext;
import com.foreach.cuke.sahi.config.CustomJavascriptInjector;
import cucumber.api.Scenario;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Component
public class CustomJavascriptScenarioCallback extends CompositeScenarioCallback
{
	@Autowired
	private CustomJavascriptInjector customJavascriptInjector;

	@Autowired
	private SpelEvaluationContext context;

	@PostConstruct
	private void init(){
		// specific tag
		addCallback( new TagScenarioCallback()
		{
			@Override
			protected SpelEvaluationContext getContext() {
				return context;
			}

			@Override
			protected String getTagName() {
				return "IncludeCustomJs";
			}

			@Override
			protected void before( String argument ) {
				Set<String> scenarioKeys = new HashSet<String>();
				String[] arguments = StringUtils.lowerCase( argument ).split( "," );
				if( ArrayUtils.isNotEmpty( arguments )){
					for( String key : arguments ){
						scenarioKeys.add( StringUtils.trim( key ) );
					}
					customJavascriptInjector.inject( scenarioKeys );
				}
			}

		} );

		// default
		addCallback( new SimpleScenarioCallback()
		{
			@Override
			public void before( Scenario scenario ) {
				customJavascriptInjector.inject( new HashSet<String>() ); // reset custom javascript includes
			}

		} );
	}

}
