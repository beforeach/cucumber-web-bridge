/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.steps;

import com.foreach.cuke.core.util.StringMatcher;
import cucumber.api.java.en.And;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Arne Vandamme
 */
@Component
public final class ElementAttributeSteps extends AbstractSahiSteps
{
	@Autowired
	private StringMatcher matcher;

	@And("^" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (should have|has|should not have|does not have) class \"([^\"]*)\"$")
	public void elementWithParentHasClass( String type,
	                                       String locator,
	                                       Integer position,
	                                       String parentType,
	                                       String parentLocator,
	                                       Integer parentPosition,
	                                       String action,
	                                       String classNames ) throws Throwable {
		ElementStub element = parentType != null
				? find( type, locator, position, find( parentType, parentLocator, parentPosition ) )
				: find( type, locator, position );

		String[] actualClassNames =
				StringUtils.trim( StringUtils.defaultString( element.getAttribute( "className" ) ) ).split( " " );
		String[] expectedClassNames =
				StringUtils.trim( StringUtils.defaultString( spel.getValueAsString( classNames ) ) ).split( " " );

		LOG.debug( "class values for {} are {}", element, StringUtils.join( actualClassNames, " " ) );

		boolean containsAll = true;

		for ( String expected : expectedClassNames ) {
			containsAll &= ArrayUtils.contains( actualClassNames, expected );
		}

		if ( StringUtils.contains( action, "not" ) ) {
			assertFalse( "All classes were present but expected not to be.", containsAll );
		}
		else {
			assertTrue( "Not all classes were present", containsAll );
		}
	}

	@And("^" + StandardSahiSteps.ELEMENT_REGEX + " (should have|has|should not have|does not have) class \"([^\"]*)\"$")
	public void elementHasClass( String type,
	                             String locator,
	                             Integer position,
	                             String action,
	                             String classNames ) throws Throwable {
		elementWithParentHasClass( type, locator, position, null, null, null, action, classNames );
	}

	@And("^" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (should have|has|should not have|does not have) the following attributes:$")
	public void elementWithParentAttributesTable( String type,
	                                              String locator,
	                                              Integer position,
	                                              String parentType,
	                                              String parentLocator,
	                                              Integer parentPosition,
	                                              String action,
	                                              List<List<String>> values ) throws Throwable {
		ElementStub element = parentType != null
				? find( type, locator, position, find( parentType, parentLocator, parentPosition ) )
				: find( type, locator, position );

		for ( List<String> attribute : values ) {
			if ( attribute.size() == 1 ) {
				verifyAttributePresence( element, attribute.get( 0 ), !StringUtils.contains( action, "not" ) );
			}
			else {
				verifyAttributeValue( element, attribute.get( 0 ), attribute.get( 1 ),
				                      !StringUtils.contains( action, "not" ) );
			}
		}
	}

	@And("^" + StandardSahiSteps.ELEMENT_REGEX + " (should have|has|should not have|does not have) the following attributes:$")
	public void elementAttributesTable( String type,
	                                    String locator,
	                                    Integer position,
	                                    String action,
	                                    List<List<String>> values ) throws Throwable {
		elementWithParentAttributesTable( type, locator, position, null, null, null, action, values );
	}

	@And("^" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should not|does not) have attribute \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void elementWithParentDoesNotHaveAttributeWithValue( String type,
	                                                            String locator,
	                                                            Integer position,
	                                                            String parentType,
	                                                            String parentLocator,
	                                                            Integer parentPosition,
	                                                            String attributeName,
	                                                            String attributeValue ) throws Throwable {
		ElementStub element = parentType != null
				? find( type, locator, position, find( parentType, parentLocator, parentPosition ) )
				: find( type, locator, position );

		verifyAttributeDoesNotHaveValue( element, attributeName, attributeValue );
	}

	@And("^" + StandardSahiSteps.ELEMENT_REGEX + " (?:should not|does not) have attribute \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void elementDoesNotHaveAttributeWithValue( String type,
	                                                  String locator,
	                                                  Integer position,
	                                                  String attributeName,
	                                                  String attributeValue ) throws Throwable {
		elementWithParentDoesNotHaveAttributeWithValue( type, locator, position, null, null, null, attributeName,
		                                                attributeValue );
	}

	@And("^" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should have|has) attribute \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void elementWithParentHasAttributeWithValue( String type,
	                                                    String locator,
	                                                    Integer position,
	                                                    String parentType,
	                                                    String parentLocator,
	                                                    Integer parentPosition,
	                                                    String attributeName,
	                                                    String attributeValue ) throws Throwable {
		ElementStub element = parentType != null
				? find( type, locator, position, find( parentType, parentLocator, parentPosition ) )
				: find( type, locator, position );

		verifyAttributeHasValue( element, attributeName, attributeValue );
	}

	@And("^" + StandardSahiSteps.ELEMENT_REGEX + " (?:should have|has) attribute \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void elementHasAttributeWithValue( String type,
	                                          String locator,
	                                          Integer position,
	                                          String attributeName,
	                                          String attributeValue ) throws Throwable {
		elementWithParentHasAttributeWithValue( type, locator, position, null, null, null, attributeName,
		                                        attributeValue );
	}

	@And("^" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should not|does not) have attribute \"([^\"]*)\"$")
	public void elementWithParentDoesNotHaveAttribute( String type,
	                                                   String locator,
	                                                   Integer position,
	                                                   String parentType,
	                                                   String parentLocator,
	                                                   Integer parentPosition,
	                                                   String attributeName ) throws Throwable {
		ElementStub element = parentType != null
				? find( type, locator, position, find( parentType, parentLocator, parentPosition ) )
				: find( type, locator, position );

		verifyAttributeNotPresent( element, attributeName );
	}

	@And("^" + StandardSahiSteps.ELEMENT_REGEX + " (?:should not|does not) have attribute \"([^\"]*)\"$")
	public void elementDoesNotHaveAttribute( String type,
	                                         String locator,
	                                         Integer position,
	                                         String attributeName ) throws Throwable {
		elementWithParentDoesNotHaveAttribute( type, locator, position, null, null, null, attributeName );
	}

	@And("^" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should have|has) attribute \"([^\"]*)\"$")
	public void elementWithParentHasAttribute( String type,
	                                           String locator,
	                                           Integer position,
	                                           String parentType,
	                                           String parentLocator,
	                                           Integer parentPosition,
	                                           String attributeName ) throws Throwable {
		ElementStub element = parentType != null
				? find( type, locator, position, find( parentType, parentLocator, parentPosition ) )
				: find( type, locator, position );

		verifyAttributePresent( element, attributeName );
	}

	@And("^" + StandardSahiSteps.ELEMENT_REGEX + " (?:should have|has) attribute \"([^\"]*)\"$")
	public void elementHasAttribute( String type,
	                                 String locator,
	                                 Integer position,
	                                 String attributeName ) throws Throwable {
		elementWithParentHasAttribute( type, locator, position, null, null, null, attributeName );
	}

	/**
	 * Check that an attribute is present on an {@link ElementStub}.
	 */
	public void verifyAttributePresent( ElementStub element, String attributeName ) {
		verifyAttributePresence( element, attributeName, true );
	}

	/**
	 * Check that an attribute is not present on an {@link ElementStub}.
	 */
	public void verifyAttributeNotPresent( ElementStub element, String attributeName ) {
		verifyAttributePresence( element, attributeName, false );
	}

	/**
	 * Check that an attribute is present or not on an {@link ElementStub}.
	 *
	 * @param element       to check
	 * @param attributeName to look for
	 * @param expected      true if attribute is expected to be present
	 */
	public void verifyAttributePresence( ElementStub element, String attributeName, boolean expected ) {
		String attributeValue = element.getAttribute( retrievAttributeName( attributeName ) );
		LOG.debug( "attribute {} has value {}", attributeName, attributeValue );
		if ( expected ) {
			assertTrue( "No value for attribute could be " + attributeName + " retrieved",
			            attributeValue != null && !StringUtils.equalsIgnoreCase( "undefined", attributeValue ) );
		}
		else {
			assertTrue( "Attribute " + attributeName + " is present",
			            attributeValue == null || StringUtils.equalsIgnoreCase( "undefined", attributeValue ) );
		}
	}

	/**
	 * Check that an attribute does not have a certain value.
	 */
	private void verifyAttributeDoesNotHaveValue( ElementStub element, String attributeName, String attributeValue ) {
		verifyAttributeValue( element, attributeName, attributeValue, false );
	}

	/**
	 * Check that an attribute has a certain value.
	 */
	public void verifyAttributeHasValue( ElementStub element, String attributeName, String attributeValue ) {
		verifyAttributeValue( element, attributeName, attributeValue, true );
	}

	/**
	 * Check that an attribute is present or not on an {@link ElementStub}.
	 *
	 * @param element        to check
	 * @param attributeName  to look for
	 * @param attributeValue to check against the actual value
	 * @param expected       true if attribute is supposed to have that value
	 */
	public void verifyAttributeValue( ElementStub element,
	                                  String attributeName,
	                                  String attributeValue,
	                                  boolean expected ) {
		String expectedValue = spel.getValueAsString( attributeValue );

		if ( StringUtils.isBlank( expectedValue ) ) {
			verifyAttributePresence( element, attributeName, expected );
		}
		else {
			String actualValue = element.getAttribute( retrievAttributeName( attributeName ) );

			LOG.debug( "attribute {} has value {}", attributeName, actualValue );

			if ( actualValue == null || StringUtils.equalsIgnoreCase( "undefined", actualValue ) ) {
				actualValue = "";
			}

			assertEquals( expected, matcher.equals( actualValue, expectedValue ) );
		}
	}

	/**
	 * Convert known standard HTML attribute names to known DOM attributes
	 */
	private String retrievAttributeName( String attributeName ) {
		String actual = spel.getValueAsString( attributeName );

		if ( "class".equals( actual ) ) {
			return "className";
		}
		if ( "for".equals( actual ) ) {
			return "htmlFor";
		}

		return actual;
	}
}
