/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.scenario;

import com.foreach.cuke.core.scenario.ScenarioCallback;
import com.foreach.cuke.sahi.util.DomExporter;
import cucumber.api.Scenario;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.CharEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class ExportDomOnFailureCallback implements ScenarioCallback
{
	private static final String TARGET_DIR = "target/cucumber/domdumps/";

	@Autowired
	private DomExporter domExporter;

	@Value("#{systemProperties['exportDomOnFailure']}")
	private Boolean exportDomOnFailure;

	@Override
	public boolean accept( Scenario scenario ) {
		return scenario.isFailed();
	}

	@Override
	public void before( Scenario scenario ) {
	}

	@Override
	public void after( Scenario scenario ) {
		if ( exportDomOnFailure == Boolean.TRUE ) {
			String name = scenario.getName();
			String dom = domExporter.getDom();
			try {
				FileUtils.writeStringToFile( new File( TARGET_DIR + name + ".html" ), dom, CharEncoding.UTF_8 );
			}
			catch ( IOException e ) {
				// do nothing
			}
		}
	}
}
