/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.element;

import com.foreach.cuke.sahi.util.CwbElementStub;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@DefaultElementHandler
@Component
public class RadioElementHandler extends ElementHandler
{
	public static final String TYPE = "radio";
	public static final String CHOOSE = "choose";
	public static final String CHOSEN = "chosen";

	@Override
	public boolean canHandle( ElementDescriptor descriptor ) {
		return StringUtils.equalsIgnoreCase( descriptor.getType(), TYPE );
	}

	@Override
	public ElementStub find( ElementDescriptor descriptor ) {
		return addParents( new ElementStub( CwbElementStub.RADIO, browser, descriptor.getSahiArguments() ),
		                   descriptor );
	}

	@Override
	public void set( ElementDescriptor descriptor ) {
		ElementStub element = find( descriptor );
		if ( StringUtils.equalsIgnoreCase( CHOOSE, descriptor.getValue() ) || StringUtils.startsWithIgnoreCase(
				CheckboxElementHandler.CHECKED, descriptor.getValue() ) ) {
			element.click();
		}
	}

	@Override
	public void verify( ElementDescriptor descriptor, boolean expectedOutcome ) {
		ElementStub element = find( descriptor );
		if ( StringUtils.equalsIgnoreCase( CHOSEN, descriptor.getValue() ) || StringUtils.startsWithIgnoreCase(
				CheckboxElementHandler.CHECKED, descriptor.getValue() ) ) {
			assertEquals( expectedOutcome, element.checked() );
		}
		else {
			assertNotEquals( expectedOutcome, element.checked() );
		}
	}
}
