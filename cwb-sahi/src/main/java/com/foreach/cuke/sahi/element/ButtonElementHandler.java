/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.element;

import com.foreach.cuke.sahi.util.CwbElementStub;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import static org.junit.Assert.assertEquals;

/**
 * Looks up in order: submit -> button -> imageSubmit
 */
@DefaultElementHandler
@Component
public class ButtonElementHandler extends ElementHandler
{
	public static final String TYPE = "button";

	@Override
	public boolean canHandle( ElementDescriptor descriptor ) {
		return StringUtils.startsWithIgnoreCase( descriptor.getType(), TYPE );
	}

	@Override
	public ElementStub find( ElementDescriptor descriptor ) {
		return addParents( new ElementStub( CwbElementStub.BUTTON, browser, descriptor.getSahiArguments() ),
		                   descriptor );
	}

	@Override
	public void set( ElementDescriptor descriptor ) {
		throw new RuntimeException( "Set impossible on " + descriptor );
	}

	@Override
	public void verify( ElementDescriptor descriptor, boolean expectedOutcome ) {
		ElementStub element = find( descriptor );
		assertEquals( expectedOutcome,
		              executeOperation( element.getText(), descriptor.getValue(), descriptor.getOperation() ) );
	}
}
