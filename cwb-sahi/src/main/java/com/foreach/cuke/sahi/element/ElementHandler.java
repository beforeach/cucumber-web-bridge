/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.element;

import com.foreach.cuke.core.util.StringMatcher;
import com.foreach.cuke.sahi.lookup.ElementStubLocator;
import com.foreach.cuke.sahi.util.DummyElementStub;
import net.sf.sahi.client.Browser;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.annotation.Annotation;

public abstract class ElementHandler implements Comparable<ElementHandler>
{
	public static final ElementStub ELEMENT_EMPTY = new DummyElementStub();

	protected final Logger LOG = LoggerFactory.getLogger( getClass() );

	@Autowired
	protected Browser browser;

	@Autowired
	protected StringMatcher matcher;

	@Autowired
	protected ElementStubLocator elementStubLocator;

	public int compareTo( ElementHandler o ) {
		return Integer.valueOf( isDefaultElementHandler( this ) ? 1000 + getPriority() : getPriority() ).compareTo(
				isDefaultElementHandler( o ) ? 1000 + o.getPriority() : o.getPriority() );
	}

	@Override
	public boolean equals( Object o ) {
		if ( this == o ) {
			return true;
		}
		if ( o == null || getClass() != o.getClass() ) {
			return false;
		}

		ElementHandler that = (ElementHandler) o;
		Integer priority1 = isDefaultElementHandler( this ) ? 1000 + this.getPriority() : this.getPriority();
		Integer priority2 = isDefaultElementHandler( that ) ? 1000 + that.getPriority() : that.getPriority();
		EqualsBuilder equalsBuilder = new EqualsBuilder();
		equalsBuilder.append( priority1, priority2 );
		return equalsBuilder.isEquals();
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		hashCodeBuilder.append( isDefaultElementHandler( this ) ? 1000 + getPriority() : getPriority() );
		return hashCodeBuilder.hashCode();
	}

	private static boolean isDefaultElementHandler( ElementHandler handler ) {
		for ( Annotation a : handler.getClass().getAnnotations() ) {
			if ( a instanceof DefaultElementHandler ) {
				return true;
			}
		}

		return false;
	}

	protected boolean executeOperation( String actual, String expected, VerifyOperation operation ) {
		switch ( operation ) {
			case EQUALS:
				return matcher.equals( actual, expected );
			case CONTAINS:
				return matcher.contains( actual, expected );
			case STARTS_WITH:
				return matcher.startsWith( actual, expected );
			case ENDS_WITH:
				return matcher.endsWith( actual, expected );
		}

		LOG.warn( "Unknown VerifyOperation: {}", operation );

		return false;
	}

	protected ElementStub addParents( ElementStub stub ) {
		// TODO extend with global scope parent
		return stub;
	}

	protected ElementStub addParents( ElementStub stub, ElementDescriptor descriptorWithFirstParent ) {
		return descriptorWithFirstParent.getParent() != null ? stub.in(
				addParents( descriptorWithFirstParent.getParent() ) ) : addParents( stub );
	}

	public int getPriority() {
		return 1;
	}

	public abstract boolean canHandle( ElementDescriptor descriptor );

	public abstract ElementStub find( ElementDescriptor descriptor );

	public abstract void set( ElementDescriptor descriptor );

	public abstract void verify( ElementDescriptor descriptor, boolean expectedOutcome );
}
