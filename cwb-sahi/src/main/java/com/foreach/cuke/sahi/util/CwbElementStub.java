/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.util;

/**
 * Custom element definitions, see cwb.js resource in com.foreach.cuke.sahi.util.
 */
public final class CwbElementStub
{
	public static final String BY_LABEL = "_byLabel";

	public static final String HEADING = "cwbHeading";
	public static final String TEXTBOX = "cwbTextbox";
	public static final String PASSWORD = "cwbPassword";
	public static final String CHECKBOX = "cwbCheckbox";
	public static final String RADIO = "cwbRadio";
	public static final String BUTTON = "cwbButton";
	public static final String SELECT = "cwbSelect";
	public static final String TINYMCE = "cwbTinyMCE";
	public static final String TINYMCE_TOOLBAR = "cwbTinyMCEToolbar";

	private CwbElementStub() {
	}
}
