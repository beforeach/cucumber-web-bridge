package com.foreach.cuke.sahi.element;

import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import static org.junit.Assert.assertEquals;

@DefaultElementHandler
@Component
public class MultipleSelectElementHandler extends SelectElementHandler
{

	@Override
	public boolean canHandle( ElementDescriptor descriptor ) {
		return isSelect( descriptor ) && isMultipleSelect( descriptor );
	}

	@Override
	public void set( ElementDescriptor descriptor ) {
		ElementStub element = find( descriptor );
		element.choose( descriptor.getValue(), true );
	}

	@Override
	public void verify( ElementDescriptor descriptor, boolean expectedOutcome ) {
		ElementStub element = find( descriptor );
		boolean equals = matcher.equals( element.getValue(), descriptor.getValue() );
		if ( !equals ) {
			for ( String selectedValue : StringUtils.split( element.getSelectedText(), "," ) ) {
				equals |= matcher.equals( selectedValue, descriptor.getValue() );
			}
		}
		assertEquals( expectedOutcome, equals );
	}
}
