/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.element;

import com.foreach.cuke.sahi.util.CwbElementStub;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import static org.junit.Assert.assertEquals;

@DefaultElementHandler
@Component
public class TextElementHandler extends ElementHandler
{
	public static final String TEXT = "text";
	public static final String TEL = "tel";
	public static final String SEARCH = "search";
	public static final String URL = "url";
	public static final String RANGE = "range";
	public static final String EMAIL = "email";
	public static final String DATE = "date";
	public static final String TIME = "time";
	public static final String NUMBER = "number";

	@Override
	public boolean canHandle( ElementDescriptor descriptor ) {
		return StringUtils.startsWithIgnoreCase( descriptor.getType(), TEXT ) ||
				StringUtils.startsWithIgnoreCase( descriptor.getType(), EMAIL ) ||
				StringUtils.startsWithIgnoreCase( descriptor.getType(), DATE ) ||
				StringUtils.startsWithIgnoreCase( descriptor.getType(), TEL ) ||
				StringUtils.startsWithIgnoreCase( descriptor.getType(), SEARCH ) ||
				StringUtils.startsWithIgnoreCase( descriptor.getType(), URL ) ||
				StringUtils.startsWithIgnoreCase( descriptor.getType(), TIME ) ||
				StringUtils.startsWithIgnoreCase( descriptor.getType(), RANGE ) ||
				StringUtils.startsWithIgnoreCase( descriptor.getType(), NUMBER );
	}

	@Override
	public ElementStub find( ElementDescriptor descriptor ) {
		return addParents( new ElementStub( CwbElementStub.TEXTBOX, browser, descriptor.getSahiArguments() ),
		                   descriptor );
	}

	@Override
	public void set( ElementDescriptor descriptor ) {
		ElementStub element = find( descriptor );
		element.setValue( descriptor.getValue() );
	}

	@Override
	public void verify( ElementDescriptor descriptor, boolean expectedOutcome ) {
		ElementStub element = find( descriptor );
		assertEquals( expectedOutcome,
		              executeOperation( element.getValue(), descriptor.getValue(), descriptor.getOperation() ) );
	}
}
