/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.util;

import com.foreach.cuke.sahi.browser.SahiBrowser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class RichTextTinyMce
{
	@Autowired
	private SahiBrowser browser;

	public void setHTML( String tinyMceId, String html ) {
		String concatenated = StringUtils.replace( html, "'", "\\'" );
		concatenated = StringUtils.replace( concatenated, "\n", "\\n" );
		browser.execute( "_setTinyMceText('" + tinyMceId + "','" + concatenated + "')" );
	}

	public void setPlainText( String tinyMceId, String text ) {
		String[] paragraphs = StringUtils.splitByWholeSeparator( StringUtils.replace( text, "\r", "" ), "\n\n" );
		setHTML( tinyMceId, "<p>" + StringUtils.join( paragraphs, "</p><p>" ) + "</p>" );
	}

	public String getHTML( String tinyMceId ) {
		return browser.fetch( "_getTinyMceContent('" + tinyMceId + "')" );
	}

	public String getPlainText( String tinyMceId ) {
		String html = getHTML( tinyMceId );
		Pattern pattern = Pattern.compile( "(<([^>]+)>)", Pattern.CASE_INSENSITIVE );
		Matcher matcher = pattern.matcher( html );

		return StringUtils.replaceEach( matcher.replaceAll( "" ), new String[] { "&nbsp;", "\n" }, new String[] { "", " " } ).trim();
	}

	public void selectText( String tinyMceId, String text ) {
		String cleaned = StringUtils.replace( text, "'", "\\'" );
		browser.execute( "_selectTinyMceText('" + tinyMceId + "','" + cleaned + "')" );
	}

	public void replaceText( String tinyMceId, String text, String replacement ) {
		String cleanedOriginal = StringUtils.replace( text, "'", "\\'" );
		String cleanedReplacement = StringUtils.replace( replacement, "'", "\\'" );
		browser.execute(
				"_replaceTinyMceText('" + tinyMceId + "','" + cleanedOriginal + "','" + cleanedReplacement + "')" );
	}

	public void removeText( String tinyMceId, String text ) {
		replaceText( tinyMceId, text, "" );
	}

	public void putTextCursor( String tinyMceId, TextCursor position, String text ) {
		String cleaned = StringUtils.replace( text, "'", "\\'" );
		if ( position == TextCursor.AFTER ) {
			browser.execute( "_putTinyMceCursorAfterText('" + tinyMceId + "','" + cleaned + "')" );
		}
		else {
			browser.execute( "_putTinyMceCursorBeforeText('" + tinyMceId + "','" + cleaned + "')" );
		}
	}

	public void putTextCursor( String tinyMceId, TextCursor position ) {
		if ( position == TextCursor.AFTER ) {
			browser.execute( "_putTinyMceCursorAfterSelection('" + tinyMceId + "')" );
		}
		else {
			browser.execute( "_putTinyMceCursorBeforeSelection('" + tinyMceId + "')" );
		}
	}

	public void insertText( String tinyMceId, String text ) {
		String cleaned = StringUtils.replace( text, "'", "\\'" );
		browser.execute( "_insertTinyMceText('" + tinyMceId + "','" + cleaned + "')" );
	}

	public void selectNodeContentsWithText( String tinyMceId, String text ) {
		String cleaned = StringUtils.replace( text, "'", "\\'" );
		browser.execute( "_selectTinyMceNodeContentsWithText('" + tinyMceId + "','" + cleaned + "')" );
	}

	public void selectNodeWithText( String tinyMceId, String text ) {
		String cleaned = StringUtils.replace( text, "'", "\\'" );
		browser.execute( "_selectTinyMceNodeWithText('" + tinyMceId + "','" + cleaned + "')" );
	}

	public void replaceTextSelection( String tinyMceId, String text ) {
		String cleaned = StringUtils.replace( text, "'", "\\'" );
		browser.execute( "_insertTinyMceText('" + tinyMceId + "','" + cleaned + "')" );
	}

	public void selectFirstNode( String tinyMceId, boolean contentsOnly ) {
		browser.execute( "_selectTinyMceFirstNode('" + tinyMceId + "', " + contentsOnly + ")" );
	}

	public void selectLastNode( String tinyMceId, boolean contentsOnly ) {
		browser.execute( "_selectTinyMceLastNode('" + tinyMceId + "', " + contentsOnly + ")" );
	}
}
