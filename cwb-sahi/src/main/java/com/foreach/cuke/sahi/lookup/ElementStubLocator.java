/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.lookup;

import com.foreach.cuke.sahi.util.DummyElementStub;
import net.sf.sahi.client.ElementStub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ElementStubLocator
{
	@Autowired
	private List<ElementStubLookup> lookups;

	public List<ElementStubLookup> getLookups() {
		return lookups;
	}

	public void setLookups( List<ElementStubLookup> lookups ) {
		this.lookups = lookups;
	}

	public ElementStub find( Object... arguments ) {
		return find( arguments, null );
	}

	public ElementStub find( Object[] arguments, ElementStub parent ) {
		for ( ElementStubLookup lookup : lookups ) {
			ElementStub element = lookup.find( arguments, parent );

			if ( element != null ) {
				return element;
			}
		}

		return new DummyElementStub();
	}

	public void add( ElementStubLookup lookup ) {
		lookups.add( lookup );
	}

	public void remove( ElementStubLookup lookup ) {
		lookups.remove( lookup );
	}

	public void clearLookups() {
		lookups.clear();
	}
}
