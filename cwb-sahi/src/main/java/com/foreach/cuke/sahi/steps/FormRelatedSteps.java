/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.steps;

import com.foreach.cuke.sahi.element.*;
import com.foreach.cuke.sahi.util.RichTextTinyMce;
import com.foreach.cuke.sahi.util.TextCursor;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public final class FormRelatedSteps extends AbstractSahiSteps
{
	private static final String OPTIONAL_ELEMENT_REGEX = "(?: (element|[\\w]+))?(?: \"([^\"]*)\")?(?: (\\d+))?";
	private static final String OPTIONAL_ELEMENT_REGEX_WITH_PARENT =
			OPTIONAL_ELEMENT_REGEX + " in " + StandardSahiSteps.ELEMENT_REGEX;

	@Autowired
	private RichTextTinyMce tinymce;

	@When("^i fill in" + OPTIONAL_ELEMENT_REGEX_WITH_PARENT + " with \"([^\"]*)\"$")
	public void i_fill_in_with_parent_with( String type,
	                                        String locator,
	                                        Integer position,
	                                        String parentType,
	                                        String parentLocator,
	                                        Integer parentPosition,
	                                        String value ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? TextElementHandler.TEXT : type;

		ElementDescriptor descriptor = new ElementDescriptor( locator, position, actualType, value );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.set( descriptor );
	}

	@When("^i fill in" + OPTIONAL_ELEMENT_REGEX + " with \"([^\"]*)\"$")
	public void i_fill_in_with( String type, String locator, Integer position, String value ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? TextElementHandler.TEXT : type;
		elementHandleService.set( new ElementDescriptor( locator, position, actualType, value ) );
	}

	@When("^i fill in" + OPTIONAL_ELEMENT_REGEX + " with$")
	public void i_fill_in_with_docstring( String type,
	                                      String locator,
	                                      Integer position,
	                                      String value ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? TextElementHandler.TEXT : type;
		elementHandleService.set( new ElementDescriptor( locator, position, actualType, value ) );
	}

	@Then("^the \"([^\"]*)\" field in " + StandardSahiSteps.ELEMENT_REGEX + " (?:should contain|contains) \"([^\"]*)\"$")
	public void the_field_in_parent_should_contain( String field,
	                                                String parentType,
	                                                String parentLocator,
	                                                Integer parentPosition,
	                                                String value ) throws Throwable {
		ElementDescriptor descriptor =
				new ElementDescriptor( field, TextElementHandler.TEXT, value, VerifyOperation.CONTAINS );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.verify( descriptor, true );
	}

	@Then("^the \"([^\"]*)\" field (?:should contain|contains) \"([^\"]*)\"$")
	public void the_field_should_contain( String field, String value ) throws Throwable {
		elementHandleService.verify(
				new ElementDescriptor( field, TextElementHandler.TEXT, value, VerifyOperation.CONTAINS ), true );
	}

	@And("^i choose" + OPTIONAL_ELEMENT_REGEX_WITH_PARENT + "$")
	public void i_choose_in_parent( String type,
	                                String locator,
	                                Integer position,
	                                String parentType,
	                                String parentLocator,
	                                Integer parentPosition ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? RadioElementHandler.TYPE : type;
		ElementDescriptor descriptor =
				new ElementDescriptor( locator, position, actualType, RadioElementHandler.CHOOSE );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.set( descriptor );
	}

	@And("^i choose" + OPTIONAL_ELEMENT_REGEX + "$")
	public void i_choose( String type, String locator, Integer position ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? RadioElementHandler.TYPE : type;
		elementHandleService.set( new ElementDescriptor( locator, position, actualType, RadioElementHandler.CHOOSE ) );
	}

	@And("^" + "(?:(element|[\\w]+) )?(?:\"([^\"]*)\" )?(?:(\\d+) )?" + "(?:should be|is) chosen in " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void should_be_chosen_in_parent( String type,
	                                        String locator,
	                                        Integer position,
	                                        String parentType,
	                                        String parentLocator,
	                                        Integer parentPosition ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? RadioElementHandler.TYPE : type;
		ElementDescriptor descriptor = new ElementDescriptor( locator, position, actualType, RadioElementHandler.CHOSEN,
		                                                      VerifyOperation.EQUALS );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.verify( descriptor, true );
	}

	@And("^" + "(?:(element|[\\w]+) )?(?:\"([^\"]*)\" )?(?:(\\d+) )?" + "(?:should be|is) chosen$")
	public void should_be_chosen( String type, String locator, Integer position ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? RadioElementHandler.TYPE : type;
		elementHandleService.verify( new ElementDescriptor( locator, position, actualType, RadioElementHandler.CHOSEN,
		                                                    VerifyOperation.EQUALS ), true );
	}

	@When("^i select \"([^\"]*)\" from" + OPTIONAL_ELEMENT_REGEX_WITH_PARENT + "$")
	public void i_select_from_in_parent( String option,
	                                     String type,
	                                     String locator,
	                                     Integer position,
	                                     String parentType,
	                                     String parentLocator,
	                                     Integer parentPosition ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? SelectElementHandler.TYPE : type;

		ElementDescriptor descriptor = new ElementDescriptor( locator, position, actualType, option );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.set( descriptor );
	}

	@When("^i select \"([^\"]*)\" from" + OPTIONAL_ELEMENT_REGEX + "$")
	public void i_select_from( String option, String type, String locator, Integer position ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? SelectElementHandler.TYPE : type;
		elementHandleService.set( new ElementDescriptor( locator, position, actualType, option ) );
	}

	@And("^\"([^\"]*)\" (?:should be|is) selected in" + OPTIONAL_ELEMENT_REGEX_WITH_PARENT + "$")
	public void should_be_selected_in_parent( String value,
	                                          String type,
	                                          String locator,
	                                          Integer position,
	                                          String parentType,
	                                          String parentLocator,
	                                          Integer parentPosition ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? SelectElementHandler.TYPE : type;
		ElementDescriptor descriptor =
				new ElementDescriptor( locator, position, actualType, value, VerifyOperation.EQUALS );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.verify( descriptor, true );
	}

	@And("^\"([^\"]*)\" (?:should be|is) selected in" + OPTIONAL_ELEMENT_REGEX + "$")
	public void should_be_selected_in( String value, String type, String locator, Integer position ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? SelectElementHandler.TYPE : type;
		elementHandleService.verify(
				new ElementDescriptor( locator, position, actualType, value, VerifyOperation.EQUALS ), true );
	}

	@And("^i check" + OPTIONAL_ELEMENT_REGEX_WITH_PARENT + "$")
	public void i_check_in_parent( String type,
	                               String locator,
	                               Integer position,
	                               String parentType,
	                               String parentLocator,
	                               Integer parentPosition ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? CheckboxElementHandler.TYPE : type;
		ElementDescriptor descriptor =
				new ElementDescriptor( locator, position, actualType, CheckboxElementHandler.CHECKED );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.set( descriptor );
	}

	@And("^i check" + OPTIONAL_ELEMENT_REGEX + "$")
	public void i_check( String type, String locator, Integer position ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? CheckboxElementHandler.TYPE : type;
		elementHandleService.set(
				new ElementDescriptor( locator, position, actualType, CheckboxElementHandler.CHECKED ) );
	}

	@And("^" + "(?:(element|[\\w]+) )?(?:\"([^\"]*)\" )?(?:(\\d+) )?" + "(?:should be|is) checked in " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void should_be_checked_in_parent( String type,
	                                         String locator,
	                                         Integer position,
	                                         String parentType,
	                                         String parentLocator,
	                                         Integer parentPosition ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? CheckboxElementHandler.TYPE : type;

		ElementDescriptor descriptor =
				new ElementDescriptor( locator, position, actualType, CheckboxElementHandler.CHECKED,
				                       VerifyOperation.EQUALS );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.verify( descriptor, true );
	}

	@And("^" + "(?:(element|[\\w]+) )?(?:\"([^\"]*)\" )?(?:(\\d+) )?" + "(?:should be|is) checked$")
	public void should_be_checked( String type, String locator, Integer position ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? CheckboxElementHandler.TYPE : type;
		elementHandleService.verify(
				new ElementDescriptor( locator, position, actualType, CheckboxElementHandler.CHECKED,
				                       VerifyOperation.EQUALS ), true
		);
	}

	@And("^i uncheck" + OPTIONAL_ELEMENT_REGEX_WITH_PARENT + "$")
	public void i_uncheck_in_parent( String type,
	                                 String locator,
	                                 Integer position,
	                                 String parentType,
	                                 String parentLocator,
	                                 Integer parentPosition ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? CheckboxElementHandler.TYPE : type;

		ElementDescriptor descriptor =
				new ElementDescriptor( locator, position, actualType, CheckboxElementHandler.UNCHECKED );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.set( descriptor );
	}

	@And("^i uncheck" + OPTIONAL_ELEMENT_REGEX + "$")
	public void i_uncheck( String type, String locator, Integer position ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? CheckboxElementHandler.TYPE : type;
		elementHandleService.set(
				new ElementDescriptor( locator, position, actualType, CheckboxElementHandler.UNCHECKED ) );
	}

	@And("^" + "(?:(element|[\\w]+) )?(?:\"([^\"]*)\" )?(?:(\\d+) )?" + "(?:should be|is) unchecked in " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void should_be_unchecked_in_parent( String type,
	                                           String locator,
	                                           Integer position,
	                                           String parentType,
	                                           String parentLocator,
	                                           Integer parentPosition ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? CheckboxElementHandler.TYPE : type;

		ElementDescriptor descriptor =
				new ElementDescriptor( locator, position, actualType, CheckboxElementHandler.UNCHECKED,
				                       VerifyOperation.EQUALS );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.verify( descriptor, true );
	}

	@And("^" + "(?:(element|[\\w]+) )?(?:\"([^\"]*)\" )?(?:(\\d+) )?" + "(?:should be|is) unchecked$")
	public void should_be_unchecked( String type, String locator, Integer position ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? CheckboxElementHandler.TYPE : type;
		elementHandleService.verify(
				new ElementDescriptor( locator, position, actualType, CheckboxElementHandler.UNCHECKED,
				                       VerifyOperation.EQUALS ), true
		);
	}

	@And("^i press" + OPTIONAL_ELEMENT_REGEX_WITH_PARENT + "$")
	public void i_press_in_parent( String type,
	                               String locator,
	                               Integer position,
	                               String parentType,
	                               String parentLocator,
	                               Integer parentPosition ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? ButtonElementHandler.TYPE : type;

		find( actualType, locator, position, find( parentType, parentLocator, parentPosition ) ).click();
	}

	@And("^i press" + OPTIONAL_ELEMENT_REGEX + "$")
	public void i_press( String type, String locator, Integer position ) throws Throwable {
		String actualType = StringUtils.isBlank( type ) ? ButtonElementHandler.TYPE : type;

		find( actualType, locator, position ).click();
	}

	@When("^i fill in the following:$")
	public void i_fill_in_the_following( List<ElementDescriptor> entries ) throws Throwable {
		for ( ElementDescriptor formElement : entries ) {
			elementHandleService.set( formElement );
		}
	}

	@When("^i fill in the following in " + StandardSahiSteps.ELEMENT_REGEX + ":$")
	public void i_fill_in_the_following_in_parent( String parentType,
	                                               String parentLocator,
	                                               Integer parentPosition,
	                                               List<ElementDescriptor> entries ) throws Throwable {
		ElementStub parent = find( parentType, parentLocator, parentPosition );
		for ( ElementDescriptor formElement : entries ) {
			formElement.setParent( parent );
			elementHandleService.set( formElement );
		}
	}

	@Given("^(?:i )?(?:put the )?focus on " + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + "$")
	public void i_focus_on_with_parent( String type,
	                                    String locator,
	                                    Integer position,
	                                    String parentType,
	                                    String parentLocator,
	                                    Integer parentPosition ) throws Throwable {

		find( type, locator, position, find( parentType, parentLocator, parentPosition ) ).focus();
	}

	@Given("^(?:i )?(?:put the )?focus on " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_focus_on( String type, String locator, Integer position ) throws Throwable {
		find( type, locator, position ).focus();
	}

	@Given("^(?:i )?remove (?:the )?focus from " + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + "$")
	public void i_remove_focus_from_with_parent( String type,
	                                             String locator,
	                                             Integer position,
	                                             String parentType,
	                                             String parentLocator,
	                                             Integer parentPosition ) throws Throwable {
		find( type, locator, position, find( parentType, parentLocator, parentPosition ) ).removeFocus();
	}

	@Given("^(?:i )?remove (?:the )?focus from " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_remove_focus_from( String type, String locator, Integer position ) throws Throwable {
		find( type, locator, position ).removeFocus();
	}

	@When("^(?:i )?select text \"([^\"]*)\" in " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_select_text_in_tinymce( String text,
	                                      String type,
	                                      String locator,
	                                      Integer position ) throws Throwable {
		String tinyMceId = find( type, locator, position ).getAttribute( "id" );
		tinymce.selectText( tinyMceId, text );
	}

	@And("^(?:i )?insert text \"([^\"]*)\" in " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_insert_text_in_tinymce( String text,
	                                      String type,
	                                      String locator,
	                                      Integer position ) throws Throwable {
		String tinyMceId = find( type, locator, position ).getAttribute( "id" );
		tinymce.insertText( tinyMceId, text );
	}

	@When("(?:i )?replace text \"([^\"]*)\" with \"([^\"]*)\" in " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_replace_text_with_in_tinymce( String text,
	                                            String replacement,
	                                            String type,
	                                            String locator,
	                                            Integer position ) throws Throwable {
		String tinyMceId = find( type, locator, position ).getAttribute( "id" );
		tinymce.replaceText( tinyMceId, text, replacement );
	}

	@When("(?:i )?(?:delete|remove) text \"([^\"]*)\" (?:in|from) " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_delete_text_from_tinymce( String text,
	                                        String type,
	                                        String locator,
	                                        Integer position ) throws Throwable {
		String tinyMceId = find( type, locator, position ).getAttribute( "id" );
		tinymce.removeText( tinyMceId, text );
	}

	@When("^(?:i )?put the cursor (before|after) text \"([^\"]*)\" in " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_put_the_cursor_before_text_in_tinymce( String relativeLocation,
	                                                     String text,
	                                                     String type,
	                                                     String locator,
	                                                     Integer position ) throws Throwable {
		String tinyMceId = find( type, locator, position ).getAttribute( "id" );
		tinymce.putTextCursor( tinyMceId,
		                       StringUtils.equals( "before", relativeLocation ) ? TextCursor.BEFORE : TextCursor.AFTER,
		                       text );
	}

	@When("^(?:i )?put the cursor (before|after) the (text|html|selection|selected text) in " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_put_the_cursor_before_the_selection_in_tinymce( String relativeLocation,
	                                                              String offset,
	                                                              String type,
	                                                              String locator,
	                                                              Integer position ) throws Throwable {
		String tinyMceId = find( type, locator, position ).getAttribute( "id" );

		TextCursor dir = StringUtils.equals( "before", relativeLocation ) ? TextCursor.BEFORE : TextCursor.AFTER;

		if ( StringUtils.equals( offset, "text" ) ) {
			if ( dir == TextCursor.BEFORE ) {
				tinymce.selectFirstNode( tinyMceId, true );
			}
			else {
				tinymce.selectLastNode( tinyMceId, true );
			}
		}
		else if ( StringUtils.equals( offset, "html" ) ) {
			if ( dir == TextCursor.BEFORE ) {
				tinymce.selectFirstNode( tinyMceId, false );
			}
			else {
				tinymce.selectLastNode( tinyMceId, false );
			}
		}

		tinymce.putTextCursor( tinyMceId, dir );
	}

	@When("^(?:i )?select the contents of the html node with text \"([^\"]*)\" in " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_select_the_contents_of_the_html_node_with_text_in_tinymce( String text,
	                                                                         String type,
	                                                                         String locator,
	                                                                         Integer position ) throws Throwable {
		String tinyMceId = find( type, locator, position ).getAttribute( "id" );
		tinymce.selectNodeContentsWithText( tinyMceId, text );
	}

	@When("^(?:i )?select the html node with text \"([^\"]*)\" in " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_select_the_html_node_with_text_in_tinymce( String text,
	                                                         String type,
	                                                         String locator,
	                                                         Integer position ) throws Throwable {
		String tinyMceId = find( type, locator, position ).getAttribute( "id" );
		tinymce.selectNodeWithText( tinyMceId, text );
	}

	@And("^(?:i )?delete the (?:selected text|selection) (?:in|from) " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_delete_the_selected_text_in_tinymce( String type,
	                                                   String locator,
	                                                   Integer position ) throws Throwable {
		String tinyMceId = find( type, locator, position ).getAttribute( "id" );
		tinymce.replaceTextSelection( tinyMceId, "" );
	}

}
