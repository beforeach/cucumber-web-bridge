/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.element;

import com.foreach.cuke.sahi.util.CwbElementStub;
import com.foreach.cuke.sahi.util.RichTextTinyMce;
import com.foreach.cuke.sahi.util.TinyMceElementStub;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

@DefaultElementHandler
@Component
public class TinyMceElementHandler extends ElementHandler
{
	public static final String TINYMCE = "tinymce";
	public static final String HTML_LOOKUP_PREFIX = "html:";

	private static final String HTML_TAG_PATTERN = "^<([^>]+)>.*";

	@Autowired
	private RichTextTinyMce tinyMce;

	@Override
	public boolean canHandle( ElementDescriptor descriptor ) {
		return StringUtils.equalsIgnoreCase( TINYMCE, descriptor.getType() );
	}

	@Override
	public ElementStub find( ElementDescriptor descriptor ) {
		return new TinyMceElementStub( tinyMce,
		                               addParents(
				                               new ElementStub( CwbElementStub.TINYMCE, browser,
				                                                descriptor.getSahiArguments() ),
				                               descriptor
		                               )
		);
	}

	@Override
	public void set( ElementDescriptor descriptor ) {
		ElementStub element = find( descriptor );
		String tinyMceId = element.getAttribute( "id" );

		boolean isHtml = false;
		String value = descriptor.getValue();

		if ( StringUtils.startsWith( value, HTML_LOOKUP_PREFIX ) ) {
			value = StringUtils.substring( value, HTML_LOOKUP_PREFIX.length() );
			isHtml = true;
		}
		else {
			if ( Pattern.matches( HTML_TAG_PATTERN, StringUtils.trim( value ) ) ) {
				isHtml = true;
			}
		}

		if ( isHtml ) {
			tinyMce.setHTML( tinyMceId, value );
		}
		else {
			tinyMce.setPlainText( tinyMceId, value );
		}
	}

	@Override
	public void verify( ElementDescriptor descriptor, boolean expectedOutcome ) {
		ElementStub element = find( descriptor );
		String tinyMceId = element.getAttribute( "id" );

		String text;
		String value = descriptor.getValue();

		if ( StringUtils.startsWith( value, HTML_LOOKUP_PREFIX ) ) {
			// Check if the html should be ignored
			value = StringUtils.substring( value, HTML_LOOKUP_PREFIX.length() );
			text = tinyMce.getHTML( tinyMceId );
		}
		else {
			text = tinyMce.getPlainText( tinyMceId );
		}

		assertEquals( expectedOutcome, executeOperation( text, value, descriptor.getOperation() ) );
	}
}
