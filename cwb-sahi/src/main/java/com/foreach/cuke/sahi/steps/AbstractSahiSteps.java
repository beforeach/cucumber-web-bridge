/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.steps;

import com.foreach.cuke.core.steps.AbstractCoreSteps;
import com.foreach.cuke.sahi.browser.SahiBrowser;
import com.foreach.cuke.sahi.element.ElementDescriptor;
import com.foreach.cuke.sahi.element.ElementHandleService;
import net.sf.sahi.client.ElementStub;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractSahiSteps extends AbstractCoreSteps
{
	@Autowired
	protected SahiBrowser browser;

	@Autowired
	protected ElementHandleService elementHandleService;

	/**
	 * Replaces SPEL parameters in the expression specified.
	 */
	protected String parse( String expression ) {
		return spel.getValueAsString( expression );
	}

	protected boolean isVisible( ElementStub element ) {
		return element != null && element.isVisible();
	}

	protected boolean exists( ElementStub element ) {
		return element != null && element.exists();
	}

	protected ElementStub find( String type, String locator, Integer position ) {
		return find( type, locator, position, null );
	}

	protected ElementStub find( String type, String locator, Integer position, ElementStub parent ) {
		ElementDescriptor descriptor = new ElementDescriptor( locator, position, type );
		descriptor.setParent( parent );

		return elementHandleService.find( descriptor );
	}
}
