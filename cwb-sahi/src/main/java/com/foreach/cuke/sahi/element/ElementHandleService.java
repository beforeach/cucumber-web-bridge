/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.element;

import com.foreach.cuke.core.spel.SpelExpressionExecutor;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Service
public class ElementHandleService
{
	private static final Logger LOG = LoggerFactory.getLogger( ElementHandleService.class );

	@Autowired
	private List<ElementHandler> elementHandlers;

	@Autowired
	private SpelExpressionExecutor spel;

	@Value("${highlight:false}")
	private Boolean shouldHighlight;

	@PostConstruct
	public void sortElementHandlers() {
		Collections.sort( elementHandlers );

		LOG.info( "Default element highlighting is {}", shouldHighlight == Boolean.TRUE ? "ON" : "OFF" );
	}

	public ElementStub find( ElementDescriptor descriptor ) {
		ElementStub found;

		for ( ElementHandler handler : getHandlersForDescriptor( descriptor ) ) {
			found = handler.find( descriptor );

			if ( found != null ) {
				highlight( found );
				return found;
			}
		}

		return null;
	}

	private void highlight( ElementStub element ) {
		// Will highlight every selected element if specified - can slow things down a bit
		if ( shouldHighlight == Boolean.TRUE ) {
			try {
				if ( element.exists() ) {
					element.highlight();
				}
			}
			catch ( Exception e ) {
				// Highlighting has no impact
			}
		}
	}

	public void set( ElementDescriptor descriptor ) {
		ElementHandler handler = getHandlersForDescriptor( descriptor ).iterator().next();
		handler.set( descriptor );
	}

	public void verify( ElementDescriptor descriptor, boolean expectedOutcome ) {
		ElementHandler handler = getHandlersForDescriptor( descriptor ).iterator().next();
		handler.verify( descriptor, expectedOutcome );
	}

	private Collection<ElementHandler> getHandlersForDescriptor( ElementDescriptor descriptor ) {
		replaceSpelStatements( descriptor );

		List<ElementHandler> handlers = new LinkedList<ElementHandler>();

		for ( ElementHandler handler : elementHandlers ) {
			if ( handler.canHandle( descriptor ) ) {
				handlers.add( handler );
			}
		}

		if ( handlers.isEmpty() ) {
			throw new RuntimeException( "No element handler for " + descriptor );
		}

		return handlers;
	}

	private void replaceSpelStatements( ElementDescriptor descriptor ) {
		if ( descriptor.getLocator() != null ) {
			Object value = spel.getValue( descriptor.getLocator() );

			if ( value instanceof ElementStub ) {
				LOG.debug( "Not replacing locator because the value returned was an ElementStub: {}",
				           descriptor.getLocator() );
			}
			else {
				descriptor.setLocator( spel.getValueAsString( descriptor.getLocator() ) );
			}
		}
		descriptor.setType( spel.getValueAsString( descriptor.getType() ) );

		Object value = spel.getValue( descriptor.getValue() );
		descriptor.setValueAsType( value );
		descriptor.setValue( ObjectUtils.toString( value ) );
	}
}
