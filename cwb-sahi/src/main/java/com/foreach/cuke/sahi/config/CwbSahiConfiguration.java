/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.config;

import com.foreach.cuke.core.config.CwbCoreConfiguration;
import com.foreach.cuke.core.spel.SpelRootProperties;
import com.foreach.cuke.sahi.browser.SahiBrowser;
import com.foreach.cuke.sahi.browser.SahiBrowserDelegate;
import net.sf.sahi.util.BrowserType;
import net.sf.sahi.util.BrowserTypesLoader;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

@Configuration
@Import(CwbCoreConfiguration.class)
public class CwbSahiConfiguration
{
	@Component("sahiBrowserFactory")
	public static class SahiBrowserFactory
	{
		@Value("${browser}")
		private String browser;

		@Value("${sahi.baseDir}")
		private String sahiBase;

		@Value("${sahi.userData}")
		private String sahiUserData;

		@Value("${threadNo:}")
		private String threadNo;

		public SahiBrowser actualBrowser() {
			net.sf.sahi.config.Configuration.initJava( sahiBase, sahiUserData );
			System.out.println( "Sahi browser = " + browser );

			SahiBrowser b = new SahiBrowser( browser );
			if ( !StringUtils.isBlank( threadNo ) ) {
				BrowserType browserType = BrowserTypesLoader.getBrowserType( browser );
				b = new SahiBrowser( browserType.path(),
				                     browserType.processName(),
				                     browserType.options().replaceAll( "[$]threadNo", threadNo ) );
			}

			b.open();

			return b;
		}
	}

	@Autowired
	private SpelRootProperties rootProperties;

	@Bean(destroyMethod = "kill")
	@DependsOn("sahiBrowserFactory")
	public SahiBrowser browserDelegate( SahiBrowserFactory sahiBrowserFactory ) {
		SahiBrowser browser = new SahiBrowserDelegate( sahiBrowserFactory );

		// Make sure that browser can be used in spel
		rootProperties.add( "browser", browser );

		return browser;
	}

}
