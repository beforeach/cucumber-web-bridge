package com.foreach.cuke.sahi.config;

import com.foreach.cuke.sahi.browser.SahiBrowser;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Injects custom javascript into SAHI js.
 * Session-scoped javascript will be available on all webpages in the entire session. This is the default scope.
 * Scenario-scoped javascript is only available in the given scenario. It is loaded by tagging the SAHI scenario
 * with an appriopriate tagCallback (i.e. @IncludeCustomJs)
 */
@Component
public class CustomJavascriptInjector
{
	private static final Logger LOG = LoggerFactory.getLogger( CustomJavascriptInjector.class );

	public static final Scope DEFAULT_SCOPE = Scope.SESSION;

	private final Map<String, ScopedJavascript> cwbJavascriptResources = new LinkedHashMap<String, ScopedJavascript>();

	@Autowired
	private SahiBrowser browser;

	@Value("classpath:/com/foreach/cuke/sahi/util/tinymce.js")
	private Resource javascriptTinyMce;

	@Value("classpath:/com/foreach/cuke/sahi/util/cwb.js")
	private Resource javascriptCwb;

	@Value( "classpath:/com/foreach/cuke/sahi/util/domExport.js" )
	private Resource javascriptDomExporter;

	@PostConstruct
	private void registerDefaultJavascript() {
		register( "cwb", javascriptCwb );
		register( "domexport", javascriptDomExporter );
		register( "tinymce", javascriptTinyMce, CustomJavascriptInjector.Scope.SCENARIO );
	}

	public void inject( Set<String> scenarioKeys ) {
		StringBuilder customJs = new StringBuilder();
		if ( !cwbJavascriptResources.isEmpty() ) {
			for ( Map.Entry<String, ScopedJavascript> entry : cwbJavascriptResources.entrySet() ) {
				if ( scenarioKeys.contains( entry.getKey() ) || entry.getValue().scope == Scope.SESSION ) {
					customJs.append( entry.getValue().javascript );
				}
			}
		}
		browser.setBrowserJS( customJs.toString() );
	}

	public void register( String key, Resource javascriptResource ) {
		this.register( key, javascriptResource, DEFAULT_SCOPE );
	}

	public void register( String key, Resource javascriptResource, Scope scope ) {
		if ( javascriptResource.exists() ) {
			try {
				this.register( StringUtils.lowerCase( key ), IOUtils.toString( javascriptResource.getInputStream() ),
				               scope );
			}
			catch ( Exception e ) {
				LOG.error( "Ignoring custom javascript file " + key + " - reading resource throws exception" );
			}
		}
		else {
			LOG.debug( "Ignoring custom javascript file " + key + " - resource does not exist" );
		}
	}

	public void register( String key, String javascript ) {
		this.register( key, javascript, DEFAULT_SCOPE );
	}

	public void register( String key, String javascript, Scope scope ) {
		if( StringUtils.isNotBlank( key ) && StringUtils.isNotBlank( javascript ) && scope != null ) {
			String sanitizedKey = StringUtils.lowerCase( key );
			if ( !cwbJavascriptResources.containsKey( sanitizedKey ) || scope.getPriority() < cwbJavascriptResources.get( sanitizedKey ).scope.getPriority() ) {
				cwbJavascriptResources.put( sanitizedKey, new ScopedJavascript( scope, javascript ) );
			}
		}
	}

	public static enum Scope
	{
		SESSION, SCENARIO;

		private int getPriority(){
			return ordinal();
		}
	}

	private static class ScopedJavascript
	{
		private Scope scope;
		private String javascript;

		private ScopedJavascript( Scope scope, String javascript ) {
			this.scope = scope;
			this.javascript = javascript;
		}
	}

}