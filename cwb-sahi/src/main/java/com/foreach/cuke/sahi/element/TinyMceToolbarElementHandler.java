/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.element;

import com.foreach.cuke.sahi.util.CwbElementStub;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@DefaultElementHandler
@Component
public class TinyMceToolbarElementHandler extends ElementHandler
{
	public static final String TINYMCE_TOOLBAR = "tinymceToolbar";

	@Override
	public boolean canHandle( ElementDescriptor descriptor ) {
		return StringUtils.equalsIgnoreCase( TINYMCE_TOOLBAR, descriptor.getType() );
	}

	@Override
	public ElementStub find( ElementDescriptor descriptor ) {
		return addParents( new ElementStub( CwbElementStub.TINYMCE_TOOLBAR, browser, descriptor.getSahiArguments() ),
		                   descriptor );
		/*
		ElementStub element = addParents( browser.byId( descriptor.getSahiArguments() ), descriptor );

		if ( !element.exists() ) {
			element = elementStubLocator.find( descriptor.getSahiArguments(), addParents( descriptor.getParent() ) );
		}

		if ( element.exists() ) {
			return browser.byId( element.getAttribute( "id" ) + "_parent" );
		}

		return null;*/
	}

	@Override
	public void set( ElementDescriptor descriptor ) {
		throw new RuntimeException( "Set not possible on tinymce toolbar" );
	}

	@Override
	public void verify( ElementDescriptor descriptor, boolean expectedOutcome ) {
		throw new RuntimeException( "Verify not possible on tinymce toolbar" );
	}
}
