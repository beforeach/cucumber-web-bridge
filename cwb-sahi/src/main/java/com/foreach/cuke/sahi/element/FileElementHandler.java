/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.element;

import com.foreach.cuke.core.util.FileResource;
import com.foreach.cuke.core.util.FileResourceResolver;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.junit.Assert.fail;

@DefaultElementHandler
@Component
public class FileElementHandler extends ElementHandler
{
	public static final String TYPE_FILE = "file";
	public static final String TYPE_FILE2 = "file2";

	@Autowired
	private FileResourceResolver fileResourceResolver;

	@Override
	public boolean canHandle( ElementDescriptor descriptor ) {
		return StringUtils.startsWithIgnoreCase( descriptor.getType(), TYPE_FILE );
	}

	@Override
	public ElementStub find( ElementDescriptor descriptor ) {
		ElementStub stub = addParents( browser.file( descriptor.getSahiArguments() ), descriptor );

		if ( !stub.exists() && StringUtils.equalsIgnoreCase( descriptor.getType(), TYPE_FILE2 ) ) {
			// In case of file2 its possible the type has been changed to textbox
			return addParents( browser.textbox( descriptor.getSahiArguments() ), descriptor );
		}

		return stub;
	}

	@Override
	public void set( ElementDescriptor descriptor ) {
		String fileName = determineFile( descriptor.getValue() );

		ElementStub fileStub = find( descriptor );

		if ( !fileStub.exists() ) {
			fail( "Could not find file input: " + descriptor );
		}

		if ( StringUtils.equalsIgnoreCase( descriptor.getType(), TYPE_FILE ) ) {
			// Do a simple set
			LOG.debug( "Performing a simple file set on " + descriptor );
			fileStub.setFile( fileName );
		}
		else {
			// Try setting with javascript and changing the element type
			// This might work in situations where the javascript requires the form to not be empty
			LOG.debug( "Performing a javascript change type file set on " + descriptor );

			browser.setFile( fileStub, fileName );

			// If it is still a file type, we should change it
			if ( !StringUtils.contains( fileStub.toString(), "textbox" ) ) {
				if ( browser.isIE() ) {
					browser.execute(
							fileStub + ".outerHTML = " + fileStub + ".outerHTML.replace(/type=['\"]?file['\"]?/, \"type=text\")" );
				}
				else {
					browser.execute( fileStub + ".type = \"text\";" );
				}

				addParents( browser.textbox( descriptor.getSahiArguments() ), descriptor ).setValue( fileName );
			}
			else {
				fileStub.setValue( fileName );
			}
		}
	}

	private String determineFile( String fileToUpload ) {
		FileResource resource = fileResourceResolver.resolve( fileToUpload );
		return resource.getAbsolutePath();
	}

	@Override
	public void verify( ElementDescriptor descriptor, boolean expectedOutcome ) {
		throw new RuntimeException( "Verify impossible on " + descriptor );
	}
}
