package com.foreach.cuke.sahi.scenario;

import com.foreach.cuke.core.scenario.CompositeScenarioCallback;
import com.foreach.cuke.core.scenario.SystemPropertyScenarioCallback;
import com.foreach.cuke.core.scenario.TagScenarioCallback;
import com.foreach.cuke.core.spel.SpelEvaluationContext;
import com.foreach.cuke.sahi.browser.SahiBrowser;
import cucumber.api.Scenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ResetBrowserScenarioCallback extends CompositeScenarioCallback
{
	@Autowired
	private SahiBrowser browser;

	@Autowired
	private SpelEvaluationContext context;

	@PostConstruct
	private void init(){
		addCallback( new SystemPropertyScenarioCallback()
		{
			@Override
			protected String getPropertyKey() {
				return "browser.reset";
			}

			@Override
			public void after( Scenario scenario ) {
				resetBrowser();
			}

		} );

		addCallback( new TagScenarioCallback()
		{
			@Override
			protected SpelEvaluationContext getContext() {
				return context;
			}

			@Override
			protected String getTagName() {
				return "resetBrowser";
			}

			@Override
			protected void after( String argument ) {
				resetBrowser();
			}
		} );
	}

	private void resetBrowser(){
		browser.reset();
	}
}
