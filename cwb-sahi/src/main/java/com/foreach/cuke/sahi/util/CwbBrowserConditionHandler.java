package com.foreach.cuke.sahi.util;

import com.foreach.cuke.sahi.browser.SahiBrowser;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CwbBrowserConditionHandler
{

	@Autowired
	protected SahiBrowser browser;

	public void waitFor( int waitInterval, int timeout, CwbBrowserCondition condition ) {
		int total = 0;
		while ( total < timeout ) {
			browser.waitFor( waitInterval );
			total += waitInterval;
			if ( condition.test() ) {
				return;
			}
		}
		Assert.fail( "Browsercondition was not satisfied - timeout occurred" );
	}

}
