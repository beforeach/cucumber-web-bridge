/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.browser;

import net.sf.sahi.client.Browser;
import net.sf.sahi.client.ExecutionException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingBrowser extends Browser
{
	private static final Logger LOG = LoggerFactory.getLogger( SahiBrowser.class );

	private String last = "";
	private String lastStep = "";

	public LoggingBrowser( String browserName ) {
		super( browserName );
	}

	public LoggingBrowser( String browserPath, String browserProcessName, String browserOption ) {
		super( browserPath, browserProcessName, browserOption );
	}

	public LoggingBrowser( String browserPath,
	                       String browserProcessName,
	                       String browserOption,
	                       String host,
	                       int port ) {
		super( browserPath, browserProcessName, browserOption, host, port );
	}

	public LoggingBrowser( String browserName, String host, int port ) {
		super( browserName, host, port );
	}

	public LoggingBrowser() {
	}

	@Override
	public void execute( String step ) throws ExecutionException {
		if ( !StringUtils.contains( step, "setServerVarPlain" ) && !StringUtils.equals( last, step ) ) {
			LOG.debug( step );
			last = "execute " + step;
		}
		super.execute( step );
	}

	@Override
	public String fetch( String expression ) throws ExecutionException {
		String value = "[unknown]";

		try {
			value = super.fetch( expression );
		}
		catch ( ExecutionException ee ) {
			value = ee.getMessage();
		}
		finally {
			String cur = "fetch " + expression + "" + value;
			if ( !StringUtils.equals( last, cur ) ) {
				LOG.debug( "{}: {}", expression, value );
				last = cur;
			}
		}

		return value;
	}

	@Override
	public void executeStep( String step ) throws ExecutionException {
		if ( LOG.isTraceEnabled() ) {
			if ( !StringUtils.equals( lastStep, step ) ) {
				LOG.trace( "[TRACE] {}", step );
			}
			lastStep = step;
		}
		super.executeStep( step );
	}
}
