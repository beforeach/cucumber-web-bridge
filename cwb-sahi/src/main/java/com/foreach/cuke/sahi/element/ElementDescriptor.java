/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.element;

import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collection;

public class ElementDescriptor
{
	public static final Collection<String> FORM_ELEMENTS =
			Arrays.asList( "text", "textbox", "textarea", "password", "select", "checkbox", "radio" );

	private String locator, type, value;
	private Integer position;
	private VerifyOperation operation;
	private ElementStub parent;

	private Object valueAsType;

	public ElementDescriptor() {
	}

	public ElementDescriptor( int position, String type ) {
		this.position = position;
		this.type = type;
	}

	public ElementDescriptor( String locator, String type ) {
		this( locator, null, type, null, null );
	}

	public ElementDescriptor( String locator, Integer position, String type ) {
		this( locator, position, type, null, null );
	}

	public ElementDescriptor( String locator, Integer position, String type, String value ) {
		this( locator, position, type, value, null );
	}

	public ElementDescriptor( String locator, String type, String value ) {
		this( locator, null, type, value, null );
	}

	public ElementDescriptor( String locator, String type, String value, VerifyOperation operation ) {
		this( locator, null, type, value, operation );
	}

	public ElementDescriptor( String locator, Integer position, String type, String value, VerifyOperation operation ) {
		this.locator = locator;
		this.type = type;
		this.value = value;
		this.position = position;
		this.operation = operation;
	}

	public String getLocator() {
		return locator;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition( Integer position ) {
		this.position = position;
	}

	public Object[] getSahiArguments() {
		if ( locator != null ) {
			return position != null ? new Object[] { locator + "[" + ( position - 1 ) + "]" } : new Object[] {
					locator };
		}

		return position != null ? new Object[] { position - 1 } : new Object[0];
	}

	public void setLocator( String locator ) {
		this.locator = locator;
	}

	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue( String value ) {
		this.value = value;
	}

	public <T> T getValueAsType() {
		return (T) valueAsType;
	}

	public void setValueAsType( Object valueAsType ) {
		this.valueAsType = valueAsType;
	}

	public VerifyOperation getOperation() {
		return operation;
	}

	public void setOperation( VerifyOperation operation ) {
		this.operation = operation;
	}

	public boolean isFormElement() {
		return FORM_ELEMENTS.contains( StringUtils.lowerCase( type ) );
	}

	public ElementStub getParent() {
		return parent;
	}

	public void setParent( ElementStub parent ) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		return "Element{" +
				"locator='" + locator + '\'' +
				", type='" + type + '\'' +
				", value='" + value + '\'' +
				'}';
	}
}
