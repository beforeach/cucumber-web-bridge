/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.util;

import net.sf.sahi.client.ElementStub;
import net.sf.sahi.client.ExecutionException;

import java.util.List;

public class ProxyElementStub extends ElementStub
{
	protected ElementStub original;

	public ProxyElementStub( ElementStub original ) {
		super( null, null );
		this.original = original;
	}

	@Override
	public String toString() {
		return original.toString();
	}

	@Override
	public void click() throws ExecutionException {
		original.click();
	}

	@Override
	public void doubleClick() throws ExecutionException {
		original.doubleClick();
	}

	@Override
	public void rightClick() throws ExecutionException {
		original.rightClick();
	}

	@Override
	public void check() throws ExecutionException {
		original.check();
	}

	@Override
	public void uncheck() throws ExecutionException {
		original.uncheck();
	}

	@Override
	public void focus() throws ExecutionException {
		original.focus();
	}

	@Override
	public void removeFocus() throws ExecutionException {
		original.removeFocus();
	}

	@Override
	public void dragAndDropOn( ElementStub dropElement ) throws ExecutionException {
		original.dragAndDropOn( dropElement );
	}

	@Override
	public void dragAndDropOn( int x, int y ) throws ExecutionException {
		original.dragAndDropOn( x, y );
	}

	@Override
	public void mouseOver() throws ExecutionException {
		original.mouseOver();
	}

	@Override
	public void mouseDown() throws ExecutionException {
		original.mouseDown();
	}

	@Override
	public void mouseUp() throws ExecutionException {
		original.mouseUp();
	}

	@Override
	public void hover() throws ExecutionException {
		original.hover();
	}

	@Override
	public void setValue( String value ) throws ExecutionException {
		original.setValue( value );
	}

	@Override
	public void setFile( String value ) throws ExecutionException {
		original.setFile( value );
	}

	@Override
	public void choose( String value ) throws ExecutionException {
		original.choose( value );
	}

	@Override
	public void choose( String value, boolean append ) throws ExecutionException {
		original.choose( value, append );
	}

	@Override
	public void choose( String[] values ) throws ExecutionException {
		original.choose( values );
	}

	@Override
	public void choose( String[] values, boolean append ) throws ExecutionException {
		original.choose( values, append );
	}

	@Override
	public String text() throws ExecutionException {
		return original.text();
	}

	@Override
	public String getText() throws ExecutionException {
		return original.getText();
	}

	@Override
	public void rteWrite( String value ) throws ExecutionException {
		original.rteWrite( value );
	}

	@Override
	public String rteText() {
		return original.rteText();
	}

	@Override
	public String rteHTML() {
		return original.rteHTML();
	}

	@Override
	public String fetch() throws ExecutionException {
		return original.fetch();
	}

	@Override
	public boolean exists() {
		return original.exists();
	}

	@Override
	public boolean exists( boolean optimistic ) {
		return original.exists( optimistic );
	}

	@Override
	public String value() throws ExecutionException {
		return original.value();
	}

	@Override
	public String getValue() throws ExecutionException {
		return original.getValue();
	}

	@Override
	public String selectedText() throws ExecutionException {
		return original.selectedText();
	}

	@Override
	public boolean isVisible() throws ExecutionException {
		return original.isVisible();
	}

	@Override
	public boolean isVisible( boolean optimistic ) throws ExecutionException {
		return original.isVisible( optimistic );
	}

	@Override
	public String getSelectedText() throws ExecutionException {
		return original.getSelectedText();
	}

	@Override
	public boolean containsText( String text ) {
		return original.containsText( text );
	}

	@Override
	public boolean containsHTML( String html ) {
		return original.containsHTML( html );
	}

	@Override
	public String style( String html ) {
		return original.style( html );
	}

	@Override
	public ElementStub in( ElementStub inEl ) {
		return original.in( inEl );
	}

	@Override
	public ElementStub under( ElementStub underEl ) {
		return original.under( underEl );
	}

	@Override
	public ElementStub near( ElementStub nearEl ) {
		return original.near( nearEl );
	}

	@Override
	public ElementStub above( ElementStub aboveEl ) {
		return original.above( aboveEl );
	}

	@Override
	public ElementStub aboveOrUnder( ElementStub aboveOrUnderEl ) {
		return original.aboveOrUnder( aboveOrUnderEl );
	}

	@Override
	public ElementStub leftOf( ElementStub leftOfEl ) {
		return original.leftOf( leftOfEl );
	}

	@Override
	public ElementStub rightOf( ElementStub rightOfEl ) {
		return original.rightOf( rightOfEl );
	}

	@Override
	public ElementStub leftOrRightOf( ElementStub leftOrRightOfEl ) {
		return original.leftOrRightOf( leftOrRightOfEl );
	}

	@Override
	public String fetch( String string ) throws ExecutionException {
		return original.fetch( string );
	}

	@Override
	public boolean checked() throws ExecutionException {
		return original.checked();
	}

	@Override
	public ElementStub parentNode() {
		return original.parentNode();
	}

	@Override
	public ElementStub parentNode( String tagName ) {
		return original.parentNode( tagName );
	}

	@Override
	public ElementStub parentNode( String tagName, int occurrence ) {
		return original.parentNode( tagName, occurrence );
	}

	@Override
	public List<ElementStub> collectSimilar() {
		return original.collectSimilar();
	}

	@Override
	public int countSimilar() {
		return original.countSimilar();
	}

	@Override
	public void keyUp( int keyCode, int charCode ) {
		original.keyUp( keyCode, charCode );
	}

	@Override
	public String getAttribute( String attribute ) throws ExecutionException {
		return original.getAttribute( attribute );
	}

	@Override
	public void blur() throws ExecutionException {
		original.blur();
	}

	@Override
	public void keyDown( int keyCode, int charCode ) {
		original.keyDown( keyCode, charCode );
	}

	@Override
	public void keyPress( String keySequence, String combo ) {
		original.keyPress( keySequence, combo );
	}

	@Override
	public void highlight() throws ExecutionException {
		original.highlight();
	}

	@Override
	public boolean equals( Object obj ) {
		return original.equals( obj );
	}

	@Override
	public int hashCode() {
		return original.hashCode();
	}
}
