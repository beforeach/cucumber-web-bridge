/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.steps;

import com.foreach.cuke.sahi.element.ElementDescriptor;
import com.foreach.cuke.sahi.element.VerifyOperation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@Component
public final class CommonElementSteps extends AbstractSahiSteps
{
	@Given("^i am on \"([^\"]*)\"$")
	public void i_am_on( String url ) throws Throwable {
		String parsedUrl = parse( url );

		if ( StringUtils.isBlank( parsedUrl ) ) {
			LOG.error( "Resulting url is empty: {}", url );
			throw new RuntimeException( "Cannot navigate to blank url, possibly missing a property value: " + url );
		}
		else {
			LOG.debug( "Navigating browser to {}", parsedUrl );
			browser.navigateTo( parsedUrl, true );
		}
	}

	@Then("^i should be on \"([^\"]*)\"$")
	public void i_should_be_on( String url ) throws Throwable {
		assertEquals( spel.getValue( url ), browser.fetch( "window.location.href" ) );
	}

	@When("^(?:i )?go to \"([^\"]*)\"$")
	public void i_go_to( String url ) throws Throwable {
		i_am_on( url );
	}

	@And("^i double click on " + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + "$")
	public void i_double_click_on( String type,
	                               String locator,
	                               Integer position,
	                               String parentType,
	                               String parentLocator,
	                               Integer parentPosition ) throws Throwable {
		find( type, locator, position, find( parentType, parentLocator, parentPosition ) ).doubleClick();
	}

	@And("^i double click on " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_double_click( String type, String locator, Integer position ) throws Throwable {
		find( type, locator, position ).doubleClick();
	}

	@And("^i right click on " + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + "$")
	public void i_right_click_on( String type,
	                              String locator,
	                              Integer position,
	                              String parentType,
	                              String parentLocator,
	                              Integer parentPosition ) throws Throwable {
		find( type, locator, position, find( parentType, parentLocator, parentPosition ) ).rightClick();
	}

	@And("^i right click on " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_right_click( String type, String locator, Integer position ) throws Throwable {
		find( type, locator, position ).rightClick();
	}

	@And("^i click on " + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + "$")
	public void i_click_on( String type,
	                        String locator,
	                        Integer position,
	                        String parentType,
	                        String parentLocator,
	                        Integer parentPosition ) throws Throwable {
		find( type, locator, position, find( parentType, parentLocator, parentPosition ) ).click();
	}

	@And("^i click on " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_click_on( String type, String locator, Integer position ) throws Throwable {
		find( type, locator, position ).click();
	}

	@When("^i follow \"([^\"]*)\"$")
	public void i_follow( String link ) throws Throwable {
		i_click_on( "link", link, null );
	}

	@And("^i open \"([^\"]*)\" in (?:popup|window) \"([^\"]*)\"$")
	public void i_open_in_popup( String link, String popupName ) throws Throwable {
		ElementStub linkEl = find( "link", link, null );
		browser.setAttribute( linkEl, "target", popupName );
		linkEl.click();
	}

	@And("^i (?:should )?see " + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + "$")
	public void i_should_see_specific_parent( String type,
	                                          String locator,
	                                          Integer position,
	                                          String parentType,
	                                          String parentLocator,
	                                          Integer parentPosition ) throws Throwable {
		element_in_parent_element_is_visible( type, locator, position, parentType, parentLocator, parentPosition );
	}

	@And("^i (?:should )?see " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_should_see_specific( String type, String locator, Integer position ) throws Throwable {
		assertTrue( isVisible( find( type, locator, position ) ) );
	}

	@And("^i (?:should|do) not see " + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + "$")
	public void i_should_not_see_specific_parent( String type,
	                                              String locator,
	                                              Integer position,
	                                              String parentType,
	                                              String parentLocator,
	                                              Integer parentPosition ) throws Throwable {
		element_in_parent_element_is_not_visible( type, locator, position, parentType, parentLocator, parentPosition );
	}

	@And("^i (?:should|do) not see " + StandardSahiSteps.ELEMENT_REGEX + "$")
	public void i_should_not_see_specific( String type, String locator, Integer position ) throws Throwable {
		ElementStub stub = find( type, locator, position );
		assertFalse( isVisible( stub ) );
	}

	@When("^the following (?:elements|fields) in " + StandardSahiSteps.ELEMENT_REGEX + " (?:should exist|exist):$")
	public void the_following_elements_in_parent_exist( String parentType,
	                                                    String parentLocator,
	                                                    Integer parentPosition,
	                                                    List<ElementDescriptor> descriptors ) throws Throwable {
		ElementStub parent = find( parentType, parentLocator, parentPosition );
		for ( ElementDescriptor descriptor : descriptors ) {
			descriptor.setParent( parent );
			assertTrue( "Does not exist: " + descriptor.toString(), exists( elementHandleService.find( descriptor ) ) );
		}
	}

	@When("^the following (?:elements|fields) in " + StandardSahiSteps.ELEMENT_REGEX + " (?:are|should be) visible:$")
	public void the_following_elements_in_parent_are_visible( String parentType,
	                                                          String parentLocator,
	                                                          Integer parentPosition,
	                                                          List<ElementDescriptor> descriptors ) throws Throwable {
		ElementStub parent = find( parentType, parentLocator, parentPosition );
		for ( ElementDescriptor descriptor : descriptors ) {
			descriptor.setParent( parent );
			assertTrue( "Not visible: " + descriptor.toString(), isVisible( elementHandleService.find( descriptor ) ) );
		}
	}

	@When("^the following (?:elements|fields) (?:should exist|exist):$")
	public void the_following_elements_exist( List<ElementDescriptor> descriptors ) throws Throwable {
		for ( ElementDescriptor descriptor : descriptors ) {
			assertTrue( "Does not exist: " + descriptor.toString(), exists( elementHandleService.find( descriptor ) ) );
		}
	}

	@When("^the following (?:elements|fields) (?:are|should be) visible:$")
	public void the_following_elements_are_visible( List<ElementDescriptor> descriptors ) throws Throwable {
		for ( ElementDescriptor descriptor : descriptors ) {
			assertTrue( "Not visible: " + descriptor.toString(), isVisible( elementHandleService.find( descriptor ) ) );
		}
	}

	@When("^the following (?:elements|fields) in " + StandardSahiSteps.ELEMENT_REGEX + " (?:do not|should not) exist:$")
	public void the_following_elements_in_parent_do_not_exist( String parentType,
	                                                           String parentLocator,
	                                                           Integer parentPosition,
	                                                           List<ElementDescriptor> descriptors ) throws Throwable {
		ElementStub parent = find( parentType, parentLocator, parentPosition );
		if ( exists( parent ) ) {
			for ( ElementDescriptor descriptor : descriptors ) {
				descriptor.setParent( parent );
				assertFalse( "Exists: " + descriptor.toString(), exists( elementHandleService.find( descriptor ) ) );
			}
		}
	}

	@When("^the following (?:elements|fields) in " + StandardSahiSteps.ELEMENT_REGEX + " (?:are not|should not be) visible:$")
	public void the_following_elements_in_parent_are_not_visible( String parentType,
	                                                              String parentLocator,
	                                                              Integer parentPosition,
	                                                              List<ElementDescriptor> descriptors ) throws Throwable {
		ElementStub parent = find( parentType, parentLocator, parentPosition );
		if ( parent != null && parent.exists() ) {
			for ( ElementDescriptor descriptor : descriptors ) {
				descriptor.setParent( parent );
				assertFalse( "Visible: " + descriptor.toString(),
				             isVisible( elementHandleService.find( descriptor ) ) );
			}
		}
	}

	@When("^the following (?:elements|fields) (?:do not|should not) exist:$")
	public void the_following_elements_do_not_exist( List<ElementDescriptor> descriptors ) throws Throwable {
		for ( ElementDescriptor descriptor : descriptors ) {
			assertFalse( "Exists: " + descriptor.toString(), exists( elementHandleService.find( descriptor ) ) );
		}
	}

	@When("^the following (?:elements|fields) (?:are not|should not be) visible:$")
	public void the_following_elements_are_not_visible( List<ElementDescriptor> descriptors ) throws Throwable {
		for ( ElementDescriptor descriptor : descriptors ) {
			assertFalse( "Visible: " + descriptor.toString(), isVisible( elementHandleService.find( descriptor ) ) );
		}
	}

	// Contains

	@Then("^(?:ensure that )?^(?:the )?" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should not contain|does not contain) \"([^\"]*)\"$")
	public void the_element_in_parent_should_not_contain( String type,
	                                                      String locator,
	                                                      Integer position,
	                                                      String parentType,
	                                                      String parentLocator,
	                                                      Integer parentPosition,
	                                                      String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ),
		        find( parentType, parentLocator, parentPosition ), VerifyOperation.CONTAINS, false );
	}

	@Then("^(?:ensure that )?^(?:the )?" + StandardSahiSteps.ELEMENT_REGEX + " (?:should not contain|does not contain) \"([^\"]*)\"$")
	public void the_element_should_not_contain( String type,
	                                            String locator,
	                                            Integer position,
	                                            String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ), null,
		        VerifyOperation.CONTAINS, false );
	}

	@Then("^(?:ensure that )?^(?:the )?" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should contain|contains) \"([^\"]*)\"$")
	public void the_element_in_parent_should_contain( String type,
	                                                  String locator,
	                                                  Integer position,
	                                                  String parentType,
	                                                  String parentLocator,
	                                                  Integer parentPosition,
	                                                  String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ),
		        find( parentType, parentLocator, parentPosition ), VerifyOperation.CONTAINS, true );
	}

	@Then("^(?:ensure that )?^(?:the )?" + StandardSahiSteps.ELEMENT_REGEX + " (?:should contain|contains) \"([^\"]*)\"$")
	public void the_element_should_contain( String type,
	                                        String locator,
	                                        Integer position,
	                                        String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ), null,
		        VerifyOperation.CONTAINS, true );
	}

	@Then("^the (?:field|element) values in " + StandardSahiSteps.ELEMENT_REGEX + " (?:should contain|contain):$")
	public void the_field_values_in_parent_should_contain( String parentType,
	                                                       String parentLocator,
	                                                       Integer parentPosition,
	                                                       List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, find( parentType, parentLocator, parentPosition ), VerifyOperation.CONTAINS, true );
	}

	@Then("^the (?:field|element) values (?:should contain|contain):$")
	public void the_field_values_should_contain( List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, null, VerifyOperation.CONTAINS, true );
	}

	@Then("^the (?:field|element) values in " + StandardSahiSteps.ELEMENT_REGEX + " (?:should not contain|do not contain):$")
	public void the_field_values_in_parent_should_not_contain( String parentType,
	                                                           String parentLocator,
	                                                           Integer parentPosition,
	                                                           List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, find( parentType, parentLocator, parentPosition ), VerifyOperation.CONTAINS, false );
	}

	@Then("^the (?:field|element) values (?:should not contain|do not contain):$")
	public void the_field_values_should_not_contain( List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, null, VerifyOperation.CONTAINS, false );
	}

	// Starts with

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should not start with|does not start with) \"([^\"]*)\"$")
	public void the_element_in_parent_should_not_start_with( String type,
	                                                         String locator,
	                                                         Integer position,
	                                                         String parentType,
	                                                         String parentLocator,
	                                                         Integer parentPosition,
	                                                         String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ),
		        find( parentType, parentLocator, parentPosition ), VerifyOperation.STARTS_WITH, false );
	}

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_REGEX + " (?:should not start with|does not start with) \"([^\"]*)\"$")
	public void the_element_should_not_start_with( String type,
	                                               String locator,
	                                               Integer position,
	                                               String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ), null,
		        VerifyOperation.STARTS_WITH, false );
	}

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should start with|starts with) \"([^\"]*)\"$")
	public void the_element_in_parent_should_start_with( String type,
	                                                     String locator,
	                                                     Integer position,
	                                                     String parentType,
	                                                     String parentLocator,
	                                                     Integer parentPosition,
	                                                     String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ),
		        find( parentType, parentLocator, parentPosition ), VerifyOperation.STARTS_WITH, true );
	}

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_REGEX + " (?:should start with|starts with) \"([^\"]*)\"$")
	public void the_element_should_start_with( String type,
	                                           String locator,
	                                           Integer position,
	                                           String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ), null,
		        VerifyOperation.STARTS_WITH, true );
	}

	@Then("^the (?:field|element) values in " + StandardSahiSteps.ELEMENT_REGEX + " (?:should start with|start with):$")
	public void the_field_values_in_parent_should_start_with( String parentType,
	                                                          String parentLocator,
	                                                          Integer parentPosition,
	                                                          List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, find( parentType, parentLocator, parentPosition ), VerifyOperation.STARTS_WITH, true );
	}

	@Then("^the (?:field|element) values (?:should start with|start with):$")
	public void the_field_values_should_start_with( List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, null, VerifyOperation.STARTS_WITH, true );
	}

	@Then("^the (?:field|element) values in " + StandardSahiSteps.ELEMENT_REGEX + " (?:should not start with|do not start with):$")
	public void the_field_values_in_parent_should_not_start_with( String parentType,
	                                                              String parentLocator,
	                                                              Integer parentPosition,
	                                                              List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, find( parentType, parentLocator, parentPosition ), VerifyOperation.STARTS_WITH, false );
	}

	@Then("^the (?:field|element) values (?:should not start with|do not start with):$")
	public void the_field_values_should_not_start_with( List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, null, VerifyOperation.STARTS_WITH, false );
	}

	// Ends with

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should not end with|does not end with) \"([^\"]*)\"$")
	public void the_element_in_parent_should_not_end_with( String type,
	                                                       String locator,
	                                                       Integer position,
	                                                       String parentType,
	                                                       String parentLocator,
	                                                       Integer parentPosition,
	                                                       String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ),
		        find( parentType, parentLocator, parentPosition ), VerifyOperation.ENDS_WITH, false );
	}

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_REGEX + " (?:should not end with|does not end with) \"([^\"]*)\"$")
	public void the_element_should_not_end_with( String type,
	                                             String locator,
	                                             Integer position,
	                                             String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ), null,
		        VerifyOperation.ENDS_WITH, false );
	}

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should end with|ends with) \"([^\"]*)\"$")
	public void the_element_in_parent_should_end_with( String type,
	                                                   String locator,
	                                                   Integer position,
	                                                   String parentType,
	                                                   String parentLocator,
	                                                   Integer parentPosition,
	                                                   String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ),
		        find( parentType, parentLocator, parentPosition ), VerifyOperation.ENDS_WITH, true );
	}

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_REGEX + " (?:should end with|ends with) \"([^\"]*)\"$")
	public void the_element_should_end_with( String type,
	                                         String locator,
	                                         Integer position,
	                                         String value ) throws Throwable {
		verify( Arrays.asList( new ElementDescriptor( locator, position, type, value ) ), null,
		        VerifyOperation.ENDS_WITH, true );
	}

	@Then("^the (?:field|element) values in " + StandardSahiSteps.ELEMENT_REGEX + " (?:should end with|end with):$")
	public void the_field_values_in_parent_should_end_with( String parentType,
	                                                        String parentLocator,
	                                                        Integer parentPosition,
	                                                        List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, find( parentType, parentLocator, parentPosition ), VerifyOperation.ENDS_WITH, true );
	}

	@Then("^the (?:field|element) values (?:should end with|end with):$")
	public void the_field_values_should_end_with( List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, null, VerifyOperation.ENDS_WITH, true );
	}

	@Then("^the (?:field|element) values in " + StandardSahiSteps.ELEMENT_REGEX + " (?:should not end with|do not end with):$")
	public void the_field_values_in_parent_should_not_end_with( String parentType,
	                                                            String parentLocator,
	                                                            Integer parentPosition,
	                                                            List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, find( parentType, parentLocator, parentPosition ), VerifyOperation.ENDS_WITH, false );
	}

	@Then("^the (?:field|element) values (?:should not end with|do not end with):$")
	public void the_field_values_should_not_end_with( List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, null, VerifyOperation.ENDS_WITH, false );
	}

	// Equals & matches

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should equal|equals|matches|should match) \"([^\"]*)\"$")
	public void the_element_in_parent_should_equal( String type,
	                                                String locator,
	                                                Integer position,
	                                                String parentType,
	                                                String parentLocator,
	                                                Integer parentPosition,
	                                                String value ) throws Throwable {
		ElementDescriptor descriptor = new ElementDescriptor( locator, position, type, value, VerifyOperation.EQUALS );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.verify( descriptor, true );
	}

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_REGEX + " (?:should equal|equals|matches|should match) \"([^\"]*)\"$")
	public void the_element_should_equal( String type,
	                                      String locator,
	                                      Integer position,
	                                      String value ) throws Throwable {
		elementHandleService.verify( new ElementDescriptor( locator, position, type, value, VerifyOperation.EQUALS ),
		                             true );
	}

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:should not equal|does not equal|does not match|should not match) \"([^\"]*)\"$")
	public void the_element_should_not_equal( String type,
	                                          String locator,
	                                          Integer position,
	                                          String parentType,
	                                          String parentLocator,
	                                          Integer parentPosition,
	                                          String value ) throws Throwable {
		ElementDescriptor descriptor = new ElementDescriptor( locator, position, type, value, VerifyOperation.EQUALS );
		descriptor.setParent( find( parentType, parentLocator, parentPosition ) );

		elementHandleService.verify( descriptor, false );
	}

	@Then("^(?:ensure that )?" + StandardSahiSteps.ELEMENT_REGEX + " (?:should not equal|does not equal|does not match|should not match) \"([^\"]*)\"$")
	public void the_element_should_not_equal( String type,
	                                          String locator,
	                                          Integer position,
	                                          String value ) throws Throwable {
		elementHandleService.verify( new ElementDescriptor( locator, position, type, value, VerifyOperation.EQUALS ),
		                             false );
	}

	@Then("^the (?:field|element) values in " + StandardSahiSteps.ELEMENT_REGEX + " (?:should be|are|equal|should equal|match|should match):$")
	public void the_field_values_in_parent_should_be( String parentType,
	                                                  String parentLocator,
	                                                  Integer parentPosition,
	                                                  List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, find( parentType, parentLocator, parentPosition ), VerifyOperation.EQUALS, true );
	}

	@Then("^the (?:field|element) values (?:should be|are|equal|should equal|should match|match):$")
	public void the_field_values_should_be( List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, null, VerifyOperation.EQUALS, true );
	}

	@Then("^the (?:field|element) values in " + StandardSahiSteps.ELEMENT_REGEX + " (?:should not be|are not|do not equal|should not equal|do not match|should not match):$")
	public void the_field_values_in_parent_should_not_be( String parentType,
	                                                      String parentLocator,
	                                                      Integer parentPosition,
	                                                      List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, find( parentType, parentLocator, parentPosition ), VerifyOperation.EQUALS, false );
	}

	@Then("^the (?:field|element) values (?:should not be|are not|do not equal|should not equal|do not match|should not match):$")
	public void the_field_values_should_not_be( List<ElementDescriptor> entries ) throws Throwable {
		verify( entries, null, VerifyOperation.EQUALS, false );
	}

	@And("^" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:exists|should exist)$")
	public void element_in_parent_element_exists( String type,
	                                              String locator,
	                                              Integer position,
	                                              String parentType,
	                                              String parentLocator,
	                                              Integer parentPosition ) throws Throwable {
		assertTrue( exists( find( type, locator, position, find( parentType, parentLocator, parentPosition ) ) ) );
	}

	@And("^" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:is|should be) visible$")
	public void element_in_parent_element_is_visible( String type,
	                                                  String locator,
	                                                  Integer position,
	                                                  String parentType,
	                                                  String parentLocator,
	                                                  Integer parentPosition ) throws Throwable {
		assertTrue( isVisible( find( type, locator, position, find( parentType, parentLocator, parentPosition ) ) ) );
	}

	@And("^" + StandardSahiSteps.ELEMENT_REGEX + " (?:exists|should exist)$")
	public void element_does__exist( String type, String locator, Integer position ) throws Throwable {
		ElementStub stub = find( type, locator, position );
		assertTrue( exists( stub ) );
	}

	@And("^" + StandardSahiSteps.ELEMENT_REGEX + " (?:is|should be) visible$")
	public void element_is_visible( String type, String locator, Integer position ) throws Throwable {
		i_should_see_specific( type, locator, position );
	}

	@And("^" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:does not|should not) exist$")
	public void element_in_parent_element_does_not_exist( String type,
	                                                      String locator,
	                                                      Integer position,
	                                                      String parentType,
	                                                      String parentLocator,
	                                                      Integer parentPosition ) throws Throwable {
		ElementStub parent = find( parentType, parentLocator, parentPosition );

		if ( exists( parent ) ) {
			ElementStub stub = find( type, locator, position, parent );
			assertFalse( exists( stub ) );
		}
	}

	@And("^" + StandardSahiSteps.ELEMENT_WITH_PARENT_REGEX + " (?:is not|should not be) visible$")
	public void element_in_parent_element_is_not_visible( String type,
	                                                      String locator,
	                                                      Integer position,
	                                                      String parentType,
	                                                      String parentLocator,
	                                                      Integer parentPosition ) throws Throwable {
		ElementStub parent = find( parentType, parentLocator, parentPosition );

		if ( exists( parent ) ) {
			// If parent doesn't exist, then the element will not be found and that's the same as not visible
			ElementStub stub = find( type, locator, position, parent );
			assertFalse( isVisible( stub ) );
		}
	}

	@And("^" + StandardSahiSteps.ELEMENT_REGEX + " (?:does not|should not) exist$")
	public void element_does_not_exist( String type, String locator, Integer position ) throws Throwable {
		ElementStub stub = find( type, locator, position );
		assertFalse( exists( stub ) );
	}

	@And("^" + StandardSahiSteps.ELEMENT_REGEX + " (?:is not|should not be) visible$")
	public void element_is_not_visible( String type, String locator, Integer position ) throws Throwable {
		i_should_not_see_specific( type, locator, position );
	}

	private void verify( List<ElementDescriptor> entries,
	                     ElementStub parent,
	                     VerifyOperation operation,
	                     boolean outcome ) {
		for ( ElementDescriptor descriptor : entries ) {
			descriptor.setOperation( operation );
			descriptor.setParent( parent );

			elementHandleService.verify( descriptor, outcome );
		}
	}

	@And("^(?:i )?switch to (?:popup|window) \"([^\"]*)\"$")
	public void i_switch_to_popup( String name ) throws Throwable {
		browser.setWindow( spel.getValueAsString( name ) );
	}

	@And("^(?:i )?clear the (?:popup|window)$")
	public void i_clear_the_popup() throws Throwable {
		browser.clearWindow();
	}

	@And("^(?:i )?close (?:popup|window) \"([^\"]*)\"$")
	public void i_close_popup( String name ) throws Throwable {
		String popupName = spel.getValueAsString( name );
		if ( browser.isPopup( popupName ) ) {
			browser.clearWindow();
		}

		browser.popup( popupName ).close();
	}

	@And("^(?:i )?close the window$")
	public void i_close_the_window() throws Throwable {
		if ( browser.isPopup() ) {
			browser.close();
			browser.clearWindow();
		}
	}
}
