/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.lookup;

import com.foreach.cuke.sahi.browser.SahiBrowser;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ElementStubLookupByLabel implements ElementStubLookup
{
	@Autowired
	private SahiBrowser browser;

	public ElementStub find( Object[] arguments, ElementStub parent ) {
		ElementStub labelElement = parent != null ? browser.label( arguments ).in( parent ) : browser.label( arguments );

		if ( labelElement.exists() ) {
			String targetId = labelElement.getAttribute( "htmlFor" );

			if ( StringUtils.isNotBlank( targetId ) ) {
				return browser.byId( targetId );
			}
		}

		return null;
	}
}
