/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.element;

import com.foreach.cuke.core.spel.SpelExpressionExecutor;
import net.sf.sahi.client.ElementStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@DefaultElementHandler
@Component
public class StoredElementHandler extends ElementHandler
{
	public static final String PARENT = "parent";
	public static final String ELEMENT = "element";

	@Autowired
	private SpelExpressionExecutor spel;

	@Override
	public boolean canHandle( ElementDescriptor descriptor ) {
		return StringUtils.equalsIgnoreCase( PARENT, descriptor.getType() ) || StringUtils.equalsIgnoreCase( ELEMENT,
		                                                                                                     descriptor.getType() );
	}

	@Override
	public ElementStub find( ElementDescriptor descriptor ) {
		Object stub = spel.getValue( descriptor.getLocator() );

		if ( stub instanceof ElementStub ) {
			return (ElementStub) stub;
		}

		LOG.info( "Returning a fake ElementStub because stored variable was not found." );

		return ELEMENT_EMPTY;
	}

	@Override
	public void set( ElementDescriptor descriptor ) {
		throw new UnsupportedOperationException( "Set is not supported on parent element type" );
	}

	@Override
	public void verify( ElementDescriptor descriptor, boolean expectedOutcome ) {
		throw new UnsupportedOperationException( "Verify is not supported on parent element type" );
	}
}
