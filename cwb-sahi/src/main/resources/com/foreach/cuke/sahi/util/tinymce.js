/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function _setTinyMceText( tinyMceId, text )
{
    _setTinyMceFocus( tinyMceId );

    tinymce.editors[tinyMceId].setContent( text );

    _resetTinyMceCursor( tinyMceId, false );
    return true;
}

function _tinymceUtilFindElementWithText( body, text )
{
    var curr;
    var queue = [body];
    while ( curr = queue.pop() ) {
        if ( !curr.textContent.match( text ) ) {
            continue;
        }
        for ( var i = 0; i < curr.childNodes.length; ++i ) {
            switch ( curr.childNodes[i].nodeType ) {
                case Node.TEXT_NODE : // 3
                    if ( curr.childNodes[i].textContent.match( text ) ) {
                        return curr.childNodes[i];
                    }
                    break;
                case Node.ELEMENT_NODE : // 1
                    queue.push( curr.childNodes[i] );
                    break;
            }
        }
    }

    return null;
}

function _resetTinyMceCursor( tinyMceId, beforeText )
{
    var ed = tinymce.editors[tinyMceId];
    var doc = ed.getDoc();

    var win = doc.defaultView || doc.parentWindow;
    var sel = win.getSelection();
    sel.removeAllRanges();

    var range = doc.createRange();
    range.selectNodeContents( beforeText ? ed.dom.getRoot().firstChild : ed.dom.getRoot().lastChild );
    range.collapse( beforeText );
    sel.addRange( range );
}

function _setTinyMceCursor( tinyMceId, elementId )
{
    var doc = tinymce.editors[tinyMceId].getDoc();

    var element = elementId != null ? doc.getElementById( elementId ) : null;

    if ( element == null ) {
        element = doc.body.firstChild;

        if ( element == null ) {
            element = doc.body;
        }
    }

    if ( typeof doc.createRange != "undefined" ) {
        var win = doc.defaultView || doc.parentWindow;
        var sel = win.getSelection();
        sel.removeAllRanges();

        var range = doc.createRange();
        range.selectNodeContents( doc.getElementById( elementId ) );
        range.collapse( true );
        sel.addRange( range );
    }
    else if ( typeof doc.body.createTextRange != "undefined" ) {
        var textRange = doc.body.createTextRange();
        textRange.moveToElementText( element );
        textRange.collapse( start );
        textRange.select();
    }

    return true;
}

function _putTinyMceCursorAfterText( tinyMceId, text )
{
    if ( _selectTinyMceText( tinyMceId, text ) ) {
        var sel = tinymce.editors[tinyMceId].selection;
        sel.collapse( false );
    }
}

function _putTinyMceCursorBeforeText( tinyMceId, text )
{
    if ( _selectTinyMceText( tinyMceId, text ) ) {
        var sel = tinymce.editors[tinyMceId].selection;
        sel.collapse( true );
    }
}

function _putTinyMceCursorAfterSelection( tinyMceId )
{
    _setTinyMceFocus( tinyMceId );

    var sel = tinymce.editors[tinyMceId].selection;
    sel.collapse( false );
}

function _putTinyMceCursorBeforeSelection( tinyMceId )
{
    _setTinyMceFocus( tinyMceId );
    
    var sel = tinymce.editors[tinyMceId].selection;
    sel.collapse( true );
}

function _insertTinyMceText( tinyMceId, text )
{
    var sel = tinymce.editors[tinyMceId].selection;
    sel.setContent( text );
}

function _replaceTinyMceText( tinyMceId, text, replacement )
{
    if ( _selectTinyMceText( tinyMceId, text ) ) {
        _insertTinyMceText( tinyMceId, replacement );
    }
}

function _selectTinyMceFirstNode( tinyMceId, contentsOnly )
{
    _selectTinyMceNode( tinyMceId, tinymce.editors[tinyMceId].dom.getRoot().firstChild, contentsOnly );
}

function _selectTinyMceLastNode( tinyMceId, contentsOnly )
{
    _selectTinyMceNode( tinyMceId, tinymce.editors[tinyMceId].dom.getRoot().lastChild, contentsOnly );
}

function _selectTinyMceNode( tinyMceId, node, contentsOnly )
{
    var ed = tinymce.editors[tinyMceId];
    var doc = ed.getDoc();

    if ( contentsOnly ) {
        var win = doc.defaultView || doc.parentWindow;
        var sel = win.getSelection();
        sel.removeAllRanges();

        var range = doc.createRange();
        range.selectNodeContents( node );
        sel.addRange( range );
    }
    else {
        ed.selection.select( node );
    }
}

function _selectTinyMceNodeWithText( tinyMceId, text )
{
    var textNode = _tinymceUtilFindElementWithText( tinymce.editors[tinyMceId].dom.getRoot(), text );
    var doc = tinymce.editors[tinyMceId].getDoc();

    if ( textNode != null ) {
        tinymce.editors[tinyMceId].selection.select( textNode.parentNode );
        /*
         var win = doc.defaultView || doc.parentWindow;
         var sel = win.getSelection();
         sel.removeAllRanges();

         sel.select( textNode.parentNode );
         */
        /*
         var range = doc.createRange();
         range.selectNode( textNode.parentNode );
         sel.addRange( range );
         */
        return true;
    }

    return false;
}

function _selectTinyMceNodeContentsWithText( tinyMceId, text )
{
    var textNode = _tinymceUtilFindElementWithText( tinymce.editors[tinyMceId].dom.getRoot(), text );
    var doc = tinymce.editors[tinyMceId].getDoc();

    if ( textNode != null ) {
        var win = doc.defaultView || doc.parentWindow;
        var sel = win.getSelection();
        sel.removeAllRanges();

        var range = doc.createRange();
        range.selectNodeContents( textNode.parentNode );
        sel.addRange( range );

        return true;
    }

    return false;
}

function _selectTinyMceText( tinyMceId, text )
{
    var body = tinymce.editors[tinyMceId].dom.getRoot();
    var doc = tinymce.editors[tinyMceId].getDoc();

    var element = _tinymceUtilFindElementWithText( body, text );

    if ( element == null ) {
        console.log( "Did not find text " + text + " in tinymce with id " + tinyMceId );
        return false;
    }

    var win = doc.defaultView || doc.parentWindow;
    var sel = win.getSelection();
    sel.removeAllRanges();

    var range = doc.createRange();
    var start = element.textContent.indexOf( text );
    var end = start + text.length;
    console.log( "selecting " + start + " to " + end );

    range.setStart( element, start );
    range.setEnd( element, end );
    sel.addRange( range );

    return true;
}

function _setTinyMceFocus( tinyMceId )
{
    //if ( Prototype.Browser.IE ) {
    //    tinyMCE.execCommand( 'mceFocus', false, tinyMceId );
    //}
    //else {
        //var editor = tinyMCE.get( tinyMceId );
        //if ( editor ) {
        //    editor.execCommand( 'mceStartTyping' );
        //    editor.contentWindow.focus();
        //}
    //}
    tinymce.execCommand( 'mceFocus', false, tinyMceId );
}

function _getTinyMceContent( tinyMceId )
{
    return tinymce.editors[tinyMceId].getContent();
}