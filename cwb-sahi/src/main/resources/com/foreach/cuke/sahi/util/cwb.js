
/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
_sahi.oldWindowLoad = _sahi.onWindowLoad;

var _cwbLoader = { delayedStart: null };

function CucumberWebBridgeStartDelay( e, tries ) {
    var nextDelay = _cwbLoader.delayedStart( tries );

    if ( nextDelay > 0 ) {
        if ( console ) {
            console.log('Delaying CWB script another ' + nextDelay + ' milliseconds' );
        }
        setTimeout( function() {
                        CucumberWebBridgeStartDelay( e, tries + 1 );
                    },
                    nextDelay
        );
    }
    else {
        _sahi.oldWindowLoad( e );
    }
}

_sahi.onWindowLoad = function(e)
{
    if ( _cwbLoader.delayedStart != null ) {
        if ( console ) {
            console.log( 'CWB delaying start of script');
        }
        CucumberWebBridgeStartDelay( e, 1 );
    }
    else {
        _sahi.oldWindowLoad( e );
    }
};

/**
 * Contains a set of cwb specific javascript functions.
 */
function CucumberWebBridgeSahiElementStubs() {
    /**
     * Looks for heading1 to 6 element.
     */
    this._heading = function( identifier, parent ) {
        if ( _sahi._exists(_sahi._heading1(identifier, parent)) ) {
            return _sahi._heading1(identifier, parent);
        }

        if ( _sahi._exists(_sahi._heading2(identifier, parent)) ) {
            return _sahi._heading2(identifier, parent);
        }

        if ( _sahi._exists(_sahi._heading3(identifier, parent)) ) {
            return _sahi._heading3(identifier, parent);
        }

        if ( _sahi._exists(_sahi._heading4(identifier, parent)) ) {
            return _sahi._heading4(identifier, parent);
        }

        if ( _sahi._exists(_sahi._heading5(identifier, parent)) ) {
            return _sahi._heading5(identifier, parent);
        }

        if ( _sahi._exists(_sahi._heading6(identifier, parent)) ) {
            return _sahi._heading6(identifier, parent);
        }

        return null;
    };

    /**
     * Looks for a textbox/textarea/numberbox/emailbox/datebox, with fallback to label lookup.
     */
    this._textbox = function( identifier, parent ) {
        if ( _sahi._exists(_sahi._textbox(identifier, parent)) ) {
            return _sahi._textbox(identifier, parent);
        }

        if ( _sahi._exists(_sahi._textarea(identifier, parent)) ) {
            return _sahi._textarea(identifier, parent);
        }

        if ( _sahi._exists(_sahi._numberbox(identifier, parent)) ) {
            return _sahi._numberbox(identifier, parent);
        }

        if ( _sahi._exists(_sahi._emailbox(identifier, parent)) ) {
            return _sahi._emailbox(identifier, parent);
        }

        if ( _sahi._exists(_sahi._datebox(identifier, parent)) ) {
            return _sahi._datebox(identifier, parent);
        }

        if ( _sahi._exists(_sahi._telephonebox(identifier, parent)) ) {
            return _sahi._telephonebox(identifier, parent);
        }

        if ( _sahi._exists(_sahi._searchbox(identifier, parent)) ) {
            return _sahi._searchbox(identifier, parent);
        }

        if ( _sahi._exists(_sahi._urlbox(identifier, parent)) ) {
            return _sahi._urlbox(identifier, parent);
        }

        if ( _sahi._exists(_sahi._rangebox(identifier, parent)) ) {
            return _sahi._rangebox(identifier, parent);
        }

        if ( _sahi._exists(_sahi._timebox(identifier, parent)) ) {
            return _sahi._timebox(identifier, parent);
        }

        return _sahi._byLabel( identifier, parent );
    };

    /**
     * Looks for a password field, with fallback to label lookup.
     */
    this._password = function( identifier, parent ) {
        if ( _sahi._exists(_sahi._password(identifier, parent)) ) {
            return _sahi._password(identifier, parent);
        }

        return _sahi._byLabel( identifier, parent );
    };

    /**
     * Looks for a checkbox field, with fallback to label lookup.
     */
    this._checkbox = function( identifier, parent ) {
        if ( _sahi._exists(_sahi._checkbox(identifier, parent)) ) {
            return _sahi._checkbox(identifier, parent);
        }

        return _sahi._byLabel( identifier, parent );
    };

    /**
     * Looks for a radio field, with fallback to label lookup.
     */
    this._radio = function( identifier, parent ) {
        if ( _sahi._exists(_sahi._radio(identifier, parent)) ) {
            return _sahi._radio(identifier, parent);
        }

        return _sahi._byLabel( identifier, parent );
    };

    /**
     * Looks for a select field, with fallback to label lookup.
     */
    this._select = function( identifier, parent ) {
        if ( _sahi._exists(_sahi._select(identifier, parent)) ) {
            return _sahi._select(identifier, parent);
        }

        return _sahi._byLabel( identifier, parent );
    };

    /**
     * Looks for label element and then the target element it is attached to.
     */
    this._byLabel = function( identifier, parent ) {
        if ( _sahi._exists(_sahi._label(identifier, parent)) ) {
            var targetId = _sahi._getAttribute(_sahi._label(identifier, parent), "htmlFor");

            if ( targetId ) {
                return _sahi._byId( targetId );
            }
        }

        return null;
    };

    /**
     * Looks for a button field.
     */
    this._button = function( identifier, parent ) {
        if ( _sahi._exists(_sahi._submit(identifier, parent)) ) {
            return _sahi._submit(identifier, parent);
        }

        if ( _sahi._exists(_sahi._button(identifier, parent)) ) {
            return _sahi._button(identifier, parent);
        }

        if ( _sahi._exists(_sahi._imageSubmitButton(identifier, parent)) ) {
            return _sahi._imageSubmitButton(identifier, parent);
        }

        return null;
    };

    /**
     * Look for a tinymce box.
     */
    this._tinymce = function( identifier, parent ) {
        if ( _sahi._exists( _sahi._byId( identifier ) ) ) {
            return _sahi._byId( identifier );
        }

        return _sahi._byLabel( identifier, parent );
    };

    /**
     * Look for the toolbar attached to a tinymce instance.
     */
    this._tinymceToolbar = function( identifier, parent ) {
        var tinymce = null;

        if ( _sahi._exists( _sahi._byId( identifier ) ) ) {
            tinymce = _sahi._byId( identifier );
        }
        else {
            tinymce = _sahi._byLabel( identifier, parent );
        }

        if ( _sahi._exists( tinymce ) ) {
            return _sahi._byId( tinymce.getAttribute( "id" ) + '_parent' );
        }

        return null;
    };

}

var _cwb = new CucumberWebBridgeSahiElementStubs();

// Define _sahi extensions
_sahi._cwbHeading = _cwb._heading;
_sahi._cwbTextbox = _cwb._textbox;
_sahi._cwbPassword = _cwb._password;
_sahi._cwbCheckbox = _cwb._checkbox;
_sahi._cwbRadio = _cwb._radio;
_sahi._cwbButton = _cwb._button;
_sahi._cwbSelect = _cwb._select;
_sahi._byLabel = _cwb._byLabel;
_sahi._cwbTinyMCE = _cwb._tinymce;
_sahi._cwbTinyMCEToolbar = _cwb._tinymceToolbar;

_sahi._setAttribute = function( el, attribute, value ){
    el.setAttribute( attribute, value );
};