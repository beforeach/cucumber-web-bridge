/*
 * Copyright 2014 Foreach bvba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.cuke.sahi.element;

import net.sf.sahi.client.ElementStub;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertSame;

public class TestElementHandler
{
	@Test
	public void handlerSorting() {
		ElementHandler defaultOne = new DefaultOne();
		ElementHandler defaultTwo = new DefaultTwo();
		ElementHandler defaultThree = new DefaultThree();
		ElementHandler nonDefault = new NonDefault();

		List<ElementHandler> list = Arrays.asList( defaultOne, nonDefault, defaultTwo, defaultThree );

		Collections.sort( list );

		assertSame( nonDefault, list.get( 0 ) );
		assertSame( defaultTwo, list.get( 3 ) );
	}

	public static abstract class AbstractElementHandler extends ElementHandler
	{
		@Override
		public boolean canHandle( ElementDescriptor descriptor ) {
			return false;
		}

		@Override
		public ElementStub find( ElementDescriptor descriptor ) {
			return null;
		}

		@Override
		public void set( ElementDescriptor descriptor ) {
		}

		@Override
		public void verify( ElementDescriptor descriptor, boolean expectedOutcome ) {
		}
	}

	@DefaultElementHandler
	public static class DefaultOne extends AbstractElementHandler
	{
	}

	@DefaultElementHandler
	public static class DefaultTwo extends AbstractElementHandler
	{
		@Override
		public int getPriority() {
			return 2;
		}
	}

	@DefaultElementHandler
	public static class DefaultThree extends AbstractElementHandler
	{
	}

	public static class NonDefault extends AbstractElementHandler
	{
	}
}
